local map = ...
local game = map:get_game()

--local hp_menu = require"scripts/menus/boss_hp"

map.ai = "robot"
local retaliation = 1

local boss_hp = game:get_property"boss_hp"
game:set_value("boss_hp", boss_hp)

--if not sol.menu.is_started(hp_menu) then
--	sol.menu.start(map, hp_menu)
--else hp_menu:refresh() end

print("boss start", sol.main.get_elapsed_time())

function map:is_done()
	if boss_hp > 0 then
		self.opponent:set_animation("attack", "stopped")
		sol.timer.start(self, 250, function()
			sol.audio.play_sound"lamp"
			game.guess_analyzer:do_obfuscate_attack(retaliation)
			retaliation = retaliation + 1
			sol.timer.start(self, 1250, function()
				map:add_guesses(game:get_property"boss_gain")
				game.guess_analyzer:update_panel_count() --make additional guesses available
				game.guess_analyzer:clear_hints() --only clear hints for codeword change on boss stage
				map:new_codeword()
			end)
		end)
		return false
	else --boss defeated, level is now complete
		self.opponent:set_animation"collapse"
		sol.audio.play_sound"hero_dying"
		
		print("defeated!", sol.main.get_elapsed_time())
		
		return true
	end
end

function map:on_hit()
	self.opponent:set_animation"hit"
	sol.audio.play_sound"boss_hurt"
	
	boss_hp = boss_hp - 1
	game:set_value("boss_hp", boss_hp)
	self.opponent:refresh() --update heart count
	
	if boss_hp <=0 then self:set_timer_suspended(true) end
end
