--entering this map starts the game over sequence

local game_over_menu = require"scripts/menus/game_over"

local map = ...
local game = map:get_game()

--want to override map_meta event
function map:on_started()
	sol.menu.start(self, game_over_menu)
	
	sol.timer.start(self, 1800, function()
		local num_continues = tonumber(game:get_value"continues")
		if num_continues ~= 0 then --also may be false/nil
			sol.main.menus:start_submenu("game_over_continue", function()
				--decrement number of continues
				if num_continues then
					num_continues = num_continues - 1
					game:set_value("continues", num_continues)
				end
				
				--update total retries stats
				local total_retries = (game:get_value"total_retries" or 0) + 1
				game:set_value("total_retries", total_retires)
				
				game:start_game_over() --restart stage
			end)
		else sol.main.menus:start_submenu"game_over_no_continue" end
	end)
end
