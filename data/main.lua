--Main Lua script of the quest.
--See the Lua API! http://www.solarus-games.org/doc/latest


local initial_menus = require"scripts/menus/initial_menus"
local settings = require"scripts/settings"
local game_manager = require"scripts/game_manager"
local title_menu = require"scripts/menus/title"
local debug = require"scripts/menus/debug"
local screenshot = require"scripts/screenshot"
local solarus_logo = require"scripts/menus/solarus_logo"
require"scripts/multi_events"

--// Event called when Solarus starts
function sol.main:on_started()
	math.randomseed(os.time())
	
	settings.load()
	debug:on_language_changed()
	
	local title_name = sol.language.get_string"title.name"
	sol.video.set_window_title(title_name.." - Solarus "..sol.main.get_solarus_version())
	sol.video.set_window_size(sol.video.get_quest_size()) --don't upscale window size
	
	sol.game_manager = game_manager
	
	if sol.main.get_elapsed_time() < 2000 then
		sol.menu.start(sol.main, solarus_logo)
		sol.timer.start(sol.main, 500, function() --start title menu slightly after logo
			sol.menu.start(sol.main, title_menu, false)
		end)
	else sol.menu.start(sol.main, title_menu) end --no solarus splash logo on reset
end

--// Event called when Solarus stops
function sol.main:on_finished()
	settings.save()
end

-- Event called when the player pressed a keyboard key.
function sol.main:on_key_pressed(key, modifiers)
	local handled = false
	
	if key == "f11" or
	(key == "return" and (modifiers.alt or modifiers.control)) then
		-- F11 or Ctrl + return or Alt + Return: switch fullscreen.
		sol.video.set_fullscreen(not sol.video.is_fullscreen())
		
		handled = true
	elseif key == "f12" then
		screenshot:save"screenshot"
		
		handled = true
	elseif key == "f4" and modifiers.alt then
		-- Alt + F4: stop the program.
		sol.main.exit()
		
		handled = true
	end

	return handled
end
