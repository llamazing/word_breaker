All music by Eric Matyas (www.soundimage.org)

License: **Soundimage International Public License**

Info taken from www.soundimage.org (19 Apr 2020)

## About

Hi everyone,

I’m a big believer in the democratization of motion pictures, television, music, games and pretty much all media. Anyone should be able to tell stories, explore ideas and communicate through these media without needing millions of dollars to do so. Thanks to digital technology, this is now more possible than ever.

Unfortunately, the cost of obtaining quality music for projects is still often prohibitively expensive… especially for individuals, students, teachers and media artists.

This site is my attempt to try to help remedy this.

One of the challenges of using preexisting music in films and videos is that the music often doesn’t time out right or “fit” with the movie that’s already been edited. With this in mind, I’ve tried to create pieces that are a little less “on the nose”…pieces that convey moods and emotions in a more general way such that they might fit into your project a little bit easier.

If you hear a piece that you’d like customized, or if you would like me to create a custom piece for your project, feel free to contact me at ematyas@aol.com.

I hope you find something you can use.

EM

P.S. A big thank you to Kevin MacLeod for encouraging me to give this a try.

## FAQs

### How do I download your music?

To download any given piece of music to your computer, simply right-click on the blue text link under its player and choose “save link as” or “save target as” depending on your browser. Then choose the location on your system where you would like to save it.

### Can I alter your music?

Yes, you may alter it any way you like, but you must still attribute the original piece to me in your credits.

### Can you alter your music for me?

I would be happy to customize my music for your project for a donation. Feel free to contact me at ematyas@aol.com.

### What if I can’t attribute you?

You will need to contact me for a non-attribution license. They are:

```
$30 per track for music
$5 per sound effect.
$5 per texture image.
$10 per video clip.
```

Please contact me at ematyas@aol.com.

### Can you score my project for me?

I would be happy to create original music for you. Contact me at ematyas@aol.com.

### What license does your music fall under?

My license is exactly the same as CC by Attribution 4.0 with one restriction: The use of my music in any media that violates Youtube community standards is prohibited. In other words, you can’t use my music in obscene material. Please see my “About” page for the full license.

### Prohibited Uses

The use of my music or images in any media that violates Youtube community standards is prohibited. In other words, you can’t use my music in obscene material.

### How can I donate?

Donation links through PayPal are located toward the bottom of each page. Donations can be any amount and are greatly appreciated.

### How much should I donate?

The amount people donate varies wildly, but from a purely mathematical perspective, the average donation is about $10-$15 (USD.) Honestly, though, any amount is very helpful and greatly appreciated. Please keep in mind that PayPal takes a fee from each donation, so if you donate a very small amount, like $1, it will most likely all go to PayPal and I won’t receive any of it.

### CONTACT:

Feel free to email me anytime at:

ematyas@aol.com

## Attribution Info

In order to use my work legally, you must credit me in your actual video, game, interactive novel, etc. Crediting me on the info page on Youtube, Google Play, etc. instead of in the actual video, game, novel, etc. is not sufficient. (The reason is that people often don’t read the information section.)

### FOR MUSIC:

If you are using only my music and not that of another artist(s) then your credits screen of your video, game, etc. needs to say:

```
Music by Eric Matyas
www.soundimage.org
```

If you are using my music tracks as well as tracks by other artist(s) then you need to identify which tracks are mine and credit me, for example:

```
“Invasion of the Giant Disco Ants”
“Too Quiet”
“Mayhem”
by Eric Matyas
www.soundimage.org
```

>>>
*If you cannot or do not wish to attribute me for my music tracks, then you will need to purchase a non-attribution license from me for $30 per track. Please contact me at ematyas@aol.com.
>>>