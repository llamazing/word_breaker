--[[achievements.lua
	version 0.1a1
	2 Jul 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script manages achievements and displays a notification when new ones are earned.
	
	Usage:
	local achievements = require"scripts/achievements"
	achievements.give"name_of_achievement"
]]

local settings = require"scripts/settings"

local achievements = {}

--TODO move to separate .dat file
local ACHIEVEMENTS_LIST = {
	achievement_casual = true,
	achievement_normal = true,
	achievement_hardcore = true,
	achievement_speedrunner = true,
	achievement_high_score = true,
	achievement_brain_transplant = true,
	achievement_quick_study = true,
	achievement_anagrampage = true,
	achievement_lucky_ducky = true,
	achievement_codebreaker = true,
}

--// Gives the player an achievement if it hasn't already been earned
	--id (string) - settings id corresponding to the achievement to award
	--value (number, non-negative integer, optional) - value for the achievement
		--1 or more grants the player that tier of the achievement (higher is better)
		--default: grants the first tier of the achievement (value = 1)
		--a value lower than or equal to the current settings value has no effect (unless setting to 0)
		--can set to 0 or false to remove all tiers of the achievement
		--some achievements may have only 1 tier, others as many as 3 (bronze, silver, gold)
function achievements.give(id, value)
	assert(type(id)=="string", "Bad argument #1 to 'give' (string expected)")
	assert(ACHIEVEMENTS_LIST[id], "Bad argument #1 to 'give', invalid id: "..id)
	
	local num_value = tonumber(value or (value==nil and 1 or 0))
	assert(num_value, "Bad argument #2 to 'give' (number or false or nil expected)")
	num_value = math.floor(num_value)
	assert(num_value>=0, "Bad argument #2 to 'give', number must be non-negative")
	
	local game = sol.main.get_game()
	assert(game, "Error in 'give', no game is running")
	if game:get_value"cheater" then return end
	
	local old_value = tonumber(settings.get_value(id)) or 0
	if num_value>old_value or value==0 then --setting to 0 removes achievement
		settings.set_value(id, num_value)
		local string_key = id:gsub("_",".",1)..".title" --convert first underscore to period to get corresponding strings.dat key
		local name = sol.language.get_string(string_key)
		if num_value>0 then
			print(string.format("You earned the %q achievement! (tier %d)", name, num_value)) --TODO better notification
		else print(string.format("Achievement %q removed", name)) end
	end
end

return achievements

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
