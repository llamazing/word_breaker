--[[gamepad_manager.lua
	version 0.1a1
	23 May 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script manages gamepad inputs by looking up button mappings and processing button
	sequences (e.g. double-press or button combo).
	
	usage:
	local gamepad_manager = require"scripts/gamepad_manager"
	function menu:on_joypad_button_pressed(button)
		if is_menu_handling_events then
			local action = gamepad_manager.get_button_action(button)
			if action == "close" then sol.menu.stop(self) end
			return true
		else return false end
	end
]]

local gamepad_manager = {}

--TODO add menu to configure gamepad button bindings
local BINDINGS = {
	[0] = "action",
	"back",
	"scramble",
	"submit",
	"hint",
	false, --home
	"pause",
	false,
	false,
	"toggle_mode",
	"contextal_menu",
	"up",
	"down",
	"left",
	"right",
}

function gamepad_manager.get_button_action(button)
	return BINDINGS[button] or false
end

return gamepad_manager

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
