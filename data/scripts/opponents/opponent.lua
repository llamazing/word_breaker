--[[ opponent.lua
	version 0.1a1
	4 Jun 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script animates the opponent portrait and plays audio taunts based on the actions
	of the player.
	
	Currently there is one opponent for all levels, except for a second boss opponent that
	is only present on the final level. There remains the possibility for future expansion
	where there could be multiple opponents that alternate randomly each level, where each
	one has a different appearance and voice.
]]

local settings = require"scripts/settings"
local portrait_menu = require"scripts/menus/portrait"

--convenience
local math__random = math.random

local opponent = {}

local ai_list = {}

local function load_ai(enemy_id)
	if not enemy_id then return end
	if ai_list[enemy_id] then return ai_list[enemy_id] end --loaded previously
	
	local new_ai
	
	local env = setmetatable({math=math}, {})
	function env.ai(properties)
		--TODO validate properties
		
		local taunt_probability = properties.taunt_probability or {}
		local max_taunts = properties.max_taunts or {}
		
		local data = {
			sprite = properties.sprite,
			is_taunts = properties.is_taunts,
			taunt_probability = {
				bad_guess = taunt_probability.bad_guess,
				good_guess = taunt_probability.good_guess,
				long_delay = taunt_probability.long_delay,
				guesses_remaining = taunt_probability.guesses_remaining,
			},
			max_taunts = {
				any_streak = max_taunts.any_streak or 0,
				long_delay = max_taunts.long_delay or 0,
				low_on_guesses = max_taunts.low_on_guesses or 0,
			},
			streak_repeat_time = properties.streak_repeat_time,
			min_long_delay_time = properties.min_long_delay_time,
			taunts = properties.taunts, --TODO better implementation
		}
		
		new_ai = data
	end
	
	local file_path = "scripts/opponents/"..enemy_id..".dat"
	assert(sol.file.exists(file_path), "Opponent data not found: "..file_path)
	
	local chunk = sol.main.load_file(file_path)
	setfenv(chunk, env)
	chunk()
	
	ai_list[enemy_id] = data
	
	return new_ai
end

function opponent.create(enemy_id)
	enemy_id = enemy_id or "keith" --default ai
	
	local game = sol.main:get_game()
	local map = game:get_map()
	
	local new_opponent = {}
	
	--TODO use alternate method to disable during tutorial
	if game:get_difficulty()=="tutorial" then enemy_id = nil end --no opponent during tutorial
	
	local ai = load_ai(enemy_id)
	
	portrait_menu:set_portrait(enemy_id)
	if not sol.menu.is_started(portrait_menu) then
		sol.menu.start(map, portrait_menu)
	end
	
	local guess_delay_timer = nil
	local last_streak_timestamp = nil
	local last_guess_timestamp = nil
	local streaks = {good_guess=0, bad_guess=0}
	
	--counts of taunts played during this stage
	local low_on_guesses_count = 0
	local long_delay_count = 0
	local any_streak_count = 0
	
	local function roll_taunt(taunt_type, rank)
		local probability = ai.taunt_probability[taunt_type]
		if not proability then
			probability = 0
		elseif type(probability)=="function" then
			probability = probability(rank)
		else probability = probability[rank] or 0 end
		return math__random() < probability
	end
	
	local function start_guess_delay_timer(context)
		
	end
	
	local function increment_streak(streak_type, amount)
		for streak,streak_value in pairs(streaks) do
			--increment streak counter of corresponding type, reset all other streak counters
			if streak == streak_type then
				local new_value = streak_value + amount
				streaks[streak_type] = new_value
				
				--calculate time since last taunt
				local current_time = sol.main.get_elapsed_time()
				local elapsed_time = prev_streak_timestamp and current_time - prev_streak_timestamp
				
				--chance to play taunt but only if at least min time has passed since last taunt
				if any_streak_count < ai.max_taunts.any_streak
				and roll_taunt(streak_type, new_value)
				and (not elapsed_time or elapsed_time >= ai.streak_repeat_time) then
					new_opponent:play_taunt(streak_type)
					streaks[streak_type] = 0
					last_streak_timestamp = current_time
					any_streak_count = any_streak_count + 1
					return true
				end
			else streaks[streak] = 0 end --reset counters of other types
		end
		
		return false
	end
	
	function new_opponent:play_taunt(event_name)
		if not ai or not ai.is_taunts then return end
		if settings.get_value"voice_taunts"==false then return end
		
		
		local language_id = sol.language.get_language()
		if language_id then
			local taunts = ai.taunts[event_name] or {}
			local taunt_count = #taunts
			if taunt_count >= 1 then
				local taunt_name = taunts[math__random(#taunts)]
				sol.audio.play_sound(language_id.."/"..taunt_name)
			end
		end
	end
	
	function new_opponent:set_animation(...)
		portrait_menu:set_animation(...)
	end
	
	function new_opponent:refresh()
		portrait_menu:refresh()
	end
	
	function new_opponent:on_win()
		self:play_taunt"win"
	end
	
	function new_opponent:on_lose()
		self:play_taunt"lose"
	end
	
	function new_opponent:submit_guess(match_count)
		if not ai then return end
		
		--reset time since last guess
		local current_time = sol.main.get_elapsed_time()
		last_guess_timestamp = current_time
		
		--restart guess delay timer
		if long_delay_count < ai.max_taunts.long_delay then
			if guess_delay_timer then guess_delay_timer:stop() end
			guess_delay_timer = sol.timer.start(map, ai.min_long_delay_time, function()
				local new_time = sol.main.get_elapsed_time()
				if roll_taunt("long_delay", (new_time - current_time)/1000) then --current time is time when timer was started
					self:play_taunt"long_delay"
					prev_streak_timestamp = sol.main.get_elapsed_time()
					long_delay_count = long_delay_count + 1
					guess_delay_timer = nil
					return false --stop periodic checks
				else return math__random(30000, 60000) end --recheck again at randomly chosen interval
			end)
		end
		
		--check for taunts to play
		local is_taunt
		if match_count==5 then
			self:play_taunt"anagram"
			increment_streak(false)
		elseif match_count >= 3 then
			is_taunt = increment_streak("good_guess", match_count - 2)
		elseif match_count >= 1 then
			is_taunt = increment_streak("bad_guess", 3 - match_count)
		end
		
		if not is_taunt then
			--calculate time since last taunt
			local elapsed_time = prev_streak_timestamp and current_time - prev_streak_timestamp
			
			local guess_count = map:get_guess_count()
			local max_guesses = map:get_max_guesses()
			local guesses_remaining = max_guesses - guess_count
			
			if low_on_guesses_count < ai.max_taunts.low_on_guesses
			and roll_taunt("guesses_remaining", guesses_remaining)
			and (not elapsed_time or elapsed_time >= ai.streak_repeat_time) then
				self:play_taunt"few_guesses_remain"
				prev_streak_timestamp = sol.main.get_elapsed_time()
				low_on_guesses_count = low_on_guesses_count + 1 --don't play taunt more than once per level
			end
		end
	end
	
	new_opponent:refresh()
	
	return new_opponent
end

return opponent

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
