--[[dictionary_manager.lua
	version 0.1a1
	15 May 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script loads a list of words in the current language from the dictionary.dat file
	located at: languages/<language id>/text/dictionary.dat
	
	Retains the most recently returned data for quick return next time. Changing languages
	forces the data to be reloaded.
	
	Usage:
	local dictionary_manager = require("scripts/dictionary_manager")
	local dictionary, language_id, char_list = dictionary_manager:load() --language must be set!
]]

local ustring = require"scripts/lib/ustring/ustring"

local dictionary_manager = {}

--re-use these variables next time instead of reading data file again
local env --(table, key/value)
local recent_dictionary --(table, combo)
local recent_language_id --(string)
local recent_char_list --(table, combo)

--// Creates environment for reading dictionary.dat (call first time only then re-use returned table for subsequent uses)
local function create_env()
	local env = { --allow core functions
		_VERSION = _VERSION,
		string = string,
		table = table,
		math = math,
		bit = bit,
		type = type,
		tonumber = tonumber,
		tostring = tostring,
		ipairs = ipairs,
		pairs = pairs,
		next = next,
		select = select,
		unpack = unpack,
		rawget = rawget,
		rawset = rawset,
		rawequal = rawequal,
		pcall = pcall,
		xpcall = xpcall,
		print = print,
		error = error,
		assert = assert,
	}
	setmetatable(env, {__index = function() return function() end end})
	
	function env.dictionary(properties)
		assert(type(properties)=="table", "Bad argument #1 to 'dictionary' (table expected)")
		
		local dictionary = {}
		
		for i,word in ipairs(properties) do dictionary[i] = word end --copy words to dictionary
		for k,_ in pairs(properties) do
			assert(dictionary[k], "Bad argument #1 to 'dictionary' (table must be an array), invalid key: "..tostring(k))
		end
		
		assert(#dictionary >= 32, "Error in 'dictionary', dictionary must contain at least 32 entries") --32 guarantees at least one word in dictionary will not match the up to 30 guesses or 1 current codeword
		
		for _,word in ipairs(dictionary) do dictionary[word] = true end --add reverse lookup
		
		
		--## make list of valid characters
		
		local char_list = {}
		local char_count = 0 --index of new entry to add
		local chars_str = sol.language.get_string"valid_letters"
		
		--TODO this code is redundant with code in guess_analyzer
		for i = 1, ustring.len(chars_str) do --UNICODE multi-codepoint
			local char = ustring.sub(chars_str,i,i)
			
			if not char_list[char] then --ignore duplicate characters
				char_count = char_count + 1
				char_list[char_count] = char
				char_list[char] = true --reverse lookup
			end
		end
		
		recent_dictionary = dictionary
		recent_char_list = char_list
	end
	
	return env
end

--// Loads a dictionary in the specified language (reuses previous if same language, else loads data)
	--language_id (string, optional) - language identifier (e.g. "en" or "fr") of the dictionary to load, default is the current language
function dictionary_manager:load(language_id)
	local language_id = language_id or sol.language.get_language()
	assert(language_id, "Error in 'load_dictionary': Language not set")
	assert(type(language_id)=="string", "Bad argument #2 to 'load_dictionary' (string or nil expected)")
	assert(sol.language.get_language_name(language_id), "Bad argument #2 to 'load_dictionary', language not found: "..language_id)
	
	--if different language than last time (or first time) then reload fresh data, else return same data as previous
	if recent_language_id ~= language_id then
		local file_path = "languages/"..language_id.."/text/dictionary.dat"
		assert(sol.file.exists(file_path), "Dictionary not found: "..file_path)
		
		local chunk = sol.main.load_file(file_path)
		if not env then env = create_env() end --create first time then reuse for subsequent
		setfenv(chunk, env)
		chunk()
		
		recent_language_id = language_id
	end
	
	return recent_dictionary, recent_language_id, recent_char_list
end

return dictionary_manager

--[[ Copyright 2019 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
