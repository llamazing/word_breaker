--[[settings.lua
	version 1.0.1a1
	6 Jun 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script reads and writes key/value pairs from the "settings.dat" file in the quest
	write directory, supporting the built-in values as well as custom user-defined values.
	Key names must contain only alpha-numeric or underscore characters, where the starting
	character must be a letter. Values must be a boolean, number, string, table (no tables
	of tables) or nil (to clear the value).
	
	NOTE: Do not call sol.main.load_settings() or sol.main.save_settings() when using this
	script. Use settings.load() or settings.save() instead.
	
	Usage:
	local settings = require("scripts/settings")
	settings.load() --equivalent to sol.main.load_settings()
	settings.save() --equivalent to sol.main.save_settings()
	settings.get_value(key) --get a saved settings value (or nil if does not exist)
	settings.set_value(key, value) --set a saved settings value
]]

local settings = {}

local FILE_NAME = "settings.dat" --name to use for settings file

local DEFAULT_LANGUAGE = "en" --not used if quest sets language before loading settings

--functions to read the built-in settings current values
local BUILT_IN_READ = {
	--video_mode = sol.video.get_mode, --DEPRECATED
	fullscreen = sol.video.is_fullscreen,
	sound_volume = sol.audio.get_sound_volume,
	music_volume = sol.audio.get_music_volume,
	language = sol.language.get_language,
	joypad_enabled = sol.input.is_joypad_enabled,
}

--functions to write the built-in settings current values (pass current value as first argument)
local BUILT_IN_WRITE = {
	--video_mode = sol.video.set_mode, --DEPRECATED
	fullscreen = sol.video.set_fullscreen,
	sound_volume = sol.audio.set_sound_volume,
	music_volume = sol.audio.set_music_volume,
	language = sol.language.set_language,
	joypad_enabled = sol.input.set_joypad_enabled,
}

local data = {} --store settings values in memory until file is written

local ALLOWED_TYPES = {"boolean", "number", "string", "table", "nil"} --only these types are valid value types in saved settings values (table also allowed at top-level only)
for _,allowed_type in ipairs(ALLOWED_TYPES) do ALLOWED_TYPES[allowed_type]=true end --reverse lookup

--// Checks whether string name is valid, beginning with a letter and contains only alpha-numeric/underscore characters
local function is_valid_name(name)
	return type(name)=="string" and not not name:match"^%a[%w_]*$"
end

local function is_valid_data(data) return ALLOWED_TYPES[type(data)] end

--// Extracts valid keys/values from table, putting them in new table and returns that new table
local function sanitize_table(data)
	if not type(data)=="table" then return end
	
	local new_data = {}
	for k,v in pairs(data) do
		if (tonumber(k) or is_valid_name(k)) and ALLOWED_TYPES[type(v)] and type(v)~="table" then
			new_data[k] = v
		end
	end
	
	return new_data
end

local function table_to_string(data)
	data = sanitize_table(data) or {}
	local lines = {"{"}
	
	for k,v in pairs(data) do
		local key = k
		local value = v
		
		if tonumber(key) then key = string.format("[%s]", key) end
		--TODO use brackets for any keys that are reserved lua words
		
		if type(value)=="string" then
			value = string.format("%q", value) --add quotes and escape characters to string
		else value = tostring(value) end
		
		lines[#lines+1] = string.format("%s = %s,", key, value)
	end
	lines[#lines+1] = "}"
	
	return table.concat(lines,"\n")
end

--// Loads settings values from file into memory (creates new file if does not exist)
	--call one time initially in main.lua, behaves similarly to sol.main.load_settings()
function settings.load()
	data = {} --clear any existing data
	
	if sol.file.exists(FILE_NAME) then --read data from file
		local env = setmetatable({}, {__newindex = function(self, key, value)
			if is_valid_name(key) then --ignore any bad keys
				if type(value)=="table" then value = sanitize_table(value) end
				data[key] = value
			end
		end})
		
		local chunk = sol.main.load_file(FILE_NAME)
		setfenv(chunk, env)
		chunk()
		
		--use Solarus default values for any built-in settings not defined
		for key,func in pairs(BUILT_IN_READ) do
			if data[key]==nil then data[key] = func() end
		end
		
		--set default language if none already set
		if not data.language then data.language = DEFAULT_LANGUAGE end
	else --create new file with default values
		--set default language if none already set
		if not sol.language.get_language() then
			sol.language.set_language(DEFAULT_LANGUAGE)
		end
		
		settings.save() --write file with initial values
	end
	
	--apply values from file to active game
	for key,func in pairs(BUILT_IN_WRITE) do
		local value = data[key]
		func(value)
	end
end

--// Writes settings values from memory to file, overwriting existing file
	--call before program exits, behaves similarly to sol.main.save_settings()
function settings.save()
	for key,func in pairs(BUILT_IN_READ) do data[key] = func() end --update current values of built-in variables
	
	local file = sol.file.open(FILE_NAME, "w")
	
	for key,value in pairs(data) do
		if type(value)=="string" then
			value = string.format("%q", value) --add quotes and escape characters to string
		elseif type(value)=="table" then
			value = table_to_string(value)
		else value = tostring(value) end
		
		file:write(string.format("%s = %s\n", key, value))
	end
	
	file:flush()
	file:close()
end

--// Returns a saved settings value (excluding built-in values, use API instead to access value)
	--key (string) - Name of the settings value to retrieve
	--default (any, optional) - value to return if no setting value is saved
	--returns (string, number or boolean) - The corresponding value (nil if no value is defined with this key)
function settings.get_value(key, default)
	assert(is_valid_name(key), "Bad argument #1 to 'get_value' (string must contain only alpha-numeric or underscore characters and must begin with a letter)")
	local value = data[key]
	if type(value)=="table" then value = sanitize_table(value) end --make copy to return
	
	if value==nil then value = default end
	return value
end

--// Saves a settings value (key/value pair)
	--key (string) - Name of the value to save (must contain alphanumeric characters or '_' only, and must start with a letter)
	--value (string, number or boolean) - The value to set, or nil to unset this value
	--NOTE: This method changes a value in memory, but settings:save() must be called to write it to the settings file
function settings.set_value(key, value)
	assert(is_valid_name(key), "Bad argument #1 to 'set_value' (string must contain only alpha-numeric or underscore characters and must begin with a letter)")
	assert(not BUILT_IN_READ[key], "Bad argument #1 to 'set_value' ("..key.." is a built-in variable name that cannot be used)")
	assert(ALLOWED_TYPES[type(value)], "Bad argument #2 to 'set_value' (expected "..table.concat(ALLOWED_TYPES, ", ")..")")
	
	if type(value)=="table" then value = sanitize_table(value) end
	
	data[key] = value
end

return settings

--[[ Copyright 2020 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
