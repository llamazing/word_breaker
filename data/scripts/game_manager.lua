--[[game_manager.lua
	version 0.1a1
	7 Jun 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script manages the game by keeping track of the current dictionary of valid words
	and various settings such as the difficulty mode used for level progression.
	
	Custom events:
	* game:on_codeword_changed(codeword)
]]

require"scripts/multi_events"
require"scripts/menus/dialog_box"
require"scripts/map_manager"
local dictionary_manager = require"scripts/dictionary_manager"
local guess_analyzer = require"scripts/menus/guess_analyzer"
local initial_game = require"scripts/initial_game"

local game_manager = {}

local SETTINGS_FILE = "game_settings.dat"
local DEFAULT_COMMANDS = {
	"action",
	"attack",
	"pause",
	"item_1",
	"item_2",
	"right",
	"up",
	"left",
	"down",
}

local DIFFICULTY_LEVELS = {
	casual = {
		[1] = 30,
		[2] = 28,
		[3] = 26,
		[5] = 24,
		[7] = 22,
		[9] = 20,
		[12] = 18,
		[16] = 16,
		[20] = 14,
		final = 24, --boss
		boss_hp = 3,
		boss_gain = 8, --14 + 8*(3-1) = 30
		save = true,
		continues = true,
		hints = 12,
		monkey_wrenches = false,
	},
	normal = {
		[1] = 30,
		[2] = 26,
		[3] = 22,
		[5] = 20,
		[7] = 18,
		[9] = 16,
		[12] = 14,
		[15] = 12,
		final = 18, --boss
		boss_hp = 4,
		boss_gain = 6, --12 + 6*(4-1) = 30
		save = "previous",
		continues = "previous",
		hints = false,
		monkey_wrenches = true,
	},
	hardcore = {
		[1] = 30,
		[2] = 26,
		[3] = 22,
		[4] = 18,
		[5] = 16,
		[6] = 14,
		[7] = 12,
		[9] = 10,
		final = 12, --boss level
		boss_hp = 6,
		boss_gain = 4, --10 + 4*(6-1) = 30, guesses gained each hit
		save = false,
		continues = 3,
		hints = false,
		time_limit = 10, --minutes
		boss_time = 6, --minutes, time for boss battle
		boss_gain_time = 0.5, --minutes, extra time for each hit
		monkey_wrenches = true,
	},
}
--populate all levels from 1 to final with guess counts
for _,data in pairs(DIFFICULTY_LEVELS) do
	local guesses = 30
	for i = 1,data.final do
		if not data[i] then
			data[i] = guesses
		else guesses = data[i] end
	end
end

local MAP_LIST = {
	"map1",
	"map2",
	"map3",
	"map4",
	"map5",
	"map6",
	final = "boss",
}
for i,map_id in ipairs(MAP_LIST) do MAP_LIST[map_id] = i end --reverse lookup


--## Savegame File ##--

--// Loads existing game or creates new one for given difficulty
function game_manager.start(difficulty)
	local savegame_name = difficulty..".dat"
	local exists = sol.game.exists(savegame_name)
	
	--create the game but do not start it
	local game = sol.game.load(savegame_name)
	if not exists then --This is a new savegame file.
		local continues = (DIFFICULTY_LEVELS[difficulty] or {}).continues
		local num_continues = tonumber(continues)
		initial_game:initialize_new_savegame(game, num_continues)
	end
	
	--internal variables
	local dictionary --(table, array) list of valid words, do not modify contents
	local dictionary_language --(string) language id of the dictionary
	local char_list = {} --(table, combo) list of valid characters (string) / chars (string) as keys with value of true
	
	function game:get_difficulty() return difficulty end
	
	--// Returns the dictionary language id and number of entries in the currently loaded dictionary, or nil if no dictionary is loaded
	function game:get_dictionary() return dictionary_language, #dictionary end
	
	--// Custom iterator to get each dictionary word (does not expose internal table)
		--usage: for i,word in game:iter_dictionary() do
	function game:iter_dictionary()
		if not dictionary then return end
		local iter,_,start_val = ipairs(dictionary)
		return function(_,i) return iter(dictionary, i) end, {}, start_val
	end
	
	--// Returns randomly selected dictionary word (string)
	function game:random_word()
		assert(dictionary, "Error in 'random_word': Dictionary not loaded")
		return dictionary[math.random(#dictionary)]
	end
	
	--// Checks whether the specified word is present in the active dictionary
		--returns boolean - true: the guess is in the dictionary, false: the guess is not in the dictionary
	function game:is_valid_word(word)
		assert(type(word)=="string", "Bad argument #2 to 'is_valid_word' (string expected)")
		assert(dictionary, "Error in 'is_valid_word': Dictionary not loaded")
		return not not dictionary[word]
	end
	
	function game:get_max_guesses(level)
		level = tonumber(level) or self:get_value"level"
		local data = DIFFICULTY_LEVELS[difficulty]
		if data then
			return data[level]
		else return 30 end --default to 30 guesses, e.g. tutorial
	end
	
	function game:get_property(id)
		local data = DIFFICULTY_LEVELS[difficulty] or {}
		return data[id]
	end
	
	function game:get_map_id(level)
		local map_id
		
		level = tonumber(level)
		assert(type(level)=="number", "Bad argument #1 to 'get_map_id' (number expected)")
		level = math.floor(level)
		assert(level>0, "Bad argument #1 to 'get_map_id', number must be positive")
		
		local game_info = DIFFICULTY_LEVELS[difficulty] or {}
		
		local final = game_info.final
		if level<final then --regular level progression
			map_id = MAP_LIST[(level - 1) % #MAP_LIST + 1]
		elseif level==final then --final level
			map_id = MAP_LIST.final
		else map_id = false	end --finished final level
		
		return map_id
	end
	
	--// returns true if player is allowed to continue playing
	function game:regress_stage()
		local continues = (DIFFICULTY_LEVELS[difficulty] or {}).continues
		local num_continues = tonumber(continues)
		
		local is_save = (DIFFICULTY_LEVELS[difficulty] or {}).save or false
		
		if num_continues then
			if (game:get_value"continues" or 0) >= 1 then
				return true
			else --no more continues
				game:set_value("continues", 0)
				if not is_save then game_manager.delete(difficulty) end --erase savegame
				return false
			end
		elseif continues=="previous" then
			local level = game:get_value"level" or 1
			if level > 1 then
				local new_level = level - 1
				game:set_value("level", new_level)
				
				--set map id
				local map_index = (new_level - 1)%#MAP_LIST + 1
				local map_id = MAP_LIST[map_index]
				game:set_starting_location(map_id)
			end
			return true
		elseif continues==true then return true end
	end
	
	game:register_event("on_started", function(self)
		--disable default keyboard bindings
		for _,command in ipairs(DEFAULT_COMMANDS) do
			self:set_command_keyboard_binding(command, nil)
		end
		
		sol.menu.start(self, guess_analyzer)
		game.guess_analyzer = guess_analyzer
	end)
	
	--TODO trigger first time only
	game:register_event("on_map_changed", function(self)
		
	end)
	
	function game:on_paused()
		
	end
	
	function game:on_unpaused()
		
	end
	
	function game:set_custom_command_effect() end --do nothing
	
	dictionary, dictionary_language, char_list = dictionary_manager:load()
	
	return game
end

--// Deletes savegame file
function game_manager.delete(difficulty)
	local savegame_name = difficulty..".dat"
	if sol.game.exists(savegame_name) then sol.game.delete(savegame_name) end
end

--// Discards existing savegame and starts a new game for the given difficulty
function game_manager.new(difficulty)
	game_manager.delete(difficulty)
	
	return game_manager.start(difficulty)
end

local game_meta = sol.main.get_metatable"game"
function game_meta:on_codeword_changed(codeword)
	guess_analyzer:update_guesses()
	guess_analyzer:clear_slate()
end

function game_meta:save_game()
	local game = self
	
	local is_save = (DIFFICULTY_LEVELS[game:get_difficulty()] or {}).save
	if is_save then
		local current_level = game:get_value"level"
		if current_level>1 then  --don't save until past first level
			game:save()
			return true
		end
	end
	
	return false
end

return game_manager

--[[ Copyright 2019-2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
