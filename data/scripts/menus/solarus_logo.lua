--[[solarus_logo.lua
	version 1.0
	3 Jun 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This menu script displays splash screen animation of the Solarus logo then exits.
	
	Based on original menu by Maxs (modified to use larger sprites and code clean-up).
]]

require"scripts/coroutine_helper"

local menu = {}

local SUN_WIDTH,SUN_HEIGHT --values get assigned when sprites are created

local title, subtitle, sun, sword --sprites
local sun_offset = {x=0,y=0} --sets vertical position of sun sprite and how much is drawn
local is_title_visible = false --changes to true when title text is revealed

function menu:start_animation()
	--reset to starting positions
	sun_offset.y = SUN_HEIGHT --start with sun below horizon
	title:fade_out(0)
	subtitle:fade_out(0)
	sun:fade_in(0)
	sword:fade_in(0)
	
	sol.menu.start_coroutine(self, function() --1910 ms total
		--sun rising movement
		local movement = sol.movement.create"straight"
		movement:set_speed(160)
		movement:set_angle(math.pi/2) --sun moves up
		movement:set_max_distance(SUN_HEIGHT)
		start_movement(movement, sun_offset) --540 ms
		
		--instantly reveal title and subtitle text
		if not is_title_visible then
			title:fade_in(0)
			subtitle:fade_in(0)
			is_title_visible = true
		end
		wait(750)
		
		--fade out everything
		title:fade_out()
		subtitle:fade_out()
		sun:fade_out()
		sword:fade_out()
		wait(620)
		
		sol.menu.stop(self)
	end)
	
end

--// Creates sprites and starts animation
function menu:on_started()
	title = sol.sprite.create"menus/solarus_logo"
	title:set_animation"title"
	title:set_xy(0,0)
	
	subtitle = sol.sprite.create"menus/solarus_logo"
	subtitle:set_animation"subtitle"
	subtitle:set_xy(0,98)
	
	sun = sol.sprite.create"menus/solarus_logo"
	sun:set_animation"sun"
	sun:set_xy(62,0)
	
	sword = sol.sprite.create"menus/solarus_logo"
	sword:set_animation"sword"
	sword:set_blend_mode"multiply"
	sword:set_xy(62,0)
	
	SUN_WIDTH, SUN_HEIGHT = sun:get_size()
	self:start_animation()
end

--// Discard sprites
function menu:on_finished()
	title = nil
	subtitle = nil
	sun = nil
	sword = nil
end

--// Pressing any key reveals title and subtitle text early
function menu:on_key_pressed(key)
	if not is_title_visible then
		title:fade_in(0)
		subtitle:fade_in(0)
	end
	return true
end

function menu:on_draw(dst_surface)
	local width,height = dst_surface:get_size()
	local offset_x = width/2 - 256
	local offset_y = 0.75*height/2 - 65
	
	sun:draw_region(
		0, 0,
		SUN_WIDTH, SUN_HEIGHT - sun_offset.y,
		dst_surface,
		offset_x, offset_y + sun_offset.y
	)
	sword:draw(dst_surface, offset_x, offset_y)
	title:draw(dst_surface, offset_x, offset_y)
	subtitle:draw(dst_surface, offset_x, offset_y)
end

return menu
