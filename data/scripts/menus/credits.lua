--[[ credits.lua
	version 0.1a1
	7 Jun 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This menu script displays the credits sequence at the end of the game.
	
	Usage:
	credits:start_dialog"credits.text.original"
	
	Credits Syntax (in dialogs.dat):
	* Regular lines left justified
	* For two columns center justified:
	  first|second
	* For two columns inside justified:
	  role:name
	* For center justified header of a block:
	  #header:
	  #header|
	  (use : or |, matching what is used by other lines in that block)
	* Empty lines separate blocks
]]

local ustring = require"scripts/lib/ustring/ustring"
local ui = require"scripts/menus/ui/ui"

local menu = {}
local dialog_id
local dialog
local finished_cb

--convenience
local get_text_size = sol.text_surface.get_predicted_size

--constants
local FONT = "AndikaNewBasic/AndikaNewBasic-Bold"
local FONT_SIZE = 16
local JUMBLE_SIZE = 16 --font size of jumble text
local SEP_WIDTH = 16 --width of empty space to put in place of : or | separator
local _,TEXT_HEIGHT = get_text_size(FONT,FONT_SIZE,"")
local CREDITS_X = 240 --x position of left-most edge of credits text
local CREDITS_WIDTH = 508 --max width of credits text
local LETTER_SPEED = 320 --speed of letter movements in pixels per second
local FONT_SPACE_WIDTH --width of space character text in pixels
do
	local width1 = get_text_size(FONT,FONT_SIZE,"a")
	local width2 = get_text_size(FONT,FONT_SIZE," a")
	FONT_SPACE_WIDTH = width2 - width1
end

local line_index = 1
local is_hide = {}
local bg_surface = sol.surface.create()
bg_surface:fill_color{0,0,0}

local function get_distance(x1, y1, x2, y2)
	local dx = x2 - x1
	local dy = y2 - y1
	return math.sqrt(dx*dx + dy*dy)
end

local is_appended = false
local persistent_letters = {}
local scroll_letters = {x=0,y=0} --transfer moving letters here after movement then scroll up off screen
local scroll_offset = 0

--// Used to create text surfaces for each individual character (including match value) of jumble text
local all_letters = {} --(table, key/value) string characters as keys, where value is a list of all text surfaces that have that letter
local jumble_letters = {}
local function create_letter(character)
	local text_surface = sol.text_surface.create{
		font = "AndikaNewBasic/AndikaNewBasic-Bold",
		font_size = JUMBLE_SIZE,
		font_color = {0, 0, 0}, --black
		rendering_mode = "antialiasing",
		horizontal_alignment = "left",
		vertical_alignment = "middle",
		font_hinting = "light",
		text = character
	}
	
	--add text surface to list of all letters
	if not all_letters[character] then all_letters[character] = {} end
	local list = all_letters[character]
	list[#list+1] = text_surface
	jumble_letters[#jumble_letters+1] = text_surface
	text_surface:fade_out(0)
	is_hide[text_surface] = true
	
	return text_surface
end

--// Used to assign movements to all_letters, moving text surfaces from jumble text to credits text; jumble letters selected at random
local moving_letters = {}
local orphaned_letters = {}
local function assign_letter(character, x, y)
	local new_letter
	
	local list = all_letters[character]
	if list then
		new_letter = table.remove(list, math.random(#list))
		assert(new_letter, "Cannot find letter: "..character)
		local entry = {text=new_letter, dest_x=x, dest_y=y}
		moving_letters[#moving_letters + 1] = entry
		
		entry.distance = get_distance(x, y, new_letter:get_xy())
		entry.grow_count = FONT_SIZE - JUMBLE_SIZE
		entry.step_time = 10*math.floor(entry.distance*100/LETTER_SPEED/entry.grow_count) --in ms, multiple of 10ms
		
		local movement = sol.movement.create"target"
		movement:set_speed(LETTER_SPEED)
		movement:set_target(x,y)
		entry.movement = movement
	else --create new letter that fades in without movement
		new_letter = sol.text_surface.create{
			font = "AndikaNewBasic/AndikaNewBasic-Bold",
			font_size = FONT_SIZE,
			color = {255, 255, 255}, --black
			rendering_mode = "antialiasing",
			horizontal_alignment = "left",
			vertical_alignment = "middle",
			font_hinting = "light",
			text = character
		}
		new_letter:set_xy(x,y)
		new_letter:fade_out(0)
		is_hide[new_letter] = true
		local entry = {text=new_letter, dest_x=x, dest_y=y}
		orphaned_letters[#orphaned_letters+1] = entry
	end
	
	return new_letter
end

--// Creates a series of letters from a string at the given position
local function assign_string(text, x, y)
	local pos_x = x
	local is_persistent = false
	for i = 1,ustring.len(text) do --UNICODE multi-codepoint
		local char_i = ustring.sub(text,i,i) --UNICODE multi-codepoint
		if not char_i:match"%s" then --UNICODE multi-codepoint
			if char_i ~= "$" then
				local new_letter = assign_letter(char_i, pos_x, y)
				local char_width = get_text_size(FONT,FONT_SIZE,char_i)
				pos_x = pos_x + char_width
				
				if is_persistent then
					persistent_letters[new_letter] = true
				end
				is_persistent = false
			else is_persistent = true end --next letter is persistent
		else pos_x = pos_x + FONT_SPACE_WIDTH end
	end
end

local function grow_letter(text_surface, time, max_count)
	local count = 0
	local font_size = text_surface:get_font_size()
	
	sol.timer.start(menu, time, function()
		count = count + 1
		font_size = font_size + 1
		text_surface:set_font_size(font_size)
		
		return count < max_count
	end)
end

--// Starts the credits using the specified dialog id
	--id (string) - id of the first dialog to use in the credits
	--callback (function, optional) - callback function to be called when the credits have finished
		--default: performs hard reset if no callback specified
function menu:start_dialog(id, callback)
	assert(type(id)=="string", "Bad argument #2 to 'show_dialog' (string expected)")
	assert(not sol.menu.is_started(self), "Error in 'show_dialog', credits menu is already started")
	
	assert(not callback or type(callback)=="function", "Bad argument #3 to 'start_dialog' (function or nil expected)")
	
	--start a game if not already running so sol.main.reset() works --TODO bug solarus#1475
	if not sol.main:get_game() then
		local game = sol.game.load"credits"
		game:set_starting_location("void", "start")
		game:start()
	end
	
	finished_cb = callback
	
	dialog_id = id
	sol.menu.start(sol.main, self)
end

function menu:next_dialog()
	assert(dialog_id, "Error in 'next_dialog', next dialog has not been set")
	
	--TODO remove data from prev dialog
	
	--## Generate guess bank jumble ##--
	
	--jumble text is located in corresponding dialog id with same prefix and suffix, but substitutes "jumble" in middle
	local prefix,suffix = ustring.match(dialog_id, "^([^%.]+)%.[^%.]+%.([^%.]+)$") --UNICODE multi-codepoint
	local jumble_id = prefix..".jumble."..suffix
	local jumble_dialog = sol.language.get_dialog(jumble_id)
	assert(jumble_dialog, "Error in 'show_dialog', corresponding dialog not found: "..jumble_id)
	local jumble_text = jumble_dialog.text
	
	jumble_text = jumble_text:gsub("\r\n", "\n"):gsub("\r", "\n") --standardize line breaks
	
	--generate text surfaces for each letter and match count
	local line_count = 1 --current line to draw
	for line in jumble_text:gmatch"([^\n]*)\n" do --each line including empty ones
		--extract word and match count
		local word,match = ustring.match(line, "^([^%s%d]+)%s+(%d+)$") --UNICODE multi-codepoint
		local word_width = get_text_size(FONT,JUMBLE_SIZE,word)
		
		--determine row and col of this guess entry
		local col = 1 - line_count%2 --first column is 0
		local row = math.floor((line_count-1)/2) --first row is 0
		
		--// generate text surfaces for each letter of word
		
		local pos_x = 104*col + 56 - math.floor(word_width/2)
		local pos_y = 24*row + 32
		
		for i = 1,ustring.len(word) do --UNICODE multi-codepoint
			local new_letter = create_letter(ustring.sub(word,i,i)) --UNICODE multi-codepoint
			new_letter:set_xy(pos_x, pos_y)
			
			--find x position of next letter
			local letter_width = new_letter:get_size()
			pos_x = pos_x + letter_width
		end
		
		--// generate text surface for match count
		
		local new_letter = create_letter(match)
		local letter_width = new_letter:get_size()
		new_letter:set_xy(104*col + 104 - math.floor(letter_width/2), pos_y)
		
		line_count = line_count + 1 --increment for next line
		if line_count > 32 then break end --limit to 32 lines
	end
	
	--## Generate dialog text ##--
	
	dialog = sol.language.get_dialog(dialog_id)
	assert(dialog, "Bad argument #2 to 'show_dialog', dialog does not exist: "..dialog_id)
	local text = dialog.text
	
	local pos_y = (line_index-1)*TEXT_HEIGHT + 64 --position of top-most line in pixels
	local pos_x = CREDITS_X
	
	text = text:gsub("\r\n", "\n"):gsub("\r", "\n") --standardize line breaks
	
	local queue = {} --store lines with same separator then draw all at once for correct horizontal spacing
	local current_sep --(string) separator character used in this block of text, changing separator starts a new block
	local width1_max, width2_max = 0,0 --(number, non-negative integer) longest width of part1 & part2 among queued lines (in pixels)
	local function add_queue(data)
		queue[#queue+1] = data --add new entry
		
		local part1 = data[1]
		local sep = data[2]
		local part2 = data[3]
		local part1_width = data[4]
		local part2_width = data[5]
		local is_head = data[7]
		
		if is_head then
			local width = 0.5*(part1_width - SEP_WIDTH)
			if width>width1_max then width1_max = width end
			if width>width2_max then width2_max = width end
		else
			if part1_width>width1_max then width1_max = part1_width end
			if part2_width>width2_max then width2_max = part2_width end
		end
	end
	
	local function process_queue()
		local mult1, mult2 --multipliers to set horizontal justification (0=left, 0.5=center, 1=right)
		if current_sep==":" then
			mult1 = 1 --right justified
			mult2 = 0 --left justified
		elseif current_sep=="|" then
			mult1 = 0.5 --center justified
			mult2 = 0.5 --center justified
		end
		
		for _,entry in ipairs(queue) do
			local part1 = entry[1]
			local part2 = entry[3]
			local part1_width = entry[4]
			local part2_width = entry[5]
			local pos_y = entry[6]
			local is_head = entry[7]
			
			if is_head then
				local delta_x1 = SEP_WIDTH - part1_width
				local x1 = CREDITS_X + width1_max + math.floor(0.5*delta_x1)
				assign_string(part1, x1, pos_y)
			else
				local delta_x1 = width1_max - part1_width
				local x1 = CREDITS_X + math.floor(mult1*delta_x1)
				assign_string(part1, x1, pos_y)
				
				local delta_x2 = width2_max - part2_width
				local x2 = CREDITS_X + width1_max + SEP_WIDTH + math.floor(mult2*delta_x2)
				assign_string(part2, x2, pos_y)
			end
		end
		
		queue = {} --clear queue
	end
	
	local offset_y = 0
	for line in text:gmatch"([^\n]*)\n" do --each line including empty ones
		local head,part1,sep,part2 = ustring.match(line,  --UNICODE multi-codepoint
			"^%s*(%#?)([^%:%|]*)([%:%|]?)([^%:%|]*)"
		)
		part1 = ustring.match(part1, "^%s*(.-)%s*$") --remove leading and trailing whitespace --UNICODE multi-codepoint
		local part1_width = get_text_size(FONT,FONT_SIZE,part1)
		local part2_width = 0 --tentative
		
		if current_sep ~= sep then --start a new block
			process_queue()
			current_sep = sep
		end
		
		if sep~="" then
			part2 = ustring.match(part2, "^%s*(.-)%s*$") --remove leading and trailing whitespace --UNICODE multi-codepoint
			part2_width = get_text_size(FONT,FONT_SIZE,part2)
			add_queue{part1,sep,part2,part1_width,part2_width,pos_y,#head>0}
		else assign_string(part1,pos_x,pos_y) end --render this line immediately, left justified
		
		--prepare for next line
		pos_y = pos_y + TEXT_HEIGHT
		line_index = line_index + 1
	end
	
	process_queue()
	
	--## Begin menu animations ##--
	
	local index_start = 1
	local last_movement
	
	sol.timer.start(self, 300, function() --initial delay on black screen
		local is_repeat = false --tentative
		local pos_y
		
		for i = index_start,#jumble_letters do
			local control = jumble_letters[i]
			local _,control_y = control:get_xy()
			if not pos_y then pos_y = control_y end
			
			if control_y <= pos_y then
				is_hide[control] = nil
				control:fade_in(30)
			else
				index_start = i --resume at this line next cycle
				is_repeat = 200 --delay between revealing letters on each line of jumble
				break --remaining letters are at lower y position
			end
		end
		
		if not is_repeat then
			index_start = 1
			
			sol.timer.start(self, 1000, function() --delay before initial movement start
				sol.timer.start(self, 300, function() --delay before starting movement for each subsequent line of credits dialog
					local is_repeat = false --tentative; repeats until last line of dialog reached
					local pos_y --only apply movement for letters at this y position
					
					--start movement for letters on current line
					for i = index_start,#moving_letters do
						local control = moving_letters[i]
						if not pos_y then pos_y = control.dest_y end
						
						if control.dest_y <= pos_y then
							last_movement = control.movement
							last_movement:start(control.text)
							grow_letter(control.text, control.step_time, control.grow_count)
						else
							index_start = i --resume at this line next cycle
							is_repeat = true
							break --remaining letters are at lower y position
						end 
					end
					
					--stuff to do after all movements started
					if not is_repeat then
						local self_menu = self --retain reference to self because will be overridden in following function
						function last_movement:on_finished() --called when the last movement to start finishes (not necessarily last movement to finish)
							--fade out jumble letters that didn't move
							for _,char_list in pairs(all_letters) do
								for _,control in ipairs(char_list) do
									control:fade_out(50)
								end
							end
							
							--scroll credits text up out of view
							local SCROLL_DELAY = dialog.append and 1500 or 2500
							sol.timer.start(self_menu, SCROLL_DELAY, function() --delay before starting upward scroll
								if not is_appended then
									--persistent_letters = {} --clear previous
									scroll_letters = {x=0,y=0} --clear previous
									scroll_offset = 0
								end
								
								--copy letters over to scroll_letters in preparation for next iteration
								local i = #scroll_letters + 1
								for _,control in ipairs(moving_letters) do
									scroll_letters[i] = control.text
									i = i + 1
								end
								for _,char_list in pairs(all_letters) do
									for _,control in ipairs(char_list) do
										scroll_letters[i] = control
										i = i + 1
									end
								end
								for _,control in ipairs(orphaned_letters) do
									scroll_letters[i] = control.text
									i = i + 1
								end
								
								--reset tables
								moving_letters = {}
								jumble_letters = {}
								all_letters = {}
								orphaned_letters = {}
								
								local next_dialog = dialog.append
								if not next_dialog then
									is_appended = false
									line_index = 1
									next_dialog = dialog.next
									local dy = -432
									local cb
									
									local is_fade_out = next_dialog == "$FADE_OUT"
									if is_fade_out then
										next_dialog = nil
										dy = -216
										cb = function()
											sol.timer.start(self_menu, 1000, function()
												for _,control in ipairs(scroll_letters) do
													if not persistent_letters[control] then
														control:fade_out(80)
													end
												end
												
												sol.timer.start(self_menu, 10000, function()
													sol.menu.stop(self_menu)
												end)
											end)
										end
									end
									
									--scroll text off screen
									local movement = sol.movement.create"target"
									movement:set_speed(120)
									movement:set_target(0,dy)
									movement:start(scroll_letters, cb)
								else line_index = line_index + 1; is_appended = true end
								
								if next_dialog then
									dialog_id = next_dialog
									self_menu:next_dialog()
								end
							end)
						end
					end
					
					return is_repeat --repeat timer until movements for all letters started
				end)
				
				--fade in dialog characters that aren't present in jumble (usually symbols)
				sol.timer.start(self, 1000, function()
					for _,control in ipairs(orphaned_letters) do
						is_hide[control.text] = nil
						control.text:fade_in()
					end
				end)
			end)
		end
		
		return is_repeat --repeat timer until all letters shown
	end)
end

function menu:on_started()
	sol.audio.stop_music()
	sol.audio.play_music("Pixelin'_It_Around_Town_Standard", false)
	
	sol.timer.start(self, 500, function() --initial delay on black screen
		self:next_dialog()
	end)
end

function menu:on_finished()
	if finished_cb then
		finished_cb()
	else sol.main.reset() end
end

function menu:on_draw(dst_surface)
	bg_surface:draw(dst_surface)
	for _,control in ipairs(moving_letters) do
		if not is_hide[control.text] then
			control.text:draw(dst_surface, control.x, control.y)
		end
	end
	
	for _,char_list in pairs(all_letters) do
		for _,control in ipairs(char_list) do
			if not is_hide[control] then
				control:draw(dst_surface, control.x, control.y)
			end
		end
	end
	
	for _,control in ipairs(orphaned_letters) do
		if not is_hide[control.text] then
			control.text:draw(dst_surface, control.x, control.y)
		end
	end
	
	for _,control in ipairs(scroll_letters) do
		control:draw(dst_surface, control.x, (control.y or 0)+scroll_letters.y)
	end
end

return menu

--[[ Copyright 2019-2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
