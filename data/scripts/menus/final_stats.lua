--[[final_stats.lua
	version 0.1a1
	7 Jun 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This menu script displays various stats at the end of the game after the credits.
]]

local menu = {}

local text_surfaces = {}
local finished_cb

local FONT = "AndikaNewBasic/AndikaNewBasic-Bold"
local FONT_SIZE = 24
local FONT_COLOR = {255, 255, 255}

local STATS = {
	"difficulty_mode",
	"total_playtime",
	"total_guesses",
	"total_hints",
	"total_anagrams",
	"total_retries",
	"fastest_time",
	"least_guesses",
}

function menu:start(context, stats_data, callback)
	text_surfaces = {} --clear previous
	
	local title = sol.text_surface.create{
		font = FONT,
		font_size = FONT_SIZE*1.25,
		font_color = FONT_COLOR,
		rendering_mode = "antialiasing",
		horizontal_alignment = "center",
		vertical_alignment = "top",
		font_hinting = "light",
		text_key = "stats.title",
	}
	title:set_xy(384, 16)
	text_surfaces[1] = title
	
	--create text surfaces
	local index = 1
	for _,stat_key in ipairs(STATS) do
		local data = stats_data[stat_key]
		if data then
			local text_surface = sol.text_surface.create{
				font = FONT,
				font_size = FONT_SIZE,
				font_color = FONT_COLOR,
				rendering_mode = "antialiasing",
				horizontal_alignment = "center",
				vertical_alignment = "top",
				font_hinting = "light",
				text = string.format(
					sol.language.get_string("stats."..stat_key) or "",
					data
				),
			}
			text_surface:set_xy(384, 40+32*index)
			index = index + 1
			text_surfaces[index] = text_surface
		end
	end
	
	finished_cb = callback
	
	sol.menu.start(context, self)
end

function menu:on_started()
	sol.timer.start(self, 10000, function()
		sol.menu.stop(self)
	end)
end

function menu:on_finished()
	if finished_cb then finished_cb() end
end

function menu:on_draw(dst_surface)
	dst_surface:fill_color{0,0,0}
	for _,text_surface in ipairs(text_surfaces) do
		text_surface:draw(dst_surface)
	end
end

return menu

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
