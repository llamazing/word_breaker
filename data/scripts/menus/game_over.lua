--[[game_over.lua
	version 1.0
	30 May 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script displays the game over menu for Word Breaker.
]]

local ustring = require"scripts/lib/ustring/ustring"
local menus = require"scripts/menus/menus"
require"scripts/coroutine_helper"

local menu = {}

--available colors for each letter of title (chosen randomly), RGB values 0-255
local LETTER_COLORS = {
	{235, 6, 89},
	{0, 127, 181},
	{193, 0, 255},
	{0, 139, 0},
	{195, 164, 4},
	{255, 104, 3},
}

local PI = math.pi
local math__random = math.random

local TITLE_FONT
local TITLE_FONT_SIZE
local TITLE_TEXT
local CONTINUES_FONT = "SourceSansPro/SourceSansPro-Semibold"
--local CONTINUES_FONT = "AndikaNewBasic/AndikaNewBasic-Bold"
local CONTINUES_FONT_SIZE = 20

local title_letters = {} --array of surfaces for each letter of title
local continue_text
local is_continues = false

--// Creates up and down wobble movements for individual title letters (recursive)
local create_movt
function create_movt(surface)
	local movement = sol.movement.create"straight"
	movement:set_speed(math__random(3,4))
	local dir = surface.dir
	movement:set_angle(PI/2*dir)
	local offset_y = surface.offset_y
	local new_offset = 2 + math__random(1,2) + math__random(1,2)
	movement:set_max_distance(offset_y + new_offset)
	surface.offset_y = new_offset
	surface.dir = 4 - dir --toggle between 3 & 1
	movement:start(surface, function() create_movt(surface) end)
end

--// Assigns a drop movement to the title letter with specified index after random delay
	--index (number, positive integer) - index of the title letter, spaces skipped
local function drop_movement(index)
	local delay = (math__random(12) + math__random(12))*10
	sol.timer.start(menu, delay, function()
		local movement = sol.movement.create"straight"
		local surface = title_letters[index]
		
		local speed = 80 + 20*(math__random()+math__random())
		movement:set_speed(speed)
		movement:set_angle(3*PI/2)
		local offset_y = math__random(2,8)
		surface.offset_y = offset_y
		surface.dir = 1
		movement:set_max_distance(130+offset_y)
		movement.target = surface
		movement:start(surface, function() create_movt(surface) end)
	end)
end

--// Creates individual surfaces for each letter in title, gets called when menu starts or language is changed
local function create_letters()
	title_letters = {} --clear previous
	
	TITLE_FONT = sol.language.get_string"game_over.banner_font" or "Sniglet/Sniglet Regular"
	TITLE_FONT_SIZE = tonumber(sol.language.get_string"game_over.banner_font_size") or 100
	TITLE_TEXT = sol.language.get_string"game_over.banner" --always get when menu starts in case changed languages
	
	--temporarily create text surface of title
	local text_surface = sol.text_surface.create{
		font = TITLE_FONT,
		font_size = TITLE_FONT_SIZE,
		color = {255, 255, 255, 255},
		horizontal_alignment = "left",
		vertical_alignment = "top",
		rendering_mode = "antialiasing",
		font_hinting = "light",
		text = TITLE_TEXT,
	}
	
	--create individual surface for each letter of title
	local x_start = 0
	local x_origin = (sol.video.get_quest_size() - text_surface:get_size())/4 --left-most edge of banner text, center horizontally
	local prev_color_index
	local i = ustring.find(TITLE_TEXT, "%S", i) --UNICODE multi-codepoint (may need optional glyph separator character)
	while i and i <= ustring.len(TITLE_TEXT) do
		local subtext = ustring.sub(TITLE_TEXT,1,i) --UNICODE multi-codepoint
		local x_end, height = sol.text_surface.get_predicted_size(TITLE_FONT, TITLE_FONT_SIZE, subtext)
		local width = x_end - x_start
		
		local surface = sol.surface.create(width, height)
		text_surface:draw_region(
			x_start, 0,
			width, height,
			surface
		)
		surface.x_offset = x_start
		surface:set_xy(x_origin, -height)
		
		--choose different color than letter to left at random
		local color_index
		if prev_color_index then
			color_index = math__random(#LETTER_COLORS - 1)
			if color_index >= prev_color_index then color_index = color_index + 1 end
		else color_index = math__random(#LETTER_COLORS) end
		surface:set_color_modulation(LETTER_COLORS[color_index])
		surface.color_index = color_index
		prev_color_index = color_index
		
		table.insert(title_letters, surface)
		--modulate_color(#title_letters)
		
		x_start = x_end
		i = ustring.find(TITLE_TEXT, "%S", i+1) --UNICODE multi-codepoint
	end
	
	--create continue remaining text
	local game = sol.main.get_game()
	local text = sol.language.get_string"game_info.continues_remaining"
	local num_continues = tonumber(game:get_value"continues")
	if num_continues then is_continues = true end
	text = string.format(text, num_continues or 99)
	
	continues_text = sol.text_surface.create{
		font = CONTINUES_FONT,
		font_size = CONTINUES_FONT_SIZE,
		color = {255, 255, 255, 255},
		horizontal_alignment = "center",
		vertical_alignment = "top",
		rendering_mode = "antialiasing",
		font_hinting = "light",
		text = text,
	}
	continues_text:set_xy(384, 396)
	continues_text:fade_out(0) --start invisible
end

function menu:on_language_changed()
	create_letters()
end

function menu:on_started()
	create_letters()
	
	sol.menu.start_coroutine(self, function()
		sol.audio.play_music("Vast_Places_Looping", true)
		wait(500)
		for i=1,#title_letters do drop_movement(i) end --drop each letter with slight timing variances
		wait(1500)
		if is_continues then continues_text:fade_in(40) end
	end)
end

function menu:on_draw(dst_surface)
	dst_surface:fill_color{0,0,0}
	for i,surface in ipairs(title_letters) do
		local x,y = surface:get_xy()
		surface:draw(dst_surface, x+surface.x_offset, y)
	end
	continues_text:draw(dst_surface)
end

return menu

--[[ Copyright 2019-2020 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
