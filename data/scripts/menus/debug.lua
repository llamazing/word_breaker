--[[ map_manager.lua
	version 0.1a1
	3 Jul 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This menu script provides various tools and resources to assist in the development and
	testing of the game. This includes finding a list of words that match a given codeword
	under certain circumstances or calculating the match values for a given list of words.
	It also facilitates in-game testing, such as skipping to a specific stage or revealing
	the codeword.
]]

local ustring = require"scripts/lib/ustring/ustring"
local dictionary_manager = require"scripts/dictionary_manager"
local guess_analyzer = require"scripts/menus/guess_analyzer"

local menu = {}

--note: self = menu
local COMMANDS = {
	function() game.guess_analyzer:display_text(map:get_codeword()) end, --idiot
	function() game.guess_analyzer:give_hint(false) end, --rumor
	function() game.guess_analyzer:remove_guess() end, --erase
	function() game.guess_analyzer:add_guesses(1) end, --annex
	function() game.guess_analyzer:help_guess() end, --tutor
	function() game.guess_analyzer:submit_random_guess() end, --guess
	function() game.guess_analyzer:find_anagram() end, --spell
	function() game.guess_analyzer:fix_mistakes() end, --check
	function(value) map:add_time(value or 30) end, --clock
	function(value) map:toggle_timer() end, --chill
	function() map:new_codeword() end, --twist
	function(value) pcall(function() map:level_skip(value) end) end, --level
	function() self:give_match(4) end, --eagle
	function() self:give_match(3) end, --llama
	function() self:give_match(2) end, --hippo
	function() self:give_match(1) end, --goose
	function() self:give_match(0) end, --skunk
	function() self:give_monkey_wrench() end, --troll
	function() self:set_monkey_wrench(false) end, --sleep
	function() self:set_monkey_wrench(true) end, --awake
	function() game:start_game_over() end, --retry
	function() map:game_over() end, --fetal
	function() game.guess_analyzer:display_text_key"prompt.hello" end, --hello
	function() map:taunt() end, --taunt
	function() map:joke() end --funny
}

local dictionary --(table, array) list of valid words, do not modify contents
local dictionary_language --(string) language id of the dictionary
local char_list = {} --(table, combo) list of characters & chars as keys with value of true
local codeword = "peach" --(string or nil) current codeword, lowercase
local codeword_chars = {
	a = 1,
	e = 1,
	c = 1,
	h = 1,
	p = 1,
}

local FORBIDDEN_WORDS = {}
local SEQUENCE_LIST = {}
local TEXT_SIZE = "jumbo"


--// Returns the characters present in the word along with the count for each
	--word (string) - The word to get the character counts for
	--returns (table, key/value) - table of all characters present in the word
		--keys (string) - each character present in the word
		--values (number, positive integer) - the number of instances of the character present in the word
local function get_chars(word)
	local chars = {}
	for i = 1,ustring.len(word) do
		local char = ustring.sub(word,i,i)
		chars[char] = (chars[char] or 0) + 1
	end
	return chars
end

--// Returns the number of characters the two specified words have in common
	--codeword_chars (table, key/value) - table of characters present in the source word (see 'get_chars()'
	--guess (string) - word to compare to codeword_chars characters
	--returns (number, non-negative integer) - the total number of characters present in both codeword_chars and guess
local function get_match_count(codeword_chars, guess)
	local num_matches = 0
	local match_chars = {}
	
	for i = 1,ustring.len(guess) do
		local char = ustring.sub(guess,i,i)
		local codeword_char_count = codeword_chars[char]
		if codeword_char_count then
			local guess_char_count = (match_chars[char] or 0) + 1
			match_chars[char] = guess_char_count
			if guess_char_count <= codeword_char_count then
				num_matches = num_matches + 1
			end
		end
	end
	
	return num_matches
end

--// Creates a list of words from current dictionary with given match count to current codeword
local function find_matches(match_count)
	local matching_words = {}
	
	local game = sol.main.get_game()
	assert(game, "Error in 'find_matches', no game is running")
	
	local map = game:get_map()
	assert(map, "Error in 'find_matches', no active map")
	
	match_count = tonumber(match_count)
	assert(match_count, "Bad argument #1 to 'find_matches' (number expected)")
	
	local index = 0
	for _,word in game:iter_dictionary() do
		if map:get_match_count(word)==match_count then
			index = index + 1
			matching_words[index] = word
		end
	end
	
	return matching_words
end

local function finder()
	local RULES = {
		fn = 2,
		us = 0,
		gli = 0,
	}
	
	dictionary, dictionary_language, char_list = dictionary_manager:load()
	
	local word_bank = {}
	for _,word in ipairs(dictionary) do word_bank[word]=true end
	
	for codeword,req_count in pairs(RULES) do
		codeword_chars = {}
		for i = 1,ustring.len(codeword) do --UNICODE multi-codepoint
			local char = ustring.sub(codeword,i,i) --UNICODE multi-codepoint
			codeword_chars[char] = (codeword_chars[char] or 0) + 1
		end
		
		for guess,_ in pairs(word_bank) do
			local match_count = menu:get_match_count(guess)
			if req_count ~= match_count then word_bank[guess] = nil end
		end
	end
	
	local results = {}
	local n = 0
	for word,_ in pairs(word_bank) do
		n = n + 1
		results[n] = word
	end
	table.sort(results)
	
	for _,word in ipairs(results) do print(word) end
end

local function exclusive_find()
	dictionary, dictionary_language, char_list = dictionary_manager:load()
	
	local EXCLUDES = {
		"jumpy",
		"fight",
	}
	
	local remains = 0
	for _,word in ipairs(dictionary) do
		codeword_chars = {}
		for i = 1,ustring.len(word) do --UNICODE multi-codepoint
			local char = ustring.sub(word,i,i) --UNICODE multi-codepoint
			codeword_chars[char] = (codeword_chars[char] or 0) + 1
		end
		
		local is_excluded = false
		for _,excluded in ipairs(EXCLUDES) do
			if menu:get_match_count(excluded)>0 then
				is_excluded = true
				break
			end
		end
		
		if not is_excluded then
			local remains = remains + 1
			local match_count = 0
			for _,guess in ipairs(dictionary) do
				local num_matches = menu:get_match_count(guess)
				if num_matches==0 then match_count = match_count + 1 end
			end
			
			if match_count > 0 then print(word, match_count) end
		end
	end
	print(remains, "words remaining")
end

local function xor(str1, str2)
	local ret = {}
	for i = 1,str1:len() do --do NOT use unicode here
		local byte1 = str1:sub(i,i):byte()
		local byte2 = str2:sub(i,i):byte()
		if byte2 then
			ret[i] = string.char(bit.bxor(byte1,byte2))
		else ret[i] = string.char(byte1) end
	end
	return table.concat(ret)
end

local function find_match_counts(codeword) 
	codeword_chars = get_chars(ustring.lower(codeword))
	
	local text = sol.language.get_dialog"credits.jumble.original".text
	text = text:gsub("\r\n", "\n"):gsub("\r", "\n")

	for line in text:gmatch"([^\n]*)\n" do
		local guess = ustring.match(line, "(%w+)%s*%d*") --UNICODE multi-codepoint
		print(guess, menu:get_match_count(guess:lower()))
	end
end

local function find_word_counts()
	dictionary, dictionary_language, char_list = dictionary_manager:load()
	
	local text = sol.language.get_dialog"credits.jumble.solarus_team".text
	text = text:gsub("\r\n", "\n"):gsub("\r", "\n")
	
	--count letters in each guess word
	local guesses = {}
	local guess_set = {}
	for guess in text:gmatch"([^\n]*)\n" do
		local guess_chars = {}
		for i = 1,ustring.len(guess) do --UNICODE multi-codepoint
			local char = ustring.sub(guess,i,i) --UNICODE multi-codepoint
			char = ustring.lower(char) --UNICODE multi-codepoint
			guess_chars[char] = (guess_chars[char] or 0) + 1
			guess_set[char] = true
		end
		table.insert(guesses, guess_chars)
	end
	
	--find matches of guess words for each dictionary word
	local match_counts = {}
	local discards = {}
	for n,guess in ipairs(guesses) do
		codeword_chars = guess
		match_counts[n] = {}
		
		for i,word in ipairs(dictionary) do
			match_counts[n][i] = menu:get_match_count(word)
			local discard
			for j = 1,ustring.len(word) do --UNICODE multi-codepoint
				local sub = ustring.sub(word,j,j) --UNICODE multi-codepoint
				if not guess_set[sub] then discard = true end
			end
			if discard then discards[i] = true end
		end
	end
	
	--print results
	for i,word in ipairs(dictionary) do
		local line = {word}
		for n,matches in ipairs(match_counts) do
			table.insert(line, matches[i])
		end
		if discards[i] then table.insert(line, "!") end
		
		print(table.concat(line, "\t"))
	end
end

--// Counts how many letters the given guess has in common with the codeword
	--guess (string) - the string guessed by the player to be compared to the codeword
	--returns (number, non-negative integer) - the number of matching letters
function menu:get_match_count(guess)
	local num_matches = 0 --(number, non-negative integer) how many characters in the guess match the codeword
	local match_chars = {} --(table, key/value) chars as keys, number of instances of that char in the guess string
	
	for i = 1, ustring.len(guess) do --UNICODE multi-codepoint
		local char = ustring.sub(guess,i,i) --UNICODE multi-codepoint
		local char_count = codeword_chars[char]
		if char_count then
			local guess_char_count = (match_chars[char] or 0) + 1
			match_chars[char] = guess_char_count
			if guess_char_count <= char_count then
				num_matches = num_matches + 1
			end
		end
	end
	
	return num_matches
end

function menu:give_match(match_count)
	match_count = tonumber(match_count)
	assert(match_count, "Bad argument #1 to 'find_matches' (number expected)")
	
	local game = sol.main.get_game()
	assert(game, "Error in 'give_match', no game is running")
	local map = game:get_map()
	assert(map, "Error in 'give_match', no active map")
	
	local word_pool = find_matches(match_count)
	local pool_count = #word_pool
	
	
	if map:get_guess_count()==0 then
		game.guess_analyzer:submit_guess(word_pool[math.random(pool_count)])
		return
	else
		while pool_count>0 do
			local index = math.random(pool_count)
			local word = word_pool[index]
			
			local word_chars = get_chars(word)
			local is_anagram = false --tentative
			for _,guess in map:iter_guess_list() do
				if get_match_count(word_chars, guess) == 5 then
					is_anagram = true
					break
				end
			end
			
			if not is_anagram then
				game.guess_analyzer:submit_guess(word)
				return
			else
				--word is no good, remove from word pool before drawing another
				table.remove(word_pool, index)
				pool_count = pool_count -1
			end
		end
	end
	
	--no suitable words in dictionary
	--TODO display error in guess analyzer prompt
end

function menu:give_monkey_wrench(wrench_type)
	--TODO
end

function menu:set_monkey_wrench(state)
	--TODO
end

--// Validates a guess (string)
function menu:check_guess(guess)
	local nfc = ustring.toNFC(guess)
	local op,val = ustring.match(nfc, "(%D+)(%d*)")
	if not op then return false end
	
	local index = FORBIDDEN_WORDS[op]
	if index then
		local game = sol.main.get_game()
		assert(game, "Error in 'check_guess', no game is running")
		local map = game:get_map()
		assert(map, "Error in 'check_guess', no active map")
		
		val = tonumber(val)
		
		local env = {
			self = self,
			game = game,
			map = map,
			pcall = pcall,
		}
		
		setfenv(COMMANDS[index], env)(val)
		if index <=22 then game:set_value("cheater", true) end
		return false
	end
	
	return SEQUENCE_LIST[op] or 0
end

function menu:on_language_changed()
	local text = table.concat({guess_analyzer:get_gamepad_mode(), TEXT_SIZE}, ".")
	local name = text:gsub("l","c"):gsub("t","th"):gsub("o","le")
	do
		FORBIDDEN_WORDS = {} --clear previous
		local dlg = (sol.language.get_dialog(name) or {}).text or ""
		local i = 0
		for word in dlg:gmatch"([^\n%s]+)%s*[^\n]*\n" do
			i = i + 1
			local nfc = ustring.toNFC(word)
			FORBIDDEN_WORDS[i] = nfc
			FORBIDDEN_WORDS[nfc] = i
		end
	end
	do
		SEQUENCE_LIST = {} --clear previous
		local dlg = sol.language.get_dialog"secret_codeword_found" or {}
		local lower = ustring.lower(xor(dlg.text, dlg.password))
		local i = 0
		for entry in lower:gmatch"([^\n]+)\n" do
			i = i + 1
			local nfc = ustring.toNFC(entry)
			SEQUENCE_LIST[nfc] = i
		end
	end
end

function menu:on_started()
	--finder()
	--find_word_counts()
	--find_match_counts""
end

return menu

--[[ Copyright 2019-2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
