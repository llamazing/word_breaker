--[[ dialog_box.lua
	version 0.1a1
	25 Aug 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This menu script displays a multi-line text box using dialogs.dat entries for the text
	content.
	
	Adds the following events:
	* game_meta:on_started() - called when a game is started and creates the dialog box menu
	* game:on_dialog_started() - called when game:start_dialog() is used, causing the dialog box menu to be started
	* game:on_dialog_finished() - called when game:stop_dialog() is used, causing the dialog box menu to be closed
	* game:dialog_advance() - call after the player has fulfilled the active dialog restriction
	
	Adds the following methods:
	* game.get_dialog_box() - returns instance of the dialog box menu (table)
	
	Usage:
	--TODO usage instructions
	
]]

require"scripts/multi_events"
local ustring = require"scripts/lib/ustring/ustring"
local ui = require"scripts/menus/ui/ui"
local settings = require"scripts/settings"

local dialog_box = {}

--constants --TODO move configuration stuff to separate .dat file
local FONT = "SourceSansPro/SourceSansPro-Semibold"
local FONT_SIZE = 18
local FONT_COLOR = {255, 255, 255} --white
local FONT_HIGHLIGHT_COLOR = {255, 0, 0} --red
local BG_COLOR = {0,0,0,204} --slightly transparent black

local MAX_NUM_LINES = 15 --maximum number of lines of text from among all layouts (create this many text surfaces)
local MAX_LINE_WIDTH = 400 --maximum width for text in pixels, determines where line breaks occur
local LINE_HEIGHT = 24 --vertical offset of each line of text in pixels

local LAYOUTS = {
	top = {
		num_lines = 8,
		min_lines = 7,
		width = 416,
		margin = 8,
		x = 232,
		y = 8,
	},
	bottom = {
		num_lines = 7,
		min_lines = 7,
		width = 416,
		margin = 8,
		x = 232,
		y = -8,
	},
	full = {
		num_lines = 15,
		min_lines = 15,
		width = 416,
		margin = 8,
		x = 232,
		y = 8,
	},
}

--If the next property of a dialog matches one of these keys then call the corresponding
--function at the end of the dialog.
local NEXT_KEYWORDS = {
	['#EXIT_TO_TITLE'] = function()
		--TODO add fade to black first
		settings.set_value("completed_tutorial", true)
		sol.main.reset()
	end,
}

local KEY_ACTIONS = {
	left = "prev_dialog",
	up = "prev_dialog",
	['page up'] = "prev_dialog",
	right = "next_dialog",
	down = "next_dialog",
	['page down'] = "next_dialog",
	escape = false, --ignore and let other menus handle
	DEFAULT = "next_dialog",
}

--convenience
local get_text_size = sol.text_surface.get_predicted_size

--// Creates a new dialog box menu
	--game (sol.game) - the game instance to associate with this dialog box menu
function dialog_box.create(game)
	assert(sol.main.get_type(game)=="game", "Bad argument #1 to 'create' (sol.game expected)")
	--assert(not game.get_dialog_box, "Error: Cannot create dialog box menu because active game already has one")
	if game.get_dialog_box then return end --dialog box already created
	
	local menu = {}
	function game:get_dialog_box() return menu end
	
	local dialog
	local dialog_info
	local layout
	
	local lines = {}
	local text_surfaces = {}
	local text_surfaces2 = {} --TODO hax for Solarus issue #1369
	local highlights = {}
	local surfaces = {}
	local surface_timers = {}
	local bg_surface = sol.surface.create()
	local controls = {} --(table, array) list of all ui controls used in this menu
	local next_button = ui.create_preset"button.dialog"
	local prev_button = ui.create_preset"button.dialog"
	local history --(table, array) list of previously viewed dialogs for going backwards
	local history_index --(number, positive integer) sequence number of the current dialog since dialog opened
	
	local get_next_line --(function) returns the next line of the current dialog as a string
	local line_index
	local leftover_text
	
	--create text surfaces for each line
	for i = 1,MAX_NUM_LINES do
		lines[i] = ""
		
		text_surfaces[i] = sol.text_surface.create{
			font = FONT,
			font_size = FONT_SIZE,
			color = FONT_COLOR,
			rendering_mode = "antialiasing",
			horizontal_alignment = "left",
			vertical_alignment = "top",
		}
		
		--TODO hax for Solarus issue #1369
		text_surfaces2[i] = sol.text_surface.create{
			font = FONT,
			font_size = FONT_SIZE,
			color = FONT_HIGHLIGHT_COLOR,
			rendering_mode = "antialiasing",
			horizontal_alignment = "left",
			vertical_alignment = "top",
		}
		
		surfaces[i] = sol.surface.create(MAX_LINE_WIDTH, LINE_HEIGHT)
	end
	
	controls[#controls+1] = next_button
	next_button.on_clicked = function()
		menu:next_dialog()
	end
	
	controls[#controls+1] = prev_button
	prev_button:set_text(sol.language.get_string"dialog_box.prev")
	prev_button.on_clicked = function()
		menu:prev_dialog()
	end
	
	game:register_event("on_dialog_started", function(self, dlg, info)
		assert(not sol.menu.is_started(menu), "Error: dialog box menu is already started")
		
		--save the dialog and info
		dialog = dlg
		dialog_info = info
		
		--reset history
		history = {}
		history_index = 1
		
		self.advance_dialog = function() menu:next_dialog(true) end
		sol.menu.start(self, menu)
	end)
	
	game:register_event("on_dialog_finished", function(self, _)
		if sol.menu.is_started(menu) then sol.menu.stop(menu) end
		
		dialog = nil
		dialog_info = nil
		layout = nil
		history = nil
		history_index = nil
		
		self.advance_dialog = nil
		self.guess_analyzer:set_access_mode()
	end)
	
	local function draw_pane()
		local quest_width, quest_height = sol.video.get_quest_size()
		local button_width,button_height = next_button:get_size()
		local layout_height = (layout.num_lines)*LINE_HEIGHT + layout.margin*3 + button_height
		local layout_x = (layout.x<0 and (quest_width - layout.width) or 0) + layout.x
		local layout_y = (layout.y<0 and (quest_height - layout_height) or 0) + layout.y
		
		--surface:clear()
		local max_line = 0 --tentative
		for i,line in ipairs(lines) do
			local surface = surfaces[i]
			surface:clear()
			surface:set_xy(
				layout_x + layout.margin,
				LINE_HEIGHT*(i-1) + layout_y + layout.margin
			)
			surface:fade_out(0)
			--surface:set_opacity(0)
			
			local timer = surface_timers[i]
			if timer then timer:stop() end
			surface_timers[i] = sol.timer.start(menu, i*100, function()
				surface:fade_in(20)
				surface_timers[i] = nil
			end)
			
			if highlights[i] then
				local index = 1
				local x = 0
				for _,start in ipairs(highlights[i]) do
					--non-highlighted text before this marker
					if start>index then
						local text = ustring.sub(line,index,start-1) --UNICODE multi-codepoint
						local width,height = get_text_size(FONT,FONT_SIZE,text)
						text_surfaces[i]:draw_region(
							x, 0,
							width, height,
							surface,
							x, 0
						)
						x = x + width
						index = index + ustring.len(text) --UNICODE multi-codepoint
					end
					
					--highlighted text corresponding to this marker
					local text = ustring.match(line, "[^%s%w]*(%w*)", start) --UNICODE multi-codepoint
					local width,height = get_text_size(FONT,FONT_SIZE,text)
					text_surfaces2[i]:draw_region( --TODO hax for Solarus issue #1369
						x, 0,
						width, height,
						surface,
						x, 0
					)
					x = x + width
					index = index + ustring.len(text) --UNICODE multi-codepoint
				end
				
				
				--non-highlighted text after last marker on this line
				local text = ustring.sub(line,index) --UNICODE multi-codepoint
				local width,height = get_text_size(FONT,FONT_SIZE,text)
				text_surfaces[i]:draw_region(
					x, 0,
					width, height,
					surface,
					x, 0
				)
			else --non-highlighted text for entire line
				text_surfaces[i]:draw(
					surface
				)
			end
			
			if ustring.len(line)>0 and i<=layout.num_lines then max_line = i end --UNICODE multi-codepoint
		end
		if layout.min_lines > max_line then max_line = layout.min_lines end
		
		--set button positions
		local y_position = LINE_HEIGHT*(max_line) + layout_y + 2*layout.margin
		next_button.x = layout_x + layout.width - button_width - layout.margin
		next_button.y = y_position
		prev_button.x = layout_x + layout.margin
		prev_button.y = y_position
		
		--draw bg
		bg_surface:clear()
		bg_surface:fill_color(
			BG_COLOR,
			layout_x,
			layout_y,
			layout.width,
			y_position - layout_y + button_height + layout.margin
		)
		
		highlights = {} --clear for next pass
	end
	
	--// Called when dialog box menu is started, do initialization
	function menu:on_started()
		self:show_dialog()
	end
	
	--// Displays the text of the current dialog (may span multiple panes of text)
	function menu:show_dialog()
		local text = dialog.text
		text = text:gsub("\r\n","\n"):gsub("\r","\n")
		get_next_line = text:gmatch"([^\n]*)\n"
		
		--set layout from dialog properties
		local location = dialog.location or "top"
		layout = LAYOUTS[location]
		assert(layout, "Bad dialogs.dat entry, invalid property 'location': "..location)
		
		--codeword changed by dialog properties
		local map = game:get_map()
		if dialog.codeword and map then map:set_codeword(dialog.codeword) end
		
		--set access mode from dialog properties, or use "continue" if viewing history
		if history_index > #history then
			game.guess_analyzer:set_access_mode(dialog.action, dialog.restriction)
		else game.guess_analyzer:set_access_mode"continue" end --no need for restriction when reviewing old dialogs
		
		--implement special keyword actions
		if dialog.reset then game.guess_analyzer:remove_all_guesses() end
		if dialog.give_hint then game.guess_analyzer:give_hint(false) end
		
		--TODO clear dialog box
		line_index = 0
		
		self:next_line() --iterates to do all lines for current pane
		
		--determine if prev button is visible
		prev_button:set_visible(history_index > 1)
		
		--set next button text and determine if visible
		local next_text
		local is_next_visible = true --tentative
		if dialog.next == "#EXIT_TO_TITLE" then
			next_text = sol.language.get_string"dialog_box.exit"
		elseif history_index > #history then
			next_text = sol.language.get_string"dialog_box.continue"
			is_next_visible = dialog.action=="continue"
		else next_text = sol.language.get_string"dialog_box.next" end
		next_button:set_text(next_text)
		next_button:set_visible(is_next_visible)
		
		draw_pane()
	end
	
	function menu:next_line()
		--get next line
		local line = leftover_text or get_next_line()
		leftover_text = nil --clear for next pass
		
		if not line and line_index==0 then --no more lines in this dialog
			self:next_dialog() --start next dialog instead
			return
		end
		
		--begin next line
		line_index = line_index + 1
		line = line or "" --raw unwrapped line text
		local line_stripped = line:gsub("%@", "") --unwrapped line text with special characters removed
		local line_text = "" --displayed line text up to line break from wrapping
		
		--add words one at a time to current line until end of line or no more words fit
		for space,marker,word in ustring.gmatch(line, "(%s*)(%@?)([^%s%@]+)") do --UNICODE multi-codepoint
			local test_text = line_text..space..word
			local test_width = get_text_size(FONT, FONT_SIZE, test_text)
			if test_width > MAX_LINE_WIDTH then --wrap remaining text to next line
				if ustring.len(word)>0 then --don't wrap if just empty space at end --UNICODE multi-codepoint
					local markers = highlights[line_index]
					local this_marker = (ustring.len(marker)>0 and 1 or 0) --TODO better name --UNICODE multi-codepoint
					local marker_count = (markers and #markers or 0) + this_marker
					
					local test_text_len = ustring.len(test_text)
					local remaining_text = ustring.sub(line,test_text_len+marker_count+1) --UNICODE multi-codepoint
					leftover_text = marker..word..remaining_text
				end
				break
			else --append this word to current line and continue
				--save position of highlight marker
				if marker=="@" then
					if not highlights[line_index] then highlights[line_index] = {} end
					table.insert(highlights[line_index], ustring.len(line_text)+ustring.len(space)+1) --UNICODE multi-codepoint
				end
				
				line_text = test_text
			end
		end
		lines[line_index] = line_text --save this line of text
		text_surfaces[line_index]:set_text(line_text) --display this line of text
		text_surfaces2[line_index]:set_text(line_text) --TODO hax for Solarus issue #1369
		
		if line_index < MAX_NUM_LINES then
			if line_index < layout.num_lines then
				self:next_line() --continue adding lines until dialog box is full
			else --remaining lines don't fit the current dialog box, make blank
				for i = line_index+1, MAX_NUM_LINES do
					lines[i] = ""
					text_surfaces[i]:set_text""
					text_surfaces2[i]:set_text""
				end
			end
		end
	end
	
	--// Shows the next dialog
		--is_force (boolean, optional) - if true then goes to next dialog regardless of access mode
			--if false then access mode must be "continue" to advance, default: false
	function menu:next_dialog(is_force)
		is_force = is_force or false
		assert(type(is_force)=="boolean", "Bad argument #2 to 'next_dialog' (boolean or nil expected)")
		local access_mode = game.guess_analyzer:get_access_mode()
		if access_mode~="continue" and not is_force then return false end
		
		if history_index < #history then --go forward to previously viewed dialog
			history_index = history_index + 1
			dialog = history[history_index]
			game.guess_analyzer:set_access_mode"lockout" --ignore player inputs during brief delay
			sol.timer.start(self, 10, function() self:show_dialog() end) --add slight delay before showing next dialog
		else --advance to new dialog
			local next_dialog_id = dialog.next
			if next_dialog_id then
				if not NEXT_KEYWORDS[next_dialog_id] then
					if history_index > #history then table.insert(history, dialog) end
					history_index = history_index + 1
					
					dialog = sol.language.get_dialog(next_dialog_id)
					if sol.menu.is_started(self) then --don't add delay when first showing menu
						local delay = access_mode=="continue" and 10 or 250 --10ms delay minimum to wait until after on_character_pressed event following on_key_pressed
						game.guess_analyzer:set_access_mode"lockout" --ignore player inputs during brief delay
						sol.timer.start(self, delay, function() self:show_dialog() end) --add slight delay before showing next dialog
					end
				else
					game.guess_analyzer:set_access_mode"lockout" --ignore player inputs during brief delay
					sol.timer.start(self, 10, function() NEXT_KEYWORDS[next_dialog_id]() end) --add delay to prevent fetal crash
				end
			else game:stop_dialog() end
		end
	end
	
	function menu:prev_dialog()
		if history_index <= 1 then return false end
		
		--only add to history if new dialog that has no restriction on access mode
		local access_mode = game.guess_analyzer:get_access_mode()
		--if history_index > #history and access_mode=="continue" then
		--	table.insert(history, dialog)
		--end
		
		history_index = history_index - 1
		dialog = history[history_index]
		
		game.guess_analyzer:set_access_mode"lockout" --ignore player inputs during brief delay
		sol.timer.start(self, 10, function() self:show_dialog() end) --add slight delay before showing next dialog
	end
	
	function menu:on_key_pressed(key, modifiers)
		local action = KEY_ACTIONS[key]
		if action==nil then action = KEY_ACTIONS.DEFAULT end
		if not action then return false end
		
		local access_mode = game.guess_analyzer:get_access_mode()
		if access_mode=="continue" or action=="prev_dialog" then
			self[action](self)
			return true
		end
	end
	
	function menu:on_mouse_pressed(mouse_button, x, y)
		--local access_mode = game.guess_analyzer:get_access_mode()
		--if access_mode=="continue" then self:prev_dialog(); return true end
		
		for _,control in ipairs(controls) do
			local pos_x, pos_y = control:get_xy()
			local x_min = control.x + pos_x
			local y_min = control.y + pos_y
			local ctrl_width, ctrl_height = control:get_size()
			local x_max = x_min + ctrl_width
			local y_max = y_min + ctrl_height
			
			if control.on_mouse_pressed then
				if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
					if control:on_mouse_pressed(mouse_button, x-x_min, y-y_min) then
						return true
					end
				end
			end
		end
	end
	
	function menu:on_mouse_released(mouse_button, x, y)
		for _,control in ipairs(controls) do
			local pos_x, pos_y = control:get_xy()
			local x_min = control.x + pos_x
			local y_min = control.y + pos_y
			local ctrl_width, ctrl_height = control:get_size()
			local x_max = x_min + ctrl_width
			local y_max = y_min + ctrl_height
			
			if control.on_mouse_released then
				if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
					control:on_mouse_released(mouse_button, x-x_min, y-y_min)
				else control:on_mouse_released(mouse_button, nil, nil) end --released outside control bounds
			end
		end
	end
	
	function menu:on_draw(dst_surface)
		bg_surface:draw(dst_surface)
		
		for i,surface in ipairs(surfaces) do
			if not surface_timers[i] then surface:draw(dst_surface) end --timer is delay until surface should be shown
		end
		
		for _,control in ipairs(controls) do
			control:draw(dst_surface, control.x, control.y)
		end
	end
	
	return menu
end

--create a new dialog box instance whenever starting a game
local game_meta = sol.main.get_metatable"game"
game_meta:register_event("on_started", function(...) dialog_box.create(...) end) --encapsulate in function so no return value

return dialog_box

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
