--[[title.lua
	version 1.0
	1 Jun 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script displays the title screen menu for Word Breaker.
]]

local ustring = require"scripts/lib/ustring/ustring"
local menus = require"scripts/menus/menus"
require"scripts/coroutine_helper"

local menu = {}

local controls = {} --(table, array) list of all ui controls used in this menu

--convenience
local math__random = math.random

--Timing for the non-looping intro part of song
	--title letter(s) get revealed after each delay (in ms) in this list
	--TOTAL is the total duration of the intro in ms and should be longer than the sum of all the delays
local INTRO = {1370, 250, 80, 80, 80, 250, TOTAL=2370}

--series of delays in ms between melody beats in title menu music
--note: after each delay 3 letters of the title (chosen randomly) will bounce, except if last delay of given entry then bounce 5
	--each entry is an array listing some delays (number, positive multiple of 10) in ms
		--the key 'count' specifies the number of times an entry is used (number, positive integer, optional)
			--e.g. a count of 2 means repeat once before moving to next entry
			--default: 1 (plays once with no repeat)
		--after the last entry completes it loops back to the first entry, corresponding with the music looping
local BEATS = {
	--need to adjust total time by -40ms
	{750, 750, 500, count=16},
	{500, 1500, count=16,       adjust=-10},
	{750, 1250, count=16,       adjust=-10},
	{500, 1500, count=16,       adjust=-10},
	{750, 750, 500, count=8},
	{500, 750, 750, count=8,    adjust=-10},
}
for _,row in ipairs(BEATS) do --assign default values
	row.count = row.count or 1
	row.adjust = row.adjust or 0
end

--series of delays in ms between drum beats in title menu music
--note: after each delay 2 letters of the title (chosen randomly) will change color
--(always chooses a color different than the colors of the adjacent letters)
	--each entry is an array listing some delays (number, positive multiple of 10) in ms
		--the key 'count' specifies the number of times an entry is used (number, positive integer, optional)
			--e.g. a count of 2 means repeat once before moving to next entry
			--default: 1 (plays once with no repeat)
		--after the last entry completes it loops back to the first entry, corresponding with the music looping
local DRUMS = {
	--need to adjust total time by -40ms
	{370, 130, 250, 500, 250, 500, count=7},
	{370, 130, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{370, 130, 250, 500, 250, 500, count=7},
	{370, 130, 250, 250, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{500, 500, 250, 250, 250, 250, count=3,                                   adjust=-10},
	{500, 500, 250, 120, 130, 120, 130, 120, 130, count=1},
	{500, 500, 250, 250, 250, 250, count=3},
	{500, 370, 130, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{500, 500, 250, 250, 250, 250, count=7},
	{500, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{500, 500, 500, 500, count=7,                                             adjust=-10},
	{370, 130, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{370, 130, 500, 500, 500, count=7},
	{370, 130, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{500, 500, 250, 250, 250, 250, count=3,                                   adjust=-10},
	{500, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{500, 500, 250, 250, 250, 250, count=3},
	{500, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{500, 500, 250, 250, 250, 250, count=7},
	{500, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{370, 130, 250, 500, 250, 500, count=7},
	{370, 130, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
	{500, 500, 250, 250, 250, 250, count=7,                                   adjust=-10},
	{500, 500, 120, 130, 120, 130, 120, 130, 120, 130, count=1},
}
for _,row in ipairs(DRUMS) do --assign default values
	row.count = row.count or 1
	row.adjust = row.adjust or 0
end

--available colors for each letter of title (chosen randomly), RGB values 0-255
local LETTER_COLORS = {
	{235, 6, 89},
	{0, 127, 181},
	{193, 0, 255},
	{0, 139, 0},
	{195, 164, 4},
	{255, 104, 3},
}

local TITLE_FONT
local TITLE_FONT_SIZE
local TITLE_TEXT

local title_letters = {} --array of surfaces for each letter of title

--// Applies a bounce movement to the title letter at the given index
	--index (number, positive integer) - Index of the title letter to bounce
local function bounce_movement(index)
	local movement = sol.movement.create"straight"
	local surface = title_letters[index]
	
	movement:set_speed(50)
	movement:set_angle(math.pi/2)
	movement:set_max_distance(5)
	movement:start(surface, function()
		movement = sol.movement.create"straight"
		movement:set_speed(50)
		movement:set_angle(math.pi*3/2)
		movement:set_max_distance(5)
		movement:start(surface)
	end)
end

--// Selects at random a number of title letters equal to count to apply a bounce movement
	--count (number, positive integer, optional) the number of letters to bounce, default: 1
local function do_bounce(count)
	count = count or 1 --default
	
	local index_list = {} --list of indices to bounce
	
	local pool_size = #title_letters --number of letters to choose from (initially all)
	if count >= pool_size then count = pool_size - 1 end --at most can select all letters but one
	
	--randomly select a number of letters to bounce equal to count
	for i=1,count do
		table.insert(index_list, math__random(pool_size))
		pool_size = pool_size - 1
	end
	
	--adjust index positioning to remove duplicates
	for i=2,#index_list do
		for lower_i=1,i-1 do
			local index = index_list[i]
			local lower_index = index_list[lower_i]
			if index >= lower_index then index_list[i] = index + 1 end
		end
	end
	
	--start the bounce movements (simultaneous)
	for _,index in ipairs(index_list) do
		bounce_movement(index)
	end
end

--// Changes the color of the title letter at the given index (randomly chosen color, will not match adjacent letter colors)
	--index (number, positive integer) - Index of the title letter to apply a new color
local function color_modulation(index)
	local surface = title_letters[index]
	
	local left_index = index > 1 and index - 1 --false if no letter on left
	local right_index = index < #title_letters and index + 1 --false if no letter on right
	
	--choose a different color than the current color and colors of adjacent letters
	local skip_colors_list = {surface.color_index} --list of color indices to skip, no duplicates
	do
		local skip_colors = {[surface.color_index] = true} --keys are color indices already added to list
		
		if left_index then
			local left_color = title_letters[left_index].color_index
			if not skip_colors[left_color] then
				table.insert(skip_colors_list, left_color)
				skip_colors[left_color] = true
			end
		end
		
		if right_index then
			local right_color = title_letters[right_index].color_index
			if not skip_colors[right_color] then
				table.insert(skip_colors_list, right_color)
				skip_colors[right_color] = true
			end
		end
	end
	table.sort(skip_colors_list)
	
	--randomly select from available colors
	local num_colors = #LETTER_COLORS - #skip_colors_list --number of available colors to choose from
	local color_index = math__random(num_colors)
	
	--adjust selected index to 'step over' skipped indices
	for i,skip_index in ipairs(skip_colors_list) do
		if color_index >= skip_index then color_index = color_index + 1 end
	end
	
	surface:set_color_modulation(LETTER_COLORS[color_index]) --change letter color
	surface.color_index = color_index --keep track of new color
end

--// Selects at random a number of title letters equal to count to change the color
	--count (number, positive integer, optional) the number of letters to change color, default: 1
local function do_color(count)
	count = count or 1
	
	local index_list = {} --list of indices to change color
	
	local pool_size = #title_letters --number of letters to choose from
	if count >= pool_size then count = pool_size - 1 end --at most can select all letters but one
	
	--randomly select a number of letters to change color equal to count
	for i=1,count do
		table.insert(index_list, math__random(pool_size))
		pool_size = pool_size - 1
	end
	
	--adjust index positioning to remove duplicates
	for i=2,#index_list do
		for lower_i=1,i-1 do
			local index = index_list[i]
			local lower_index = index_list[lower_i]
			if index >= lower_index then index_list[i] = index + 1 end
		end
	end
	
	--change the colors of the randomly selected letters
	for _,index in ipairs(index_list) do
		color_modulation(index)
	end
end

--// Creates a separate surface for each letter in title from strings.dat using current language
local function create_letters()
	letters = {}
	
	TITLE_FONT = sol.language.get_string"title.banner_font" or "Sniglet/Sniglet Regular"
	TITLE_FONT_SIZE = tonumber(sol.language.get_string"title.banner_font_size") or 80
	TITLE_TEXT = sol.language.get_string"title.banner" --always get when menu starts in case changed languages
	
	--temporarily create text surface of title
	local text_surface = sol.text_surface.create{
		font = TITLE_FONT,
		font_size = TITLE_FONT_SIZE,
		color = {255, 255, 255, 255},
		horizontal_alignment = "left",
		vertical_alignment = "top",
		rendering_mode = "antialiasing",
		font_hinting = "light",
		text = TITLE_TEXT,
	}
	
	--create individual surface for each letter of title
	local x_start = 0
	local x_origin = (sol.video.get_quest_size() - text_surface:get_size())/4 --left-most edge of banner text, center horizontally
	local prev_color_index
	local i = ustring.find(TITLE_TEXT, "%S", i) --UNICODE multi-codepoint (may need optional glyph separator character)
	while i and i <= ustring.len(TITLE_TEXT) do
		local subtext = ustring.sub(TITLE_TEXT,1,i) --UNICODE multi-codepoint
		local x_end, height = sol.text_surface.get_predicted_size(TITLE_FONT, TITLE_FONT_SIZE, subtext)
		local width = x_end - x_start
		
		local surface = sol.surface.create(width, height)
		text_surface:draw_region(
			x_start, 0,
			width, height,
			surface
		)
		surface.x_offset = x_start
		surface:set_xy(x_origin, 40)
		surface:fade_out(0)
		
		--choose different color than letter to left at random
		local color_index
		if prev_color_index then
			color_index = math__random(#LETTER_COLORS - 1)
			if color_index >= prev_color_index then color_index = color_index + 1 end
		else color_index = math__random(#LETTER_COLORS) end
		surface:set_color_modulation(LETTER_COLORS[color_index])
		surface.color_index = color_index
		prev_color_index = color_index
		
		table.insert(letters, surface)
		
		x_start = x_end
		i = ustring.find(TITLE_TEXT, "%S", i+1) --UNICODE multi-codepoint
	end
	
	--TODO unused, fix for silly bug where last letter of title not shown in fullscreen mode
	local dummy_surface = sol.surface.create(1,1)
	text_surface:draw_region(0,0,1,1,dummy_surface)
	
	return letters
end

--// This event gets called whenever the language changes to update the title letter surfaces
function menu:on_language_changed()
	title_letters = create_letters()
end

--// This event gets called whenever the menu is started
--// Starts the music and endlessly applies letter bounces and color changes in-sync with music
function menu:on_started()
	local letters = create_letters()
	
	--pick random order of letters for reveal animation
	local letters_queue = {}
	do local letters_copy = {}
		for i=1,#letters do letters_copy[i] = letters[i] end
		local b = #letters + 1
		for i=#letters,1,-1 do
			letters_queue[b-i] = table.remove(letters_copy, math__random(i))
		end
	end
	
	sol.timer.start(self, 100, function() --need delay before starting music because don't want to start immediately following reset
		title_letters = letters --assignment delayed so letters not drawn on first frame
		
		--start music
		sol.audio.play_music("Windle_Pixel_Saves_the_Day", true)
		
		--have letters appear during the intro
		local intro_index = 1
		local letters_remaining = #letters_queue
		local beats_remaining = #INTRO
		sol.timer.start(self, INTRO[intro_index], function()
			local remove_amount = math.floor(letters_remaining/beats_remaining)
			letters_remaining = letters_remaining - remove_amount
			beats_remaining = beats_remaining - 1
			
			for i=1,remove_amount do
				local surface = table.remove(letters_queue, #letters_queue)
				surface:fade_in(10)
			end
			
			intro_index = intro_index + 1
			return intro_index <= #INTRO and INTRO[intro_index]
		end)
		
		--delay during song intro, then repeat main loop
		sol.timer.start(self, INTRO.TOTAL, function() --1 sec delay before starting music
			--sol.audio.play_music("Windle_Pixel_Saves_the_Day_Looping", true)
			menus:start_submenu"main_menu"
			
			--initial "beat" corresponding to start of music
			do_bounce(3)
			do_color(2)
			
			--initialize values
			local beat_row_index = 1
			local beat_row = BEATS[beat_row_index]
			local beat_adjust = beat_row.adjust
			local beat_index = 1
			local beat_count = 1
			local initial_beat_delay = beat_row[beat_index] + beat_adjust
			
			local timer = sol.timer.start(self, initial_beat_delay, function()
				beat_index = beat_index + 1
				beat_adjust = 0
				
				if beat_index > #beat_row then --end of row, may need to repeat
					if beat_count >= beat_row.count then --finished repeats, move to next row
						beat_row_index = beat_row_index + 1
						if beat_row_index > #BEATS then beat_row_index = 1 end --start over
						beat_row = BEATS[beat_row_index]
						beat_adjust = beat_row.adjust
						beat_index = 1
						beat_count = 1
					else --repeat this row
						beat_index = 1
						beat_count = beat_count + 1
					end
				end
				
				local is_strong = beat_index==#beat_row
				do_bounce(is_strong and 5 or 3)
				
				return beat_row[beat_index] + beat_adjust --create new timer with this delay, endless repeat
			end)
			
			--initialize values
			local drum_row_index = 1
			local drum_row = DRUMS[beat_row_index]
			local drum_adjust = drum_row.adjust
			local drum_index = 1
			local drum_count = 1
			local initial_drum_delay = drum_row[beat_index] + drum_adjust
			
			local timer = sol.timer.start(self, initial_drum_delay, function()
				drum_index = drum_index + 1
				drum_adjust = 0
				
				if drum_index > #drum_row then --end of row, may need to repeat
					if drum_count >= drum_row.count then --finished repeats, move to next row
						drum_row_index = drum_row_index + 1
						if drum_row_index > #DRUMS then drum_row_index = 1 end --start over
						drum_row = DRUMS[drum_row_index]
						drum_adjust = drum_row.adjust
						drum_index = 1
						drum_count = 1
					else --repeat this row
						drum_index = 1
						drum_count = drum_count + 1
					end
				end
				
				do_color(2)
				
				return drum_row[drum_index] + drum_adjust --create new timer with this delay, endless repeat
			end)
		end)
	end)
end

function menu:on_key_pressed(key, modifiers)
	--TODO
end

function menu:on_mouse_pressed(mouse_button, x, y)
	for _,control in ipairs(controls) do
		local pos_x, pos_y = control:get_xy()
		local x_min = control.x + pos_x
		local y_min = control.y + pos_y
		local ctrl_width, ctrl_height = control:get_size()
		local x_max = x_min + ctrl_width
		local y_max = y_min + ctrl_height
		
		if control.on_mouse_pressed then
			if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
				if control:on_mouse_pressed(mouse_button, x-x_min, y-y_min) then
					return true
				end
			end
		end
	end
end

function menu:on_mouse_released(mouse_button, x, y)
	for _,control in ipairs(controls) do
		local pos_x, pos_y = control:get_xy()
		local x_min = control.x + pos_x
		local y_min = control.y + pos_y
		local ctrl_width, ctrl_height = control:get_size()
		local x_max = x_min + ctrl_width
		local y_max = y_min + ctrl_height
		
		if control.on_mouse_released then
			if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
				control:on_mouse_released(mouse_button, x-x_min, y-y_min)
			else control:on_mouse_released(mouse_button, nil, nil) end --released outside control bounds
		end
	end
end

function menu:on_draw(dst_surface)
	dst_surface:fill_color{0,0,0}
	for i,surface in ipairs(title_letters) do
		local x,y = surface:get_xy()
		surface:draw(dst_surface, x+surface.x_offset, y)
	end
end

menus.title = menu
sol.main.menus = menus

return menu

--[[ Copyright 2019-2020 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
