--[[ guess_analyzer.lua
	version 0.1a1
	23 Feb 2022
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script processes guesses from the player and provides an interface for the player
	to deduce the hidden codeword. The player enters guesses in the guess prompt, and they
	are added to the guess panel. The player marks individual letters as present or not in
	the codeword by using the letter slate, and the known letters of the codeword can then
	be rearranged on the scratch pad.
]]

require"scripts/coroutine_helper"
local ustring = require"scripts/lib/ustring/ustring"
local ui = require"scripts/menus/ui/ui"
local achievements = require"scripts/achievements"
local gamepad_manager = require"scripts/gamepad_manager"

local menu = {}

--convenience
local math__random = math.random

local COLOR_LIST = {
	{235, 6, 89}, --red (known wrong letter)
	{0, 130, 204}, --blue (known correct letter)
	{0, 0, 0}, --black (default)
	{139, 0, 255}, --purple (alt 1)
	{0, 200, 0}, --green (alt 2)
	{255, 142, 68}, --{255, 104, 3}, --orange (alt 3)
	{190, 190, 0}, --{195, 164, 4}, --yellow (alt 4)
}
for i,v in ipairs(COLOR_LIST) do COLOR_LIST[v] = i end --reverse lookup

local ACCESS_MODES = {
	continue = {pause=true}, --can advance dialog
	lockout = {},
	guess = {pause=true, remove_letter=true, submit_guess=true, chars=true},
	slate = {pause=true, slate=true},
	clear_slate = {pause=true, clear_slate=true},
	slate_all = {pause=true, slate=true, clear_slate=true},
	shuffle = {pause=true, scramble=true, drag_letters=true},
	guess_shuffle = {
		pause=true, remove_letter=true, submit_guess=true,
		chars=true, scramble=true, drag_letters=true,
	},
	hint = {pause=true, give_hint=true},
}

local MODIFIERS = {
	"control",
	"alt",
	"shift",
}

local KEY_ACTIONS = {
	backspace = "remove_letter",
	['return'] = "submit_guess",
	space = "scramble",
	escape = "pause",
	f1 = "show_controls",
	--with modifiers (order must be "control+alt+shift")
	['control+backspace'] = "clear_slate",
}

local GAMEPAD_ACTIONS = {
	--common actions in any mode:
	action = "cursor_action",
	pause = "pause",
	toggle_mode = "toggle_gamepad_mode",
	up = "nudge_gamepad_cursor",
	down = "nudge_gamepad_cursor",
	left = "nudge_gamepad_cursor",
	right = "nudge_gamepad_cursor",
	
	--gamepad mode-specific actions:
	slate = {
		back = "remove_letter",
		scramble = "cursor_action",
		submit = "cursor_action",
		--contextual_menu = "toggle_contextual_menu", --TODO
	},
	scratch = {
		back = "gamepad_cancel_grab",
		scramble = "scramble",
		submit = "submit_scratch",
	},
	
	---TODO not implemented yet
	--hint = "use_hint", --TODO button must be held for 1 second
}

local GAMEPAD_MODES = {
	"slate",
	"scratch",
}
for i,mode in ipairs(GAMEPAD_MODES) do GAMEPAD_MODES[mode] = i end --reverse lookup

--converts & returns restriction string to key/values table where:
	--keys are a unicode multi-codepoint single character (string) where each char represents a slate button
	--values are an integer 1-7 representing the slate color for that char/button
local PARSE_RESTRICTIONS = {
	slate = function(str)
		local restriction = {}
		if ustring.sub(str,ustring.len(str))~="," then str = str.."," end --add comma to end if not present --UNICODE multi-codepoint
		for entry in ustring.gmatch(str, "([^%,]+)%,") do --UNICODE multi-codepoint
			local char, dir = ustring.match(entry, "^([^%:%d]+)%:(%d+)$") --UNICODE multi-codepoint
			if char then restriction[char] = tonumber(dir) end
		end
		return restriction
	end,
}

local SLATE_COL_COUNT = 9 --always 9, even if fewer slate buttons
--these are set in menu:on_started()
local SLATE_ROW_COUNT
local SLATE_AREA
local MAX_SLATE_INDEX
local SLATE_CURSOR_FROM
local SLATE_CURSOR_TO

local controls = {} --(table, array) list of all ui controls used in this menu
local char_list = {} --(table, array) only characters in this list are valid for guesses
local char_colors = {} --(table, key/value) valid characters as keys with the color (table) for that character as a value or nil to use default color
local slate = {} --(table, array) list of buttons in slate, up to 36 items, last item is clear button
	--reverse lookup: slate button as key gives its char (string comprised of single unicode multi-codepoint character) as value; char as key gives its button as value
local scratch = {} --(table, array) list of scratch panel controls, contains 5 items
local scratch_text = {} --(table, array) list of scratch text controls (draggable), contains 5 items
local scratch_list = {"", "", "", "", "", count=0, max_count=5} --(table, combo) list of each character present in scratch text, up to 5 items, number of items given by key "count"
local hint_list = {count=0} --(table, key/value) list of hint characters revealed for scratch panels; letters may occupy keys 1-5
local text_prompt --(text ui control) player enters text for new guesses here
local difficulty_text
local score_text
local timer_text
local stage_text
local final_text
local error_text --(sol.text_surface) may display error after player submits guess
local guess_panels = {} --(table, array) list of guess panels, contains 30 items
local match_panels = {} --(table, array) list of match panels for each guess, contains 30 items
local gate
local hint_text
local hint_button
local gamepad_cursor

local access_mode --(string or nil) The current mode, restricts actions of player (used in tutorial mode)
	--"lockout" - the player is locked out of all actions
	--"continue" - the player is locked out of all actions but can advance the tutorial dialog
	--"guess" - the player can only enter guesses
	--"slate" - the player can only manipulate the slate (but not clear all colors)
	--"slate_clear" - the player can only clear the slate colors
	--"slate_all" - the player can only manipulate the slate or clear all colors
	--"shuffle" - the player can only rearrange scratch letters
	--"guess_shuffle" - the player can both make guesses and rearrange the scratch letters
	--nil - no restrictions
local restriction --(string or nil) restriction imposed on the player for the current access mode
local guess_count = 0
local dragged_type
local dragged_index
local dragged_new_index
local dragged_timer
local between_guesses_timer
local splats = {}
local gamepad_slate_index = 1
local gamepad_scratch_index = 1
local gamepad_mode = "slate"

local function refresh_gamepad_cursor()
	gamepad_cursor:set_animation(gamepad_mode)
	
	if gamepad_mode=="slate" then
		local row = math.floor((gamepad_slate_index - 1) / SLATE_COL_COUNT)
		local col = (gamepad_slate_index - 1) % SLATE_COL_COUNT
		gamepad_cursor:set_xy(
			246+16 + 36*col,
			81+16-2 + 36*row
		)
	elseif gamepad_mode=="scratch" then
		gamepad_cursor:set_xy(230+32 + 72*(gamepad_scratch_index - 1), 274+32)
	end
end

--// append text to the end of the prompt field
local function prompt_append(prompt, text)
	assert(type(text)=="string", "Bad argument #2 to 'prompt_append' (string expected)")
	if text=="" then return end --do nothing if empty string
	
	prompt:set_text(prompt:get_text()..text)
end

--// remove the specified number of characters from the end of the prompt field
local function prompt_remove(prompt, num_chars)
	num_chars = tonumber(num_chars)
	assert(num_chars, "Bad argument #2 to 'prompt_remove' (number expected)")
	num_chars = math.floor(num_chars)
	assert(num_chars > 0, "Bad argument #2 to 'prompt_remove', number must be positive")
	
	local old_text = prompt:get_text()
	local sub_length = ustring.len(old_text) - num_chars --UNICODE multi-codepoint
	if sub_length > 0 then
		prompt:set_text(ustring.sub(old_text,1,sub_length)) --UNICODE multi-codepoint
	else prompt:set_text"" end --remove all text
end

--// Adds a character to the scratch panels (unless full)
local function scratch_add(char)
	if scratch_list.count < scratch_list.max_count then
		local n = 1
		for i=1,5 do
			if not hint_list[i] then
				if scratch_list[n]=="" then
					scratch_list[n] = char
					scratch_list[char] = true
					
					scratch_text[i]:set_text(ustring.upper(char))
					break
				end
				n = n + 1
			end
		end
		scratch_list.count = scratch_list.count + 1
		sol.audio.play_sound"scratch_add"
		return true
	else return false end
end

--// Removes the specified character from the scratch panels
local function scratch_remove(char)
	local n = 1
	--for i=1,scratch_list.max_count do
	for i=1,5 do
		if not hint_list[i] then
			if scratch_list[n]==char then
				scratch_list[n] = ""
				scratch_list[char] = nil
				scratch_list.count = scratch_list.count - 1
				
				scratch_text[i]:set_text""
				sol.audio.play_sound"scratch_remove"
				
				return true
			end
			n = n + 1
		end
	end
	
	return false
end

--// Move selected letter from scratch pad around following cursor while dragging
local function drag_scratch(panel, mouse_button, x, y)
	if access_mode and not ACCESS_MODES[access_mode].drag_letters then return end --access mode does not allow dragging letters
	if scratch_list.max_count <=1 then return end --need at least 2 panels available to swap
	
	dragged_type = "mouse"
	dragged_index = nil --clear previous data
	
	--find which panel is selected for drag
	local n = 0
	for i,scratch_panel in ipairs(scratch) do
		if not hint_list[i] then n = n + 1 end
		if scratch_panel == panel then
			if hint_list[i] then return end --don't allow dragging hint panels
			dragged_index = i
			break
		end
	end
	local dragged_text = scratch_text[dragged_index]
	
	local panel_min,panel_max,i_min,i_max
	for i=1,5 do
		if not hint_list[i] then
			if not panel_min then
				panel_min = scratch[i]
				i_min = i
			end
			panel_max = scratch[i]
			i_max = i
		end
	end
	
	--calculate bounds of scratch panels
	local y_min,y_max,x_min,x_max
	do
		--calculate min/max positions
		local pos_x, pos_y = panel_min:get_xy()
		local width, height = panel_min:get_size() --assume same for all panels
		y_min = panel_min.y + pos_y - 4
		y_max = y_min + height + 4
		x_min = panel_min.x + pos_x - 4
		pos_x, pos_y = panel_max:get_xy()
		x_max = panel_max.x + pos_x + width + 4
	end
	
	--calculate x positions between each panel
	local x_dividers = {}
	local bin_panels = {}
	local offset
	do
		local step = (x_max - x_min)/(i_max - i_min + 1) --horz spacing between panels in pixels
		offset = step/2
		local x_dist = x_min + offset --keep running sum of x distance traversed
		for i=i_min,i_max do
			if not hint_list[i] then
				table.insert(x_dividers, x_dist)
				table.insert(bin_panels, scratch_text[i])
			end
			x_dist = x_dist + step
		end
	end
	
	dragged_index = n
	local current_index = n
	
	local dragged_start_x, dragged_start_y = sol.input.get_mouse_position()
	dragged_timer = sol.timer.start(menu, 100, function() --update position of dragged text every 100ms
		local mouse_x, mouse_y = sol.input.get_mouse_position()
		local new_x = mouse_x - dragged_start_x
		local new_y = mouse_y - dragged_start_y
		dragged_text:set_drag_offset(new_x, new_y) --move dragged text to cursor position
		
		if mouse_y>=y_min and mouse_y<=y_max and mouse_x>=x_min and mouse_x<=x_max then --within scratch panels bounds
			local new_index = #bin_panels --tentative
			for i,x_val in ipairs(x_dividers) do
				if i ~= dragged_index then
					if mouse_x <= (x_val + (i<dragged_index and 1 or -1)*offset) then
						new_index = i + (i<dragged_index and 0 or -1)
						break
					end
				end
			end
			
			if new_index ~= current_index then
				for i = 1,#bin_panels do
					if i ~= dragged_index then
						local other_panel_text = bin_panels[i]
						if new_index<dragged_index and i>=new_index and i<dragged_index then
							local step = x_dividers[i+1] - x_dividers[i] --positive
							other_panel_text:set_drag_offset(step,0)
						elseif new_index>dragged_index and i>dragged_index and i<=new_index then
							local step = x_dividers[i-1] - x_dividers[i] --negative
							other_panel_text:set_drag_offset(step,0)
						else other_panel_text:set_drag_offset(0,0) end
					end
				end
				current_index = new_index
				dragged_new_index = new_index
			end
		else --restore positions of other scratch letters
			for i = 1,#bin_panels do
				if i ~= dragged_index then
					local other_panel_text = bin_panels[i]
					other_panel_text:set_drag_offset(0,0)
				end
			end
			current_index = nil
		end
		
		return true
	end)
end

--// Lets go of dragged scratch pad letter when mouse button released
local function drop_scratch(panel)
	if dragged_index then
		if dragged_timer then
			dragged_timer:stop()
			dragged_timer = nil
		end
		
		local panel_min,panel_max,i_min,i_max
		local dragged_scratch_index
		do local n = 0
			for i=1,5 do
				scratch_text[i]:set_drag_offset(0,0) --reset offsets
				
				if not hint_list[i] then
					if not panel_min then
						panel_min = scratch[i]
						i_min = i
					end
					panel_max = scratch[i]
					i_max = i
					
					n = n + 1
					if n==dragged_index then dragged_scratch_index = i end
				end
			end
		end
		
		--calculate bounds of scratch panels
		local y_min,y_max,x_min,x_max
		do
			--calculate min/max positions
			local pos_x, pos_y = panel_min:get_xy()
			local width, height = panel_min:get_size() --assume same for all panels
			y_min = panel_min.y + pos_y - 4
			y_max = y_min + height + 4
			x_min = panel_min.x + pos_x - 4
			pos_x, pos_y = panel_max:get_xy()
			x_max = panel_max.x + pos_x + width + 4
		end
		
		--calculate x positions between each panel
		local x_dividers = {}
		local bin_panels = {}
		local offset
		do
			local step = (x_max - x_min)/(i_max - i_min + 1) --horz spacing between panels in pixels
			offset = step/2
			local x_dist = x_min + offset --keep running sum of x distance traversed
			for i=i_min,i_max do
				if not hint_list[i] then
					table.insert(x_dividers, x_dist)
					table.insert(bin_panels, scratch_text[i])
				end
				x_dist = x_dist + step
			end
		end
		
		--get current mouse position
		local mouse_x, mouse_y = sol.input.get_mouse_position()
		if mouse_y>=y_min and mouse_y<=y_max and mouse_x>=x_min and mouse_x<=x_max then --within scratch panels bounds
			local new_index = #bin_panels --tentative
			for i,x_val in ipairs(x_dividers) do
				if i ~= dragged_index then
					if mouse_x <= (x_val + (i<dragged_index and 1 or -1)*offset) then
						new_index = i + (i<dragged_index and 0 or -1)
						break
					end
				end
			end
			
			--reorder panels if changed
			if dragged_index ~= new_index then
				local moved_text = table.remove(scratch_list, dragged_index)
				table.insert(scratch_list, new_index, moved_text)
				
				local n=1
				for i=1,5 do
					if not hint_list[i] then
						scratch_text[i]:set_text(ustring.upper(scratch_list[n]))
						n = n + 1
					end
				end
				
				--if access mode is active then advance dialog
				if access_mode and ACCESS_MODES[access_mode].scramble and not restriction then
					local game = sol.main.get_game()
					if game.advance_dialog then game.advance_dialog() end --continue tutorial
				end
			end --do nothing if dropped in same panel started from
		end --else dropped outside of bounds, return all to original positions
		
		dragged_type = nil
		dragged_index = nil
		dragged_new_index = nil
		
		return true
	end
end

--// Cycles the specified slate button though the alternate colors (alt+click)
local function slate_toggle_alt(button)
	local char = slate[button]
	local direction = button:get_direction()
	
	if hint_list[char] then return end --do not change color of hints
	if (access_mode and not ACCESS_MODES[access_mode].slate) --access mode doesn't include slate
	or (restriction and (not restriction[char] or restriction[char]==direction)) then --this button is not allowed or is already set correctly
		return
	end
	
	local new_direction
	if direction < 3 then
		new_direction = 4
	elseif direction >= 7 then
		new_direction = 3
	else new_direction = direction + 1 end
	
	if direction==2 then --direction changed from 2
		scratch_remove(char) --remove letter from scratch pad --UNICODE multi-codepoint
	end
	
	button:set_direction(new_direction)
	local color = COLOR_LIST[new_direction]
	char_colors[button:get_text()] = color
	for _,guess_panel in ipairs(guess_panels) do guess_panel:refresh() end
	
	--check if access mode restriction is met
	if access_mode and ACCESS_MODES[access_mode].slate then
		local is_pass = true --tentative
		for char,dir in pairs(restriction or {}) do
			local this_button = slate[char]
			if this_button:get_direction()~=dir then is_pass = false; break end
		end
		local game = sol.main.get_game()
		if is_pass and game.advance_dialog then game.advance_dialog() end --continue tutorial
	end
end

--// Cycles the specified slate button through the primary colors (click with no modifier key)
local function slate_toggle(button)
	local char = slate[button]
	local direction = button:get_direction()
	
	if hint_list[char] then return end --do not change color of hints
	if (access_mode and not ACCESS_MODES[access_mode].slate) --access mode doesn't include slate
	or (restriction and (not restriction[char] or restriction[char]==direction)) then --this button is not allowed or is already set correctly
		return
	end
	
	local new_direction = 1 --tentative
	if direction < 3 then new_direction = direction + 1 end
	
	if new_direction==2 then --direction changed to 2
		if not scratch_add(char) then --add letter to scratch pad --UNICODE multi-codepoint
			new_direction = 3 --unable to add to scratch, clear button color instead
		end
	elseif direction==2 then --direction changed from 2
		scratch_remove(char) --remove letter from scratch pad --UNICODE multi-codepoint
	end
	
	button:set_direction(new_direction)
	local color = COLOR_LIST[new_direction]
	char_colors[button:get_text()] = color
	for _,guess_panel in ipairs(guess_panels) do guess_panel:refresh() end --refresh all guess panels
	
	--check if access mode restriction is met
	if access_mode and ACCESS_MODES[access_mode].slate then
		local is_pass = true --tentative
		for char,dir in pairs(restriction or {}) do
			local this_button = slate[char]
			if this_button:get_direction()~=dir then is_pass = false; break end
		end
		local game = sol.main.get_game()
		if is_pass and game.advance_dialog then game.advance_dialog() end --continue tutorial
	end
end

local function slate_set_color(button, direction)
	local char = slate[button]
	local old_direction = COLOR_LIST[ char_colors[button:get_text()] ] or 3
	if old_direction==direction then return end --slate button is already the new color
	
	if hint_list[char] then return end --do not change color of hints
	if (access_mode and not ACCESS_MODES[access_mode].slate) --access mode doesn't include slate
	or (restriction and (not restriction[char] or restriction[char]~=direction)) then --this button is not allowed
		return
	end
	
	if direction==2 then --direction changed to 2
		if not scratch_add(char) then return end --unable to add another character to scratch; abort
	elseif old_direction==2 then --direction chagned from 2
		scratch_remove(char)
	end
	
	button:set_direction(direction)
	local color = COLOR_LIST[direction]
	char_colors[button:get_text()] = color
	for _,guess_panel in ipairs(guess_panels) do guess_panel:refresh() end -- refresh all guess panels
	
	--check if access mode restriction is met
	if access_mode and ACCESS_MODES[access_mode].slate then
		local is_pass = true --tentative
		for char,dir in pairs(restriction or {}) do
			local this_button = slate[char]
			if this_button:get_direction()~=dir then is_pass = false; break end
		end
		local game = sol.main.get_game()
		if is_pass and game.advance_dialog then game.advance_dialog() end --continue tutorial
	end
end

--// Performs the action for the specified slate button based on the current key modifiers
local function slate_action(button, action)
	local modifiers = sol.input.get_key_modifiers()
	
	if not action then
		if modifiers.control then
			--TODO contextual menu
		elseif modifiers.alt then
			slate_toggle_alt(button)
		else slate_toggle(button) end
	else
		if action=="action" then
			local char = slate[button] --note: will be nil if on clear button, but clear button uses different on_clicked function
			menu:add_letter(char) --TODO change menu to self
		elseif action=="submit" then
			slate_toggle_alt(button)
		elseif action=="scramble" then
			slate_toggle(button)
		end
	end
end

local function colorize_last_guess(direction)
	assert(type(direction)=="number", "Bad argument #1 to 'colorize_last_guess' (number expected)")
	assert(COLOR_LIST[direction], "Bad argument #1 to 'colorize_last_guess', invalid direction: "..tostring(direction))
	
	if access_mode and not ACCESS_MODES[access_mode].slate then return end --access mode does not allow clearing slate
	if direction==3 then return end --black letters are already black; no action required
	
	local game = sol.main.get_game()
	local map = game:get_map()
	if not map then return end
	
	local last_guess = map:get_last_guess()
	if not last_guess then return end
	
	for i = 1,5 do
		local char = ustring.sub(last_guess, i, i) --UNICODE multi-codepoint
		local button = slate[char]
		local current_color = COLOR_LIST[ char_colors[button:get_text()] ] or 3
		if current_color==3 then
			slate_set_color(button, direction)
		end
	end
end

--// Resets all slate buttons to no active color
local function clear_slate(self, action)
	if access_mode and not ACCESS_MODES[access_mode].clear_slate then return end --access mode does not allow clearing slate
	if action and action~="action" then return end --gamepad action is only one to have effect
	
	char_colors = {} --reset color list
	for i=1,5 do
		if hint_list[i] then
			char_colors[ ustring.upper(hint_list[i]) ] = COLOR_LIST[2] --add back hint chars
			scratch_text[i]:set_text(ustring.upper(hint_list[i])) --codeword change may have changed the hint letter
		else scratch_text[i]:set_text"" end
	end
	
	for _,guess_panel in ipairs(guess_panels) do guess_panel:refresh() end --reset guess panel colors
	for i=1,#slate-1 do --reset button colors, skip reset button
		local button = slate[i]
		local char = slate[button]
		if hint_list[char] then
			button:set_direction(2)
		else button:set_direction(3) end
	end
	
	--reset scratch pad
	local max_count = scratch_list.max_count
	scratch_list = {count=0, max_count=max_count}
	for i = 1,max_count do scratch_list[i] = "" end
	
	--if access mode is active then advance dialog
	if access_mode and ACCESS_MODES[access_mode].clear_slate and not restriction then
		local game = sol.main.get_game()
		if game.advance_dialog then --continue tutorial
			game.guess_analyzer:set_access_mode"lockout" --prevent further actions
			
			sol.timer.start(game, 500, function() --0.5 second delay before advancing dialog
				game.advance_dialog()
			end)
		end
	end
end

--// Returns the color (table, RGB values 0-255) for the specified character based on the slate state
local function get_char_color(_, character) return char_colors[character] end

--// Gets/sets the current gamepad mode
function menu:get_gamepad_mode() return gamepad_mode end
function menu:set_gamepad_mode(mode)
	assert(type(mode)=="string", "Bad argument #1 to 'set_gamepad_mode' (string expected)")
	assert(GAMEPAD_MODES[mode], "Bad argument #1 to 'set_gamepad_mode', invalid mode: "..mode)
	
	if gamepad_mode ~= mode then
		gamepad_mode = mode
		
		--refresh displayed mode text and cursor
		refresh_gamepad_cursor()
	end
end

--// Changes current gamepad mode to next one in sequence
function menu:toggle_gamepad_mode()
	if gamepad_mode=="scratch" or dragged_type=="gamepad" then
		self:gamepad_drop_scratch()
	end
	
	local index = GAMEPAD_MODES[gamepad_mode]
	index = index + 1
	if index > #GAMEPAD_MODES then index = 1 end
	local new_mode = GAMEPAD_MODES[index]
	gamepad_mode = new_mode
	
	--refresh displayed mode text and cursor
	refresh_gamepad_cursor()
end

--// Gets/sets the current access mode
function menu:get_access_mode() return access_mode end
function menu:set_access_mode(mode, condition)
	assert(not mode or type(mode)=="string", "Bad argument #2 to 'set_access_mode' (string or nil expected)")
	assert(not mode or ACCESS_MODES[mode], "Bad argument #2 to 'set_access_mode', invalid mode: "..tostring(mode))
	assert(not condition or type(condition)=="string", "Bad argument #3 to 'set_access_mode' (string or nil expected)")
	
	access_mode = mode or nil
	restriction = condition or nil
	
	--convert restriction as string to other format depending on access mode
	local func = PARSE_RESTRICTIONS[access_mode]
	if func then restriction = func(restriction) end
end

--// Appends character to prompt text
function menu:add_letter(character) prompt_append(text_prompt, character) end

--// Removes last letter of prompt text
function menu:remove_letter() prompt_remove(text_prompt, 1) end

function menu:display_text(text)
	assert(type(text)=="string", "Bad argument #1 to 'display_text' (string expected)")
	error_text:set_text(text)
end

function menu:display_text_key(text_key)
	assert(type(text_key)=="string", "Bad argument #1 to 'display_text_key' (string expected)")
	local text = sol.language.get_string(text_key)
	assert(text, "Bad argument #1 to 'display_text_key', (invalid string key: "..text_key..")")
	error_text:set_text(text)
end

function menu:clear_hints()
	for i=1,5 do
		local char = hint_list[i]
		if char then
			hint_list[i] = nil
			hint_list[char] = nil
			
			local slate_button = slate[char]
			slate_button:set_direction(3)
			char_colors[slate_button:get_text()] = COLOR_LIST[3]
		end
		
		if not scratch_list[i] then scratch_list[i] = "" end
		scratch_text[i]:set_text(ustring.upper(scratch_list[i]))
		scratch_text[i]:set_font_color(COLOR_LIST[3])
	end
	hint_list.count = 0
	scratch_list.max_count = 5
	
	for _,guess_panel in ipairs(guess_panels) do guess_panel:refresh() end --refresh all guess panels
end

function menu:give_hint(is_cost)
	local game = sol.main.get_game()
	if not game then return end
	local map = game:get_map()
	if not map then return end
	
	if hint_list.count >= 5 then return false end --hints for all letters already given
	if access_mode and not ACCESS_MODES[access_mode].give_hint then return false end --access mode does not allow using a hint
	
	--decrement hint counter
	if is_cost~=false then
		if not game:get_property"hints" then return false end --hints not enabled
		local num_hints = game:get_value"hints" or 0
		if num_hints < 1 then return false end --no hints available
		num_hints = num_hints - 1
		game:set_value("hints", num_hints)
		hint_button:set_text(num_hints)
	end
	
	--update savegame stats
	local total_hints = (game:get_value"total_hints" or 0) + 1
	game:set_value("total_hints", total_hints)
	
	local codeword_chars = {}
	local BLUE_COLOR = COLOR_LIST[2]
	local BLACK_COLOR = COLOR_LIST[3]
	
	--get codeword chars in random order
	do local codeword = map:get_codeword()
		local n = 0
		for i = 1,5 do
			local char = ustring.sub(codeword, i,i) --UNICODE multi-codepoint
			if not hint_list[char] then
				n = n + 1
				codeword_chars[n] = char
				codeword_chars[char] = i
			end
		end
		for i = n,1,-1 do
			codeword_chars[n] = table.remove(codeword_chars, math__random(i)) --randomize order
		end
	end
	
	--look for char from codeword not already in scratch pad
	local new_char, char_index
	for i,char in ipairs(codeword_chars) do
		if not scratch_list[char] then
			new_char = char
			char_index = codeword_chars[char]
			break
		end
	end
	
	--remove a letter from scratch
	if not new_char then --player already knows all letters of codeword
		new_char = codeword_chars[1] --choose letter in position 1 (random) to be revealed
		char_index = codeword_chars[new_char]
		
		--find and remove letter from scratch matching the new hint char
		for i=1,#scratch_list do
			local char = scratch_list[i]
			if char==new_char then
				table.remove(scratch_list, i)
				scratch_list[char] = nil
				
				scratch_list.count = scratch_list.count - 1
				
				--note: slate button is already correct color, do nothing
			end
		end
	else
		--find and remove first wrong letter in scratch
		local scratch_index
		if scratch_list.count>0 then
			if scratch_list.count < scratch_list.max_count then --scratch contains at least 1 blank
				for i=1,scratch_list.max_count do
					if scratch_list[i] == "" then
						scratch_index = i
					end
				end
			else --scratch does not contain any blanks
				--put copy of scratch letters in random order
				local scratch_random = {}
				local scratch_count = scratch_list.count
				for i=1,scratch_count do
					local char = scratch_list[i]
					scratch_random[i] = char
					scratch_random[char] = i --reverse lookup to remember original index
				end
				for i=scratch_count,1,-1 do
					scratch_random[scratch_count] = table.remove(scratch_random, math__random(i)) --randomize order
				end
				
				--randomly select a scratch_list char not present in the codeword
				for i,char in ipairs(scratch_random) do
					if not codeword_chars[char] then
						scratch_index = scratch_random[char] --original index
						break
					end
				end
			end
		else scratch_index = scratch_list.max_count end --scratch is all blanks, remove one from end
		
		local scratch_char = table.remove(scratch_list, scratch_index)
		local scratch_char_upper = ustring.upper(scratch_char)
		if scratch_char ~= "" then
			scratch_list[scratch_char] = nil
			scratch_list.count = scratch_list.count - 1
			
			--change color of old slate button to red
			local slate_button = slate[scratch_char]
			slate_button:set_direction(1)
			char_colors[scratch_char_upper] = COLOR_LIST[1]
		end
		
		--change color of new slate button to blue
		local new_char_upper = ustring.upper(new_char)
		local slate_button = slate[new_char]
		slate_button:set_direction(2)
		char_colors[new_char_upper] = BLUE_COLOR
	end
	scratch_list.max_count = scratch_list.max_count - 1
	
	--add new hint char to hint list
	hint_list[char_index] = new_char
	hint_list[new_char] = true
	hint_list.count = hint_list.count + 1
	
	--refresh scratch text and color
	local n = 1
	for i=1,5 do
		local panel_text = scratch_text[i]
		local char = hint_list[i]
		if not char then
			char = scratch_list[n]
			panel_text:set_font_color(BLACK_COLOR)
			n = n + 1
		else panel_text:set_font_color(BLUE_COLOR) end
		panel_text:set_text(ustring.upper(char))
	end
	
	--refresh all guess panels
	for _,guess_panel in ipairs(guess_panels) do guess_panel:refresh() end
	
	--if access mode is active then advance dialog
	if access_mode and ACCESS_MODES[access_mode].give_hint and not restriction then
		if game.advance_dialog then --continue tutorial
			game.guess_analyzer:set_access_mode"lockout" --prevent further actions
			
			sol.timer.start(game, 500, function() --0.5 second delay before advancing dialog
				game.advance_dialog()
			end)
		end
	end
	
	return true
end

function menu:fix_mistakes()
	--TODO
end

--// Submits the text from the prompt field as a new guess, will display error if not valid
function menu:submit_guess(text)
	local game = sol.main.get_game()
	local map = game:get_map()
	if not map then return end
	
	assert(not my_text or type(my_text)=="string", "Bad argument #1 to 'submit_guess' (string or nil expected)")
	
	local guess = text or text_prompt:get_text()
	text_prompt:set_text""
	
	--don't submit guess if restriction is active and does not match
	if access_mode then
		if ACCESS_MODES[access_mode].submit_guess and (not restriction or guess==restriction) then --TODO check if guess and restriction are equivalent, not equal (i.e. convert to lower case)
			if game.advance_dialog then game.advance_dialog() end --continue tutorial
		else return end
	end
	
	--submit guess
	local is_valid, msg = map:add_guess(guess)
	if is_valid then
		local num_matches = tonumber(is_valid) or 5
		guess_count = guess_count + 1
		
		--remove splats if now expired
		local splats_expire = splats.expire
		if splats_expire then
			if splats_expire>1 then
				splats.expire = splats_expire - 1
			else splats = {} end
		end
		
		guess_panels[guess_count]:set_text(ustring.upper(guess))
		match_panels[guess_count]:set_text(num_matches)
		
		if is_valid==true then --found codeword
			map:found_anagram"codeword"
			
			map.opponent:on_win()
			error_text:set_text(msg)
			
			local access_mode = game.guess_analyzer:get_access_mode()
			if not access_mode then --don't end level if tutorial is active
				game.guess_analyzer:set_access_mode"lockout"
				if map.on_hit then map:on_hit() end --TODO better implementation
				
				sol.timer.start(self, 1500, function()
					game.guess_analyzer:set_access_mode(access_mode)
					if not map:next_level() then
						error_text:set_text_key"prompt.codeword_changed"
					end
				end)
			end
		elseif map:get_guess_count() >= map:get_max_guesses() then
			map:game_over()
		elseif num_matches==5 then --anagram found
			error_text:set_text_key"prompt.anagram_found"
			map.opponent:submit_guess(num_matches)
			map:found_anagram(true)
		else
			error_text:set_text"" --clear prompt for valid guess
			map.opponent:submit_guess(num_matches)
			map:found_anagram(false)
		end 
	elseif msg then --display error text
		error_text:set_text(msg)
		sol.audio.play_sound"wrong2"
	end
end

function menu:submit_random_guess()
	--TODO
end

function menu:help_guess()
	--TODO
end

function menu:remove_guess()
	local game = sol.main.get_game()
	local map = game:get_map()
	if not map then return end
	
	text_prompt:set_text""
	
	if guess_count >= 1 then
		guess_panels[guess_count]:set_text("")
		match_panels[guess_count]:set_text("")
		guess_count = guess_count - 1
		
		map:remove_guess()
	end
end

function menu:remove_all_guesses()
	local game = sol.main.get_game()
	local map = game:get_map()
	if not map then return end
	
	text_prompt:set_text""
	
	if guess_count >= 1 then
		for guess_index = 1, guess_count do
			guess_panels[guess_index]:set_text("")
			match_panels[guess_index]:set_text("")
		end
		
		guess_count = 0
		map:remove_all_guesses()
	end
end

--gives player additional guesses (if not maxed out)
function menu:add_guesses(amount)
	--TODO
end

function menu:sequence()
	local COLORS = {2,5,7,6,1,4} --colors sequence
	local NEXT_COLOR = {}
	for i,v in ipairs(COLORS) do NEXT_COLOR[v] = COLORS[i+1] end
	NEXT_COLOR[ COLORS[#COLORS] ] = COLORS[1]
	
	self:set_access_mode"lockout"
	
	local text = sol.language.get_dialog"true_codebreaker".text
	text = text:gsub("\r\n", "\n"):gsub("\r", "\n") --standardize line breaks
	local next_char = ustring.gmatch(text, "([^\n])")
	
	--set colors
	local color_index
	local slate_index = 1
	for row = 1,4 do
		for col = 1,9 do
			if slate_index < #slate then
				if col <= 5 then 
					color_index = (col + row - 2) % 6 + 1
				else color_index = (8 - col + row) % 6 + 1 end
				
				local char = next_char() or ""
				local button = slate[slate_index]
				button:set_text(char)
				button:set_direction(COLORS[color_index])
				slate_index = slate_index + 1
			end
		end
	end
	
	sol.audio.play_sound"awesome"
	sol.timer.start(self, 2000, function() achievements.give"achievement_codebreaker" end)
	
	sol.menu.start_coroutine(self, function()
		--animate colors
		for n=1,35 do
			wait(150)
			for i=1,#slate-1 do
				local button = slate[i]
				button:set_direction(NEXT_COLOR[button:get_direction()])
			end
		end
		
		--reset to original state
		local slate_index = 1
		for i=1,#slate-1 do
			local char = ustring.upper(char_list[slate_index])
			local color = char_colors[char]
			local direction = COLOR_LIST[color] or 3
			local button = slate[slate_index]
			button:set_text(char)
			button:set_direction(direction)
			slate_index = slate_index + 1
		end
		
		self:set_access_mode(false)
	end)
end

--// Updates the displayed timer value in format MM:SS, displays 99:99 if 100 seconds or greater
	--seconds (number, positive integer) amount of time to display in seconds
function menu:set_clock(seconds)
	assert(type(seconds)=="number", "Bad argument #1 to 'set_clock' (number expected)")
	
	local min = math.floor(seconds/60)
	local sec = seconds - 60*min
	if min >= 100 then
		min = 99
		sec = 99
	end
	
	timer_text:set_text(string.format("%2d:%02d", min, sec))
end

--// Adds separators to a number string
local function add_commas(number_string)
	local sep = sol.language.get_string"game_info.score_separator" or ","
	
	local start,text = number_string:reverse():match"(%d)(%d*)"
	if not start then return number_string end
	
	return string.reverse(start..text:gsub("(%d%d)(%d)", "%1"..sep.."%2"))
end

--// Updates the displayed score to the specified value (max 9999999999)
function menu:set_score(score)
	assert(type(score)=="number", "Bad argument #1 to 'set_score' (number expected)")
	score = math.floor(score)
	assert(score>=0, "Bad argument #1 to 'set_score' (number must be non-negative)")
	
	if score>9999999999 then score = 9999999999 end
	
	score_str = add_commas(string.format("%010d", score))
	
	local text = sol.language.get_string"game_info.score"
	score_text:set_text(string.format(text, score_str))
end

--// Updates the displayed level number
function menu:update_level()
	local game = sol.main.get_game()
	local stage = game:get_value"level" or 1
	stage_text:set_text(string.format(
		sol.language.get_string"game_info.stage", stage
	))
	final_text:set_opacity(game:get_property"final"==stage and 255 or 0)
	error_text:set_text""
end

--// Changes the number of visible guess panels based on the max number of guesses
function menu:update_panel_count()
	local game = sol.main.get_game()
	local map = game:get_map()
	if not map then return end
	
	if gate.movement then
		gate.movement:stop()
		gate.movement = nil
	end
	
	local max_guesses = map:get_max_guesses() or 30
	local gate_x,gate_y = gate:get_xy()
	gate_y = gate_y + (gate.y or 0)
	local new_gate_y = 26*(max_guesses/2-1) + 56
	local distance = new_gate_y - gate_y
	
	if distance ~= 0 then
		local movement = sol.movement.create"straight"
		movement:set_speed(88)
		movement:set_angle(math.pi/2*(distance<0 and 1 or 3))
		movement:set_max_distance(math.abs(distance))
		gate.movement = movement
		movement:start(gate, function()
			gate.movement = nil
		end)
	end
end

--// Updates match counts of previous guesses after codeword has changed
function menu:update_guesses()
	local game = sol.main.get_game()
	local map = game:get_map()
	if not map then return end
	
	--update hint letters
	local codeword = map:get_codeword()
	local new_hints = {count=hint_list.count}
	for i = 1,5 do
		if hint_list[i] then
			local char = ustring.sub(codeword,i,i)
			new_hints[i] = char
			new_hints[char] = true
		end
	end
	hint_list = new_hints
	
	for i = 1,guess_count do
		match_panels[i]:set_text( map:get_match_count(i) )
	end
end

function menu:nudge_gamepad_cursor(direction)
	if gamepad_mode=="slate" then
		local index = gamepad_slate_index
		local to_index = SLATE_CURSOR_TO[direction]
		local from_index = SLATE_CURSOR_FROM[direction]
		
		if index==SLATE_AREA and to_index then --cursor is starting from clear button
			index = to_index --move cursor where it should go depending on direction
		elseif index==from_index then --cursor is at button that should move to clear button given this direction
			index = SLATE_AREA --move cursor to clear button
		--otherwise implement normal cursor movements
		elseif direction=="right" then
			repeat
				if index % SLATE_COL_COUNT == 0 then index = index - SLATE_COL_COUNT end
				index = index + 1
			until index <= MAX_SLATE_INDEX or index==SLATE_AREA --keep moving cursor until not on blank space
		elseif direction=="left" then
			repeat
				if index % SLATE_COL_COUNT == 1 then index = index + SLATE_COL_COUNT end
				index = index - 1
			until index <= MAX_SLATE_INDEX or index==SLATE_AREA --keep moving cursor until not on blank space
		elseif direction=="up" then
			repeat
				index = index - SLATE_COL_COUNT
				if index <= 0 then index = index + SLATE_AREA end
			until index <= MAX_SLATE_INDEX or index==SLATE_AREA --keep moving cursor until not on blank space
		elseif direction=="down" then
			repeat
				index = index + SLATE_COL_COUNT
				if index > SLATE_AREA then index = index - SLATE_AREA end
			until index <= MAX_SLATE_INDEX or index==SLATE_AREA --keep moving cursor until not on blank space
		end
		gamepad_slate_index = index
		
		local row = math.floor((index - 1) / SLATE_COL_COUNT)
		local col = (index - 1) % SLATE_COL_COUNT
		gamepad_cursor:set_xy(
			246+16 + 36*col,
			81+16-2 + 36*row
		)
	elseif gamepad_mode=="scratch" then
		local index = gamepad_scratch_index
		
		if direction=="right" then
			index = index + 1
			if index > 5 then index = 1 end
		elseif direction=="left" then
			index = index - 1
			if index < 1 then index = 5 end
		elseif direction=="up" then
			index = 1
		elseif direction=="down" then
			index = 5
		end
		gamepad_scratch_index = index
		
		gamepad_cursor:set_xy(230+32 + 72*(index - 1), 274+32)
		
		--adjust scratch letter positions while dragging
		if dragged_type=="gamepad" then --the gamepad cursor has grabbed a scratch letter
			dragged_new_index = index
			for i = 1,5 do
				if i ~= dragged_index then
					local panel_text = scratch_text[i]
					if index<dragged_index and i>=index and i<dragged_index then
						panel_text:set_drag_offset(72,0) --TODO replace 72 with step variable
					elseif index>dragged_index and i>dragged_index and i<=index then
						panel_text:set_drag_offset(-72,0) --TODO replace 72 with step variable
					else panel_text:set_drag_offset(0,0) end
				else
					local panel_text = scratch_text[dragged_index]
					panel_text:set_drag_offset(72*(index - dragged_index), -5) --TODO replace 72 with step variable
				end
			end
		end
	end
end

function menu:cursor_action(action)
	if gamepad_mode=="slate" then
		local index = gamepad_slate_index
		if index > MAX_SLATE_INDEX then index = SLATE_AREA end --adjust index for clear button
		local button = slate[index]
		if button.on_clicked then button:on_clicked(action) end
	elseif gamepad_mode=="scratch" then
		local index = gamepad_scratch_index
		if dragged_type ~= "gamepad" then
			self:gamepad_grab_scratch()
		else self:gamepad_drop_scratch() end
	end
end

--// Randomizes order of letters in scratch panels
function menu:scramble()
	if access_mode and not ACCESS_MODES[access_mode].scramble then return end --cancel if different access mode is active
	if scratch_list.count==0 or scratch_list.max_count <= 1 then return end --don't do anything if no scratch letters yet or if has 4 or more hints
	
	--Pick new random order for scratch panels, keeps randomizing until different than original
	local value = 0 --(number, integer 0-119) unique value corresponding to the 120 possible letter rearrangements, tentative
	repeat
		local new_scratch = {} --temporarily store scratch list letters in the new order
		local multiplier = 1
		local count = scratch_list.max_count
		for i = 1,scratch_list.max_count-1 do
			local n = math__random(count)
			value = value + (n-1)*multiplier
			multiplier = multiplier*count
			local char = table.remove(scratch_list, n)
			count = count - 1
			new_scratch[i] = char
			if char ~= "" then new_scratch[char] = true end
		end
		
		--only one possibility to choose from when on last letter
		new_scratch[scratch_list.max_count] = scratch_list[1]
		new_scratch[ scratch_list[1] ] = true
		new_scratch.count = scratch_list.count
		new_scratch.max_count = scratch_list.max_count
		scratch_list = new_scratch
	until value>0 --if value is 0 then order did not change, re-roll again
	
	local n = 1
	for i=1,5 do
		if not hint_list[i] then
			scratch_text[i]:set_text(ustring.upper(scratch_list[n]))
			n = n + 1
		end
	end
	sol.audio.play_sound"pause_open"
	
	--if access mode is active then advance dialog
	if access_mode and ACCESS_MODES[access_mode].scramble and not restriction then
		local game = sol.main.get_game()
		if game.advance_dialog then game.advance_dialog() end --continue tutorial
	end
end

function menu:find_anagram()
	--TODO
end

function menu:submit_scratch(item)
	local chars = {}
	local n = 1
	for i=1,5 do
		if not hint_list[i] then
			chars[i] = scratch_list[n]
			n = n + 1
		else chars[i] = hint_list[i] end
	end
	local guess = table.concat(chars)
	self:submit_guess(guess)
end

function menu:gamepad_grab_scratch()
	if gamepad_mode ~= "scratch" then return end
	
	if dragged_timer then
		dragged_timer:stop()
		dragged_timer = nil
	end
	
	dragged_type = "gamepad"
	dragged_index = gamepad_scratch_index
	dragged_new_index = gamepad_scratch_index
	
	local panel_text = scratch_text[dragged_index]
	panel_text:set_drag_offset(0,-5)
	panel_text:set_font_color{57, 128, 110}
end

function menu:gamepad_drop_scratch()
	if gamepad_mode ~= "scratch" or dragged_type ~= "gamepad" then return end
	
	for i=1,5 do scratch_text[i]:set_drag_offset(0,0) end --reset offsets
	
	local panel_text = scratch_text[dragged_index]
	panel_text:set_font_color{0, 0, 0} --TODO restore original font color
	
	if dragged_index ~= dragged_new_index then --only reorder panels if changed
		local moved_text = table.remove(scratch_list, dragged_index)
		table.insert(scratch_list, dragged_new_index, moved_text)
		
		local n = 1
		for i=1,5 do
			if not hint_list[i] then
				scratch_text[i]:set_text(ustring.upper(scratch_list[n]))
				n = n + 1
			end
		end
		
		--if access mode is active then advance dialog
		if access_mode and ACCESS_MODES[access_mode].scramble and not restriction then
			local game = sol.main.get_game()
			if game.advance_dialog then game.advance_dialog() end --continue tutorial
		end
	end
	
	dragged_type = nil
	dragged_index = nil
	dragged_new_index = nil
end

function menu:gamepad_cancel_grab()
	if gamepad_mode ~= "scratch" or dragged_type ~= "gamepad" then return end
	
	for i=1,5 do scratch_text[i]:set_drag_offset(0,0) end --reset offsets
	
	local panel_text = scratch_text[dragged_index]
	panel_text:set_font_color{0, 0, 0} --TODO restore original font color
	
	dragged_type = nil
	dragged_index = nil
	dragged_new_index = nil
end

--// Animates projectiles flying from opponent portrait to randomly chosen match panels
--// which results in a splat sprite that obscures the result
	--num_hits (number, positive integer) number of projectiles to generate
	--turns (number, positive integer) number of turns that the splats persist
--calling removes any existing splat sprites, and the panels randomly chosen for the new splats will be different ones
--number of projectiles will be reduced if not enough splat-free guess panels available
function menu:do_obfuscate_attack(num_hits, turns)
	turns = turns or 3
	
	local index_pool = {} --list of match indices not currently blocked out (select only from these for new ones to block out)
	for i = 1,guess_count do
		if not splats[-i] then table.insert(index_pool, i) end
	end
	splats = {expire=turns}
	
	if #index_pool > 0 then
		if num_hits > #index_pool then num_hits = #index_pool end
		for i = 1,num_hits do
			local sprite = sol.sprite.create"enemies/splat_ball"
			sprite:set_xy(600,345)
			
			local match_index = table.remove(index_pool, math__random(#index_pool))
			local movement = sol.movement.create"target"
			local panel = match_panels[match_index]
			movement:set_target(panel.x+12, panel.y+12)
			movement:set_speed(500)
			movement:start(sprite, function()
				sprite:set_animation"splat"
			end)
			
			table.insert(splats, sprite)
			splats[-match_index] = true
		end
	end
end

--// Locks out player input and reveals the codeword
function menu:do_lose()
	local game = sol.main.get_game()
	local map = game:get_map()
	if not map then return end
	
	self:set_access_mode"lockout"
	error_text:set_text(string.format(
		sol.language.get_string"prompt.game_over", map:get_codeword()
	))
end

menu.clear_slate = clear_slate

--// Pause or unpause
function menu:pause()
	local game = sol.main.get_game()
	if game then game:set_paused(true) end
	
	if sol.menu.is_started(sol.main.menus) then
		--not this will probably not ever get called
		sol.menu.stop(sol.main.menus)
		game:set_paused(false)
	else
		sol.main.menus:start_submenu("pause_menu", function() game:set_paused(false) end)
	end
end

--// Displays quick-reference menu showing the controls
function menu:show_controls()
	--TODO
end

--// Call after starting a new level to clear old data
function menu:reset()
	local game = sol.main.get_game()
	
	if dragged_index then drop_scratch() end
	
	char_colors = {} --clear previous
	scratch_list = {"", "", "", "", "", count=0, max_count=5} --clear previous
	hint_list = {count=0}
	access_mode = nil
	restriction = nil
	guess_count = 0
	dragged_index = nil
	dragged_new_index = nil
	gamepad_mode = "slate"
	splats = {} --table array of sprites to block out match indices
	--also contains values of true at keys equal to -1*match_index for each corresponding index
	
	local is_hints = not not game:get_property"hints"
	hint_button:set_text(game:get_value"hints" or 0)
	hint_button:set_visible(is_hints)
	hint_text:set_opacity(is_hints and 255 or 0)
	
	if dragged_timer then
		dragged_timer:stop()
		dragged_timer = nil
	end
	
	if between_guesses_timer then
		between_guesses_timer:stop()
		between_guesses_timer = nil
	end
	
	local BLACK_COLOR = COLOR_LIST[3]
	for i=1,5 do
		local scratch = scratch_text[i]
		scratch:set_text""
		scratch:set_font_color(BLACK_COLOR)
	end
	
	for i=1,30 do
		guess_panels[i]:set_text""
		match_panels[i]:set_text""
	end
	
	clear_slate()
	text_prompt:set_text""
	error_text:set_text""
	
	--reset access mode and restrictions
	self:set_access_mode(false)
end

--// When menu is started create UI controls and reset to initial state
function menu:on_started()
	local game = sol.main.get_game()
	
	gamepad_mode = GAMEPAD_MODES[1]
	--TODO place gamepad cursor where it should be based on this mode
	
	controls = {}
	char_list = {}
	char_colors = {}
	slate = {}
	scratch = {}
	scratch_text = {}
	scratch_list = {"", "", "", "", "", count=0, max_count=5}
	hint_list = {count=5}
	guess_panels = {}
	match_panels = {}
	
	
	--## load list of valid characters ##--
	
	local char_count = 0 --index of new entry to add
	local chars_str = sol.language.get_string"valid_letters"
	
	for i = 1, ustring.len(chars_str) do --UNICODE multi-codepoint
		local char = ustring.sub(chars_str,i,i)
		
		if not char_list[char] then --ignore duplicate characters
			char_count = char_count + 1
			char_list[char_count] = char
			char_list[char] = true --reverse lookup
		end
	end
	
	
	--## Create Controls ##--
	
	local bg_frame = ui.create_preset("frame.beige", 584, 420)
	bg_frame:set_xy(6, 6)
	controls[#controls+1] = bg_frame
	
	difficulty_text = ui.create_preset"text_surface.game_info"
	difficulty_text:set_xy(230, 24)
	local difficulty = game:get_difficulty()
	if difficulty then
		difficulty_text:set_text_key("game_info.difficulty."..difficulty)
	else difficulty_text:set_text"" end
	
	score_text = ui.create_preset"text_surface.game_info"
	score_text:set_xy(230, 48)
	self:set_score(0)
	
	stage_text = ui.create_preset"text_surface.game_info"
	stage_text:set_xy(494, 48)
	local stage = game:get_value"level" or 1
	stage_text:set_text(string.format(
		sol.language.get_string"game_info.stage", stage
	))
	
	timer_text = ui.create_preset"text_surface.game_info"
	timer_text:set_horizontal_alignment"right"
	timer_text:set_xy(462, 48)
	timer_text:set_text"0:00"
	
	final_text = ui.create_preset"text_surface.game_info"
	final_text:set_xy(494, 24)
	final_text:set_text_key"game_info.final"
	
	--create letter slate (up to 35 letters & clear button)
	local slate_count = 0
	for row = 1, 4 do
		for col = 1, 9 do
			if char_count > slate_count then
				slate_count = slate_count + 1
				local char = char_list[slate_count]
				
				local button = ui.create_preset"button.slate"
				slate[slate_count] = button
				slate[char] = button --lookup
				slate[button] = char --reverse lookup
				controls[#controls+1] = button
				button.x = 246 + 36*(col-1)
				button.y = 81 + 36*(row-1)
				button:set_text(ustring.upper(char))
				button.on_clicked = slate_action
			else break end
		end
	end
	MAX_SLATE_INDEX = slate_count --excludes clear button
	SLATE_ROW_COUNT = math.floor(slate_count/SLATE_COL_COUNT) + 1
	SLATE_AREA = SLATE_ROW_COUNT * SLATE_COL_COUNT
	
	--create clr button
	slate_count = slate_count + 1
	local button = ui.create_preset"button.slate"
	slate[slate_count] = button
	controls[#controls+1] = button
	button.x = 246 + 36*8 --always position at far right
	button.y = 81 + 36*math.floor((slate_count-1)/9)
	button:set_text(sol.language.get_string"slate.clear_button")
	button:set_font_size(tonumber(sol.language.get_string"slate.clear_font_size") or 12)
	button:set_text_xy(0,tonumber(sol.language.get_string"slate.clear_text_y_offset") or 0)
	button:set_direction(8)
	button.on_clicked = clear_slate
	
	--calculate gamepad cursor movements based on number of slate buttons
	local left_index = SLATE_COL_COUNT * (SLATE_ROW_COUNT - 1) + 1
	if left_index > MAX_SLATE_INDEX then left_index = false end
	local right_index = MAX_SLATE_INDEX
	if right_index % SLATE_COL_COUNT == 0 then right_index = false end --do nothing if in right-most column
	local up_index = SLATE_COL_COUNT
	if up_index > MAX_SLATE_INDEX then up_index = false end
	local down_index = SLATE_COL_COUNT * (SLATE_ROW_COUNT - 1)
	if down_index < 1 then down_index = false end
	
	SLATE_CURSOR_FROM = {
		left = left_index,
		right = right_index,
		up = up_index,
		down = down_index,
	}
	
	SLATE_CURSOR_TO = {
		left = right_index,
		right = left_index,
		up = down_index,
		down = up_index,
	}
	
	--create scratch pad
	for i = 1,5 do
		local panel = ui.create_preset"text.scratch_panel"
		scratch[#scratch+1] = panel
		controls[#controls+1] = panel
		panel.index = i
		panel.x = 230 + 72*(i-1)
		panel.y = 274
		panel.on_dragged = drag_scratch
		panel.on_dropped = drop_scratch
	end
	
	--create draggable scratch panel (make separate so on top of other scratch panels)
	for i = 1,5 do
		local panel = ui.create_preset"text.scratch_panel_text"
		scratch_text[#scratch_text+1] = panel
		panel.index = i
		panel.x = 230 + 72*(i-1)
		panel.y = 274
	end
	
	--create guess prompt
	text_prompt = ui.create_preset"text.prompt"
	controls[#controls+1] = text_prompt
	text_prompt.x = 230
	text_prompt.y = 234
	
	--create error text surface
	error_text = ui.create_preset"text_surface.error"
	error_text:set_xy(230+128+8, 234+10)
	
	--create guess panels
	for row = 1,15 do
		for col = 1,2 do
			local guess_panel = ui.create_preset"guess_panel.5"
			guess_panels[2*(row-1) + col] = guess_panel
			controls[#controls+1] = guess_panel
			guess_panel.x = 104*(col-1) + 14
			guess_panel.y = 26*(row-1) + 30
			guess_panel.on_needs_color = get_char_color
			
			local match_panel = ui.create_preset"text.match_panel"
			match_panels[2*(row-1) + col] = match_panel
			controls[#controls+1] = match_panel
			match_panel.x = 104*(col-1) + 84
			match_panel.y = 26*(row-1) + 30
		end
	end
	
	--create gate frame
	gate = ui.create_preset("frame.gate", 208, 288)
	gate:set_xy(10, 420)
	
	hint_button = ui.create_preset"button.hint"
	controls[#controls+1] = hint_button
	hint_button.x = 724
	hint_button.y = 160
	hint_button:set_text("0")
	hint_button.on_clicked = self.give_hint
	
	hint_text = ui.create_preset"text_surface.game_info"
	hint_text:set_horizontal_alignment"right"
	hint_text:set_xy(716, 174)
	hint_text:set_text(sol.language.get_string"game_info.hints")
	
	gamepad_cursor = ui.create_preset"sprite.gamepad_cursor"
	gamepad_cursor:set_xy(246+16, 81+16-2)
	
	self:reset() --initialize
end

function menu:on_finished() self:reset() end

function menu:on_key_pressed(key, modifiers)
	local allowed_actions = ACCESS_MODES[access_mode]
	if not modifiers.control and not modifiers.alt then --no modifiers pressed
		local action = KEY_ACTIONS[key]
		if action and (not allowed_actions or allowed_actions[action]) then
			self[action](self)
			return true
		end
	else
		local modifiers_list = {}
		for _,modifier in ipairs(MODIFIERS) do
			if modifiers[modifier] then table.insert(modifiers_list, modifier) end
		end
		table.insert(modifiers_list, key)
		
		local action = KEY_ACTIONS[table.concat(modifiers_list, "+")]
		if action then
			if not allowed_actions or allowed_actions[action] then
				self[action](self)
				return true
			end
		elseif modifiers.control and not modifiers.shift
		and (not allowed_actions or allowed_actions.slate) then
			if char_list[key] then
				if modifiers.alt then --modifiers control+alt pressed
					slate_toggle_alt(slate[key])
				else slate_toggle(slate[key]) end --control modifier only
				return true
			else
				local key_num = tonumber(key)
				if key_num and COLOR_LIST[key_num] then
					if modifiers.alt then --modifiers control+alt pressed
						--TODO
					else colorize_last_guess(key_num) end
					return true
				end
			end
		end
	end
end

--TODO execute actions on button release instead of button press, and allow repeat presses when button held
function menu:on_joypad_button_pressed(button)
	local allowed_actions = ACCESS_MODES[access_mode]
	local command = gamepad_manager.get_button_action(button)
	
	local action = GAMEPAD_ACTIONS[gamepad_mode][command] --first try mode-specific action
	if not action then action = GAMEPAD_ACTIONS[command] end --then try generic action if not found
	
	if action and (not allowed_actions or allowed_actions[action]) then
		self[action](self, command)
		return true
	end
end

function menu:on_character_pressed(character)
	character = ustring.match(character, "%S")
	local allowed_actions = ACCESS_MODES[access_mode]
	if character and (not allowed_actions or allowed_actions.chars) then --UNICODE multi-codepoint
		self:add_letter(character)
		return true
	end
end

function menu:on_mouse_pressed(mouse_button, x, y)
	for _,control in ipairs(controls) do
		local pos_x, pos_y = control:get_xy()
		local x_min = control.x + pos_x
		local y_min = control.y + pos_y
		local ctrl_width, ctrl_height = control:get_size()
		local x_max = x_min + ctrl_width
		local y_max = y_min + ctrl_height
		
		if control.on_mouse_pressed then
			if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
				if control:on_mouse_pressed(mouse_button, x-x_min, y-y_min) then
					return true
				end
			end
		end
	end
end

function menu:on_mouse_released(mouse_button, x, y)
	for _,control in ipairs(controls) do
		local pos_x, pos_y = control:get_xy()
		local x_min = control.x + pos_x
		local y_min = control.y + pos_y
		local ctrl_width, ctrl_height = control:get_size()
		local x_max = x_min + ctrl_width
		local y_max = y_min + ctrl_height
		
		if control.on_mouse_released then
			if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
				control:on_mouse_released(mouse_button, x-x_min, y-y_min)
			else control:on_mouse_released(mouse_button, nil, nil) end --released outside control bounds
		end
	end
end

function menu:on_draw(dst_surface)
	for _,control in ipairs(controls) do
		control:draw(dst_surface, control.x, control.y)
	end
	difficulty_text:draw(dst_surface)
	score_text:draw(dst_surface)
	stage_text:draw(dst_surface)
	timer_text:draw(dst_surface)
	final_text:draw(dst_surface)
	error_text:draw(dst_surface)
	hint_text:draw(dst_surface)
	for _,sprite in ipairs(splats) do
		sprite:draw(dst_surface)
	end
	gate:draw(dst_surface)
	gamepad_cursor:draw(dst_surface)
	for _,letter in ipairs(scratch_text) do
		letter:draw(dst_surface, letter.x, letter.y)
	end
end

return menu

--[[ Copyright 2019-2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
