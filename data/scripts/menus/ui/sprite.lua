--[[ sprite.lua
	version 1.0a1
	24 May 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script creates an ui subcomponent created from a sprite entity that is drawn onto
	a destination surface. It is possible for the sprite have multiple frames and animate.
]]

local util = require"scripts/menus/ui/util"

local control = {}

-- methods to inherit from sol.sprite
local SPRITE_METHODS = {
	get_animation = true,
	set_animation = true,
	has_animation = true,
	get_direction = true,
	set_direction = true,
	get_num_directions = true,
	get_frame = true,
	set_frame = true,
	get_num_frames = true,
	get_frame_delay = true,
	set_frame_delay = true,
	get_origin = true,
	is_paused = true,
	set_paused = true,
	set_ignore_suspend = true,
	synchronize = true,
	get_size = true,
	get_blend_mode = true,
	set_blend_mode = true,
	fade_in = true,
	fade_out = true,
}

--// Creates a new image control
	--properties (table) - table containing properties defining image behavior
		--sprite (string) - id of the sprite to use
		--unpressed (string) - id of the sprite animation to use for the unpressed state
		--pressed (string) - id of the sprite animation to use for the pressed state
	--returns the newly created button object (table)
function control.create(properties)
	local new_control = {}
	
	--settings defined by data file property values and their default values
	local sprite_id = properties.sprite --(string) animation set id of the sprite
	local direction = properties.direction or 0
	local animation = properties.animation
	
	--additional settings
	local sprite --(sol.sprite) sprite that is drawn for this component
	local position = {x=0, y=0} --(table, key/value) movements change the coordinates of this table, which are added as an offset to the component position when drawn
		--x (number, integer) - amount of the horizontal offset in pixels
		--y (number, integer) - amount of the vertical offset in pixels
		--movement (sol.movement or nil) - active movement of the component, if nil then the movement is done
	local is_enabled = true --(boolean) determines animation of sprite, using "enabled" if true or "disabled" if false
	local is_visible = true --(boolean) component is not drawn if false, default: true
	
	
	--// validate data file property values
	
	assert(type(properties)=="table", "Bad argument #1 to 'create' (table expected)")
	
	--validate sprite_id
	assert(type(sprite_id)=="string", "Bad property sprite_id to 'create' (string expected)")
	sprite = sol.sprite.create(sprite_id)
	assert(sprite, "Error in 'create', sprite cannot be found: "..sprite_id)
	
	
	--//implementation
	
	--inherit methods of sol.sprite or sol.text_surface
	setmetatable(new_control, { __index = function(self, name)
		local func_name = name
		
		if SPRITE_METHODS[name] then
			if type(SPRITE_METHODS[name])=="string" then --value is the name of the sprite method to use
				func_name = SPRITE_METHODS[name]
			end
			
			return function(_, ...) return sprite[func_name](sprite, ...) end
		else return function() end end
	end})
	
	--// Returns the path (string) of the source image file
	function new_control:get_sprite_id() return sprite_id end
	
	--// Get/set whether the text is enabled, which uses a different font color when disabled
		--value (boolean) - if true then the text is enabled
	function new_control:get_enabled() return is_enabled end
	function new_control:set_enabled(value)
		assert(type(value)=="boolean", "Bad argument #2 to 'set_enabled' (boolean expected)")
		
		if is_enabled ~= value then
			is_enabled = value
		end
	end
	
	--// Get/set whether the image is visible (newly created images are visible by default)
		--value (boolean) - if true then the image is visible
	function new_control:get_visible() return is_visible end
	function new_control:set_visible(value)
		assert(type(value)=="boolean", "Bad argument #2 to 'set_visible' (boolean expected)")
		is_visible = value
	end
	
	--// Get/set the x & y offset that is added to the position of where the component is drawn
	function new_control:get_xy() return position.x, position.y end
	function new_control:set_xy(x, y)
		local x = tonumber(x)
		assert(x, "Bad argument #2 to 'set_xy' (number expected)")
		
		local y = tonumber(y)
		assert(y, "Bad argument #3 to 'set_xy' (number expected)")
		
		position.x = x
		position.y = y
	end
	
	--// Get the current movement of the component (or nil if none)
	function new_control:get_movement() return position.movement end
	--// Assign a movement to the component and start it
		--movement (sol.movement) - movement to apply to the component
		--callback (function, optional) - function to be called once the movement has finished
	function new_control:start_movement(movement, callback)
		position.movement = movement --save reference to active movement
		movement:start(position, function(...)
			position.movement = nil --remove reference once movement is done
			if callback then callback(...) end
		end)
	end
	--// Stop the current movement of the component if it exists
	function new_control:stop_movement()
		local movement = position.movement --convenience
		if movement then
			movement:stop()
			position.movement = nil
		end
	end
	
	--// Called when mouse button is pressed while cursor is on button
		--arg1 mouse_button (string): name of the mouse button pressed
		--arg2 x (number): X coordinate of cursor relative to upper-left corner of button
		--arg3 y (number): Y coordinate of cursor relative to upper-left corner of button
	function new_control:on_mouse_pressed(mouse_button, x, y)
		if not is_visible or not is_enabled then return false end
		
		--TODO drag and drop
		if mouse_button=="left" then
			if self.on_dragged then return self:on_dragged(mouse_button, x, y) end
		end
		
		return false
	end
	
	--// Called whenever mouse button is released. If mouse cursor is not over the button
	--// at time of release then x & y are nil
		--arg1 mouse_button (string): name of the mouse button released
		--arg2 x (number): X coordinate of cursor relative to upper-left corner of button
		--arg3 y (number): Y coordinate of cursor relative to upper-left corner of button
	function new_control:on_mouse_released(mouse_button, x, y)
		if not is_visible or not is_enabled then return false end
		
		if mouse_button=="left" then
			--TODO end drag and drop
			if self.on_dropped then return self:on_dropped(mouse_button, x, y) end
		end
		
		return false
	end
	
	--// Draws the image on the specified destination surface
		--dst_surface (sol.surface) - surface on which to draw the image
		--x (number, optional) - x coordinate of where to draw the image
		--y (number, optional) - y coordinate of where to draw the image
	function new_control:draw(dst_surface, x, y)
		if is_visible then
			local x = (x or 0) + position.x
			local y = (y or 0) + position.y
			
			sprite:draw(dst_surface, x, y)
		end
		
	end
	
	return new_control
end

return control

--[[ Copyright 2016-2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
