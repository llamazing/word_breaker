--[[text_surface.lua
	version 1.0a1
	21 May 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script creates and returns a sol.text_surface using properties in the ui.dat data
	file.
]]

local util = require"scripts/menus/ui/util"

local control = {}

--multiplication constant to determine vertical or horizontal position based on alignment keyword
local ALIGNMENT_OFFSETS = {
	left = 0,
	center = 0.5,
	right = 1,
	top = 0,
	middle = 0.5,
	bottom = 1,
}

--// Creates a new image control
	--properties (table) - table containing properties defining image behavior
		--sprite (string) - id of the sprite to use
		--unpressed (string) - id of the sprite animation to use for the unpressed state
		--pressed (string) - id of the sprite animation to use for the pressed state
	--returns the newly created button object (table)
function control.create(properties)
	--settings defined by data file property values and their default values
	local font = properties.font --(string) name of font to use for text
	local font_size = properties.font_size --(number, positive) font size to use for text
	local font_color --(table, array) RGB color of text when enabled
	local horizontal_alignment = properties.horizontal_alignment or "left" --(string, optional) horizontal alignment of text: "left", "center" or "right", default: "left"
	local vertical_alignment = properties.vertical_alignment or "middle" --(string, optional) vertical alignment of text: "top", "middle" or "bottom", default: "middle"
	local rendering_mode = properties.rendering_mode or "solid" --(string, optional) how text is drawn: "solid" (faster) or "antialiasing" (smoothing effect), default: "solid"
	local font_hinting = properties.font_hinting or "normal" --(string, optional) the font hinting: "normal", "mono", "light" or "none", default: "normal"
	local font_kerning = properties.font_kerning --(boolean, optional) the font kerning: true or false, default: true
		if font_kerning==nil then font_kerning = true end
	local text = properties.text or "" --(string, optional) initial content of text, default: ""
	local text_key = properties.text_key --(string, optional) strings.dat key of localized text to use as contents, default: nil (will use text property for contents instead)
	
	
	--## validate data file property values
	
	assert(type(properties)=="table", "Bad argument #1 to 'create' (table expected)")
	
	--validate font
	assert(type(font)=="string", "Bad property font to 'create' (string expected)")
	
	--validate font_size
	if font_size then
		font_size = tonumber(font_size)
		assert(font_size, "Bad property font_size to 'create' (number or nil expected)")
		assert(font_size>0, "Bad property font_size to 'create' (number must be positive)")
	end
	
	--validate font_color
	local err --temporary error message
	font_color, err = util.make_RGB_color(properties.font_color, 3) --don't allow alpha values
	assert(font_color, "Bad property font_color to 'create'"..tostring(err or ''))
	--font_color is now a table with 3 RGB values
	
	--validate horizontal_alignment
	assert(type(horizontal_alignment)=="string", "Bad property horizontal_alignment to 'create' (string or nil expected)")
	assert(ALIGNMENT_OFFSETS[horizontal_alignment], "Bad property horizontal_alignment to 'create', invalid string value: "..horizontal_alignment)
	
	--validate vertical_alignment
	assert(type(vertical_alignment)=="string", "Bad property vertical_alignment to 'create' (string or nil expected)")
	assert(ALIGNMENT_OFFSETS[vertical_alignment], "Bad property vertical_alignment to 'create', invalid string value: "..vertical_alignment)
	
	--validate rendering_mode
	assert(type(rendering_mode)=="string", "Bad property rendering_mode to 'create' (string or nil expected)")
	
	--validate font_hinting
	assert(type(font_hinting)=="string", "Bad property font_hinting to 'create' (string or nil expected)")
	
	--validate font_hinting
	assert(type(font_kerning)=="boolean", "Bad property font_kerning to 'create' (boolean or nil expected)")
	
	--validate text
	assert(type(text)=="string", "Bad property text to 'create' (string or nil expected)")
	
	--validate text_key
	assert(not text_key or type(text_key)=="string", "Bad property text_key to 'create' (string or nil expected)")
	
	
	--## implementation
	
	return sol.text_surface.create{
		horizontal_alignment = horizontal_alignment,
		vertical_alignment = vertical_alignment,
		font = font,
		rendering_mode = rendering_mode,
		color = font_color,
		font_size = font_size,
		font_hinting = font_hinting,
		--font_kerning = font_kerning,
		text = text,
		text_key = text_key,
	}
end

return control

--[[ Copyright 2016-2019 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
