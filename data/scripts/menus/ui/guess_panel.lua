--[[ guess_panel.lua
	version 1.0a1
	21 May 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script creates an ui subcomponent to display text which can set the color of each
	character independently. An optional background sprite can be specified.
	
	Whenever the colors need to be updated, call the :refresh() method. 
]]

local ustring = require"scripts/lib/ustring/ustring"
local util = require"scripts/menus/ui/util"

local control = {}

--convenience
local get_text_size = sol.text_surface.get_predicted_size

-- methods to inherit from sol.sprite
local SPRITE_METHODS = {
	get_animation = true,
	set_animation = true,
	has_animation = true,
	get_direction = true,
	set_direction = true,
	get_num_directions = true,
	get_frame = true,
	set_frame = true,
	get_num_frames = true,
	get_frame_delay = true,
	set_frame_delay = true,
	get_origin = true,
	is_paused = true,
	set_paused = true,
	set_ignore_suspend = true,
	synchronize = true,
	get_size = true,
	get_blend_mode = true,
	set_blend_mode = true,
	fade_in = true,
	fade_out = true,
}

--methods to inherit from sol.text_surface
local TEXT_SURFACE_METHODS = {
	get_font = true,
	set_font = true,
	get_rendering_mode = true,
	set_rendering_mode = true,
	get_font_size = true,
	set_font_size = true,
	get_text = true,
	get_text_size = "get_size", --text_surface method has different name: "get_size"
}

--TODO add drawable methods applicable to both sprite & text surface

--multiplication constant to determine vertical or horizontal position based on alignment keyword
local ALIGNMENT_OFFSETS = {
	left = 0,
	center = 0.5,
	right = 1,
	top = 0,
	middle = 0.5,
	bottom = 1,
}

--// Creates a new image control
	--properties (table) - table containing properties defining image behavior
		--sprite (string) - id of the sprite to use
		--unpressed (string) - id of the sprite animation to use for the unpressed state
		--pressed (string) - id of the sprite animation to use for the pressed state
	--returns the newly created button object (table)
function control.create(properties)
	local new_control = {}
	
	--settings defined by data file property values and their default values
	local sprite_id = properties.sprite --(string) animation set id of the sprite
	local direction = properties.direction or 0
	local animation = properties.animation
	local font = properties.font --(string) name of font to use for text
	local font_size = properties.font_size --(number, positive) font size to use for text
	local font_color --(table, array) RGB color of text when enabled
	local font_color_disabled --(table, array, optional) RGB color of text when disabled, default: font_color also used when disabled
	local horizontal_alignment = properties.horizontal_alignment or "center" --(string, optional) horizontal alignment of text: "left", "center" or "right", default: "left"
	local vertical_alignment = properties.vertical_alignment or "middle" --(string, optional) vertical alignment of text: "top", "middle" or "bottom", default: "top"
	local rendering_mode = properties.rendering_mode or "solid" --(string, optional) how text is drawn: "solid" (faster) or "antialiasing" (smoothing effect), default: "solid"
	local font_hinting = properties.font_hinting or "normal" --(string, optional) the font hinting: "normal", "mono", "light" or "none", default: "normal"
	local font_kerning = properties.font_kerning --(boolean, optional) the font kerning: true or false, default: true
		if font_kerning==nil then font_kerning = true end
	local text = properties.text or "" --(string, optional) initial content of text, default: ""
	local text_key = properties.text_key --(string, optional) strings.dat key of localized text to use as contents, default: nil (will use text property for contents instead)
	local text_origin_x = properties.text_origin_x or 0 --(number) x coordinate of where to anchor text in pixels
	local text_origin_y = properties.text_origin_y or 0 --(number) y coordinate of where to anchor text in pixels
	
	--additional settings
	local sprite --(sol.sprite) sprite that is drawn for this component
	local text_surface --(sol.text_surface) text surface that renders the text
	local text_buffer --(sol.surface) intermediate surface to draw text on
	local position = {x=0, y=0} --(table, key/value) movements change the coordinates of this table, which are added as an offset to the component position when drawn
		--x (number, integer) - amount of the horizontal offset in pixels
		--y (number, integer) - amount of the vertical offset in pixels
		--movement (sol.movement or nil) - active movement of the component, if nil then the movement is done
	local is_enabled = true --(boolean) determines animation of sprite, using "enabled" if true or "disabled" if false
	local is_visible = true --(boolean) component is not drawn if false, default: true
	
	
	--// validate data file property values
	
	assert(type(properties)=="table", "Bad argument #1 to 'create' (table expected)")
	
	--validate sprite_id
	assert(type(sprite_id)=="string", "Bad property sprite_id to 'create' (string expected)")
	sprite = sol.sprite.create(sprite_id)
	assert(sprite, "Error in 'create', sprite cannot be found: "..sprite_id)
	
	--validate font
	assert(type(font)=="string", "Bad property font to 'create' (string expected)")
	
	--validate font_size
	if font_size then
		font_size = tonumber(font_size)
		assert(font_size, "Bad property font_size to 'create' (number or nil expected)")
		assert(font_size>0, "Bad property font_size to 'create' (number must be positive)")
	end
	
	--validate font_color
	local err --temporary error message
	font_color, err = util.make_RGB_color(properties.font_color, 3) --don't allow alpha values
	assert(font_color, "Bad property font_color to 'create'"..tostring(err or ''))
	--font_color is now a table with 3 RGB values
	
	--validate font_color_disabled
	if properties.font_color_disabled then
		font_color_disabled, err = util.make_RGB_color(properties.font_color_disabled, 3) --don't allow alpha values
		assert(font_color, "Bad property font_color to 'create'"..tostring(err or ''))
	end --font_color_disabled is now a table with 3 RGB values
	
	--validate horizontal_alignment
	assert(type(horizontal_alignment)=="string", "Bad property horizontal_alignment to 'create' (string or nil expected)")
	assert(ALIGNMENT_OFFSETS[horizontal_alignment], "Bad property horizontal_alignment to 'create', invalid string value: "..horizontal_alignment)
	
	--validate vertical_alignment
	assert(type(vertical_alignment)=="string", "Bad property vertical_alignment to 'create' (string or nil expected)")
	assert(ALIGNMENT_OFFSETS[vertical_alignment], "Bad property vertical_alignment to 'create', invalid string value: "..vertical_alignment)
	
	--validate rendering_mode
	assert(type(rendering_mode)=="string", "Bad property rendering_mode to 'create' (string or nil expected)")
	
	--validate font_hinting
	assert(type(font_hinting)=="string", "Bad property font_hinting to 'create' (string or nil expected)")
	
	--validate font_hinting
	assert(type(font_kerning)=="boolean", "Bad property font_kerning to 'create' (boolean or nil expected)")
	
	--validate text
	assert(type(text)=="string", "Bad property text to 'create' (string or nil expected)")
	
	--validate text_key
	assert(not text_key or type(text_key)=="string", "Bad property text_key to 'create' (string or nil expected)")
	
	--//implementation
	
	text_surface = sol.text_surface.create{
		horizontal_alignment = "left", --horizontal_alignment,
		vertical_alignment = "top", --vertical_alignment,
		font = font,
		rendering_mode = rendering_mode,
		color = font_color,
		font_size = font_size,
		font_hinting = font_hinting,
		font_kerning = font_kerning,
		text = text,
		text_key = text_key,
	}
	--text_surface:set_blend_mode"multiply"
	
	text_buffer = sol.surface.create(sprite:get_size()) --TODO add width/height
	
	--inherit methods of sol.sprite or sol.text_surface
	setmetatable(new_control, { __index = function(self, name)
		local func_name = name
		
		if SPRITE_METHODS[name] then
			if type(SPRITE_METHODS[name])=="string" then --value is the name of the sprite method to use
				func_name = SPRITE_METHODS[name]
			end
			
			return function(_, ...) return sprite[func_name](sprite, ...) end
		elseif TEXT_SURFACE_METHODS[name] then
			if type(TEXT_SURFACE_METHODS[name])=="string" then --value is the name of the text_surface method to use
				func_name = TEXT_SURFACE_METHODS[name]
			end
			
			return function(_, ...) return text_surface[func_name](text_surface, ...) end
		else return function() end end
	end})
	
	function new_control:refresh()
		local text = self:get_text()
		local text_width,text_height = text_surface:get_size()
		local panel_width,panel_height = text_buffer:get_size()
		local horz_offset = (panel_width-text_width)*ALIGNMENT_OFFSETS[horizontal_alignment]
		local vert_offset = (panel_height-text_height)*ALIGNMENT_OFFSETS[vertical_alignment]
		
		text_buffer:clear()
		
		local char_start = 0 --left position of current character
		local char_end = 0 --right position of current character
		local prev_color
		local color_start = 0 --left position of prev color character(s)
		for i = 1, ustring.len(text) do --UNICODE multi-codepoint
			local char = ustring.sub(text,i,i) --UNICODE multi-codepoint
			local color = self.on_needs_color and self:on_needs_color(char) or false -- false means use default color
			
			local sub_str = ustring.sub(text,1,i) --UNICODE multi-codepoint
			local sub_width = get_text_size(font, font_size, sub_str)
			
			if color ~= prev_color then --always false for first character
				text_surface:set_color(prev_color or font_color)
				text_surface:draw_region(
					color_start,
					0,
					char_start - color_start,
					text_height,
					text_buffer,
					horz_offset + color_start,
					vert_offset
				)
				
				color_start = char_start
				prev_color = color
			end
			
			char_start = sub_width
		end
		
		--save final segment info
		text_surface:set_color(prev_color or font_color)
		text_surface:draw_region(
			color_start,
			0,
			char_start - color_start,
			text_height,
			text_buffer,
			horz_offset + color_start,
			vert_offset
		)
	end
	
	function new_control:set_text(text)
		text = text or ""
		assert(type(text)=="string", "Bad argument #2 to 'set_text' (string or nil expected)")
		
		text_surface:set_text(text)
		self:refresh()
	end
	
	function new_control:set_text_key(key) self:set_text(sol.language.get_string(key)) end
	
	--// Returns the path (string) of the source image file
	function new_control:get_sprite_id() return sprite_id end
	
	--// Get/set whether the text is enabled, which uses a different font color when disabled
		--value (boolean) - if true then the text is enabled
	function new_control:get_enabled() return is_enabled end
	function new_control:set_enabled(value)
		assert(type(value)=="boolean", "Bad argument #2 to 'set_enabled' (boolean expected)")
		
		if is_enabled ~= value then
			is_enabled = value
			
			if font_color_disabled then
				text_surface:set_color(is_enabled and font_color or font_color_disabled)
			end --don't bother changing color if disabled color is not defined
		end
	end
	
	--// Get/set whether the image is visible (newly created images are visible by default)
		--value (boolean) - if true then the image is visible
	function new_control:get_visible() return is_visible end
	function new_control:set_visible(value)
		assert(type(value)=="boolean", "Bad argument #2 to 'set_visible' (boolean expected)")
		is_visible = value
	end
	
	--// Get/set the x & y offset that is added to the position of where the component is drawn
	function new_control:get_xy() return position.x, position.y end
	function new_control:set_xy(x, y)
		local x = tonumber(x)
		assert(x, "Bad argument #2 to 'set_xy' (number expected)")
		
		local y = tonumber(y)
		assert(y, "Bad argument #3 to 'set_xy' (number expected)")
		
		position.x = x
		position.y = y
	end
	
	--// Get the current movement of the component (or nil if none)
	function new_control:get_movement() return position.movement end
	--// Assign a movement to the component and start it
		--movement (sol.movement) - movement to apply to the component
		--callback (function, optional) - function to be called once the movement has finished
	function new_control:start_movement(movement, callback)
		position.movement = movement --save reference to active movement
		movement:start(position, function(...)
			position.movement = nil --remove reference once movement is done
			if callback then callback(...) end
		end)
	end
	--// Stop the current movement of the component if it exists
	function new_control:stop_movement()
		local movement = position.movement --convenience
		if movement then
			movement:stop()
			position.movement = nil
		end
	end
	
	--// Called when mouse button is pressed while cursor is on button
		--arg1 mouse_button (string): name of the mouse button pressed
		--arg2 x (number): X coordinate of cursor relative to upper-left corner of button
		--arg3 y (number): Y coordinate of cursor relative to upper-left corner of button
	function new_control:on_mouse_pressed(mouse_button, x, y)
		if not is_visible or not is_enabled then return false end
		
		--TODO drag and drop
	end
	
	--// Called whenever mouse button is released. If mouse cursor is not over the button
	--// at time of release then x & y are nil
		--arg1 mouse_button (string): name of the mouse button released
		--arg2 x (number): X coordinate of cursor relative to upper-left corner of button
		--arg3 y (number): Y coordinate of cursor relative to upper-left corner of button
	function new_control:on_mouse_released(mouse_button, x, y)
		if not is_visible or not is_enabled then return false end
		
		if mouse_button=="left" then
			--TODO end drag and drop
		end
		
		return false
	end
	
	--// Draws the image on the specified destination surface
		--dst_surface (sol.surface) - surface on which to draw the image
		--x (number, optional) - x coordinate of where to draw the image
		--y (number, optional) - y coordinate of where to draw the image
	function new_control:draw(dst_surface, x, y)
		if is_visible then
			local x = x + position.x
			local y = y + position.y
			
			sprite:draw(dst_surface, x, y)
			text_buffer:draw(dst_surface, x, y)
		end
		
	end
	
	return new_control
end

return control

--[[ Copyright 2016-2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
