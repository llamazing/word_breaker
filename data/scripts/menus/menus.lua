--[[ menus.lua
	version 0.1a1
	25 Aug 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This menu script processes user inputs to navigate the submenus invoked from the title
	screen and pause screen menus. The content of each submenu is defined in the menus.dat
	data file.
]]

local module_name = ...

local gamepad_manager = require"scripts/gamepad_manager"
local credits = require"scripts/menus/credits"
local settings = require"scripts/settings"

local menu = {}

local menu_list = {} --(table, key/value) list of required menus, populated by data file
local settings_data = {} --(table, key/value) settings data read from menus.dat; key is id string with data table as value
local submenu_data = {} --(table, key/value) submenu data read from menus.dat; key is id string with data table as value
local submenus = {} --(table, key/value) all submenus; key is id string with menu as value
local submenu_ids = {} --(table, key/value) same as submenus except keys & values are reversed
local active_submenus = {} --(table, array) list of active submenus
	--note: only the highest index submenu is really active and lower index ones get re-opened when top one is closed
local submenu_callbacks = {} --(table, array) list of callbacks that are called when corresponding submenu in active_submenus is closed

--converts keyboard key to action, where entry is the currently selected submenu entry
local KEY_ACTIONS = {
	up = function(entry) entry:next_entry(-1) end,
	down = function(entry) entry:next_entry(1) end,
	left = function(entry) entry:adjust_entry(-1) end,
	right = function(entry) entry:adjust_entry(1) end,
	--action = function(entry) entry:activate() end,
	--attack = function(entry) entry:get_submenu():close() end,
	
	space = function(entry) entry:activate() end,
	['return'] = function(entry) entry:activate() end,
	escape = function(entry) menu:prev_submenu() end,
}

--converts gamepad command to action, where entry is the currently selected submenu entry
local GAMEPAD_ACTIONS = {
	up = function(entry) entry:next_entry(-1) end,
	down = function(entry) entry:next_entry(1) end,
	left = function(entry) entry:adjust_entry(-1) end,
	right = function(entry) entry:adjust_entry(1) end,
	action = function(entry) entry:activate() end,
	back = function(entry) menu:prev_submenu() end,
	--pause = --TODO
}

--possible values for setting types
local SETTING_TYPES = {
	boolean = true,
	slider = true,
}

--multiplication constant to determine vertical or horizontal position based on alignment keyword
local ALIGNMENT_OFFSETS = {
	left = 0,
	center = 0.5,
	right = 1,
	top = 0,
	middle = 0.5,
	bottom = 1,
}

local function create_submenu(submenu_id)
	local submenu_info = submenu_data[submenu_id]
	assert(submenu_info, "Bad argument #1 to 'create_submenu' (string expected)")
	assert(submenu_info, "Bad argument #1 to 'create_submenu', invalid submenu id: "..submenu_id)
	
	local submenu = {}
	local title_surfaces = nil
	local entries = {}
	local sel_index = 1
	
	--// Returns the currently selected submenu entry (table, key/value)
	function submenu:get_selected_entry() return entries[sel_index] end
	
	--// Event called whenever the language is changed while the submenu is open
	function submenu:on_language_changed()
		local title = submenu_info.title
		
		--create text surface for each line of title
		if title and title_surfaces then
			title = sol.language.get_string(title).."\\n"
			local title_index = 1
			for line in title:gmatch"(.-)\\n" do
				local title_surface = title_surfaces[title_index]
				if not title_surface then
					title_surface = sol.text_surface.create{
						font = submenu_info.font,
						font_size = submenu_info.font_size,
						color = submenu_info.font_color,
						horizontal_alignment = submenu_info.horizontal_alignment,
						vertical_alignment = submenu_info.vertical_alignment,
						rendering_mode = submenu_info.rendering_mode,
						font_hinting = submenu_info.font_hinting,
						--font_kerning = submenu_info.font_kerning,
						text = line,
					}
				else title_surface:set_text(line) end
				title_index = title_index + 1
			end
			
			--delete any extra text_surfaces (if former language had more lines in title)
			for i=#title_surfaces,title_index,-1 do
				title_surface[i] = nil
			end
			
			--set vertical position of each title text surface
			local offset = 0.5
			for i=#title_surfaces,1,-1 do
				offset = offset + 1
				local title_surface = title_surfaces[i]
				title_surface:set_xy(
					submenu_info.x,
					submenu_info.y-submenu_info.y_offset*offset
				)
			end
		end
		
		--create each entry of submenu
		for i,entry in ipairs(submenu_info.entries) do
			if i>#entries then break end
			local entry_id = entry.id
			
			--create control and add to entries table if visible
			local visibility = entry.visibility --TODO if visibility conditions changed then unexpected things could happen, make more robust
			if visibility==nil or (visibility and visibility(entry_id)) then
				local entry_title = sol.language.get_string(entry.title)
				if entry.substitution then --make substitution of entry text using string.format()
					entry_title = entry_title:format(entry.substitution(entry_id))
				end
				
				--update text and set position
				local new_entry = entries[i]
				new_entry.text_surface:set_text(entry_title)
				new_entry.text_surface:set_xy(
					submenu_info.x,
					submenu_info.y+(i-1)*submenu_info.y_offset
				)
				
				--create surface to display icon
				local icon_id = entry.icon
				if icon_id then
					is_lang,file_name = icon_id:match"(%&?)(.+)" --ustring.match(icon_id, "(%!?)(.+)")
					if is_lang then --true unless badly formatted string
						local text_width = new_entry.text_surface:get_size()
						is_lang = is_lang:len()>0
						local surface = sol.surface.create(file_name, is_lang)
						--local surface_width,surface_height = surface:get_size()
						surface:set_xy(
							submenu_info.x+text_width/2+4,
							submenu_info.y+(i-1)*submenu_info.y_offset-16 --TODO better fix for -16 offset
						)
						new_entry.surface = surface
						new_entry.icon = icon_id
					else new_entry.surface = nil end
				else new_entry.surface = nil end
			end
		end
	end
	
	--// Event called each time the submenu is started
	function submenu:on_started()
		local title = submenu_info.title
		title_surfaces = {id = title}
		if title then
			title = sol.language.get_string(title).."\\n"
			for line in title:gmatch"(.-)\\n" do
				local title_surface = sol.text_surface.create{
					font = submenu_info.font,
					font_size = submenu_info.font_size,
					color = submenu_info.font_color,
					horizontal_alignment = submenu_info.horizontal_alignment,
					vertical_alignment = submenu_info.vertical_alignment,
					rendering_mode = submenu_info.rendering_mode,
					font_hinting = submenu_info.font_hinting,
					--font_kerning = submenu_info.font_kerning,
					text = line,
				}
				
				table.insert(title_surfaces, title_surface)
			end
			
			local offset = 0.5
			for i=#title_surfaces,1,-1 do
				offset = offset + 1
				local title_surface = title_surfaces[i]
				title_surface:set_xy(
					submenu_info.x,
					submenu_info.y-submenu_info.y_offset*offset
				)
			end
		end
			
		for i,entry in ipairs(submenu_info.entries) do
			local entry_id = entry.id
			
			--create control and add to entries table if visible
			local visibility = entry.visibility
			
			if visibility==nil or (visibility and visibility(entry_id)) then
				local entry_title = sol.language.get_string(entry.title)
				if entry.substitution then
					entry_title = entry_title:format(entry.substitution(entry_id))
				end
				local new_entry = {
					id = entry.id,
					submenu_id = submenu_id,
					title = entry.title,
					action = entry.action,
					horizontal_alignment = submenu_info.horizontal_alignment,
					vertical_alignment = submenu_info.vertical_alignment,
					text_surface = sol.text_surface.create{
						font = submenu_info.font,
						font_size = submenu_info.font_size,
						color = submenu_info.font_color,
						horizontal_alignment = submenu_info.horizontal_alignment,
						vertical_alignment = submenu_info.vertical_alignment,
						rendering_mode = submenu_info.rendering_mode,
						font_hinting = submenu_info.font_hinting,
						--font_kerning = submenu_info.font_kerning,
						text = entry_title,
					},
					--font_color = submenu_info.font_color,
					--selected_color = submenu_info.selected_color,
				}
				new_entry.text_surface:set_xy(
					submenu_info.x,
					submenu_info.y+(i-1)*submenu_info.y_offset
				)
				
				--create additional text for settings entries
				local setting_id = entry.setting_id
				if entry.setting_id then
					
					local setting_info = settings_data[setting_id]
					
					local value
					if setting_info.get_value then
						value = setting_info.get_value()
					else
						value = settings.get_value(setting_id)
						if value==nil then
							value = setting_info.default
							settings.set_value(setting_id, value)
						end
					end
					
					if setting_info.type=="slider" then
						local min = setting_info.min or 0
						local max = setting_info.max or 100
						
						if value < min then
							value = min
						elseif value > max then
							value = max
						end
					elseif setting_info.type=="boolean" then
						local off_key = setting_info.off_key
						local on_key = setting_info.on_key
						value = sol.language.get_string(value and on_key or off_key)
					end
					
					new_entry.setting_text = sol.text_surface.create{
						font = submenu_info.font,
						font_size = submenu_info.font_size,
						color = submenu_info.font_color,
						horizontal_alignment = "left",
						vertical_alignment = submenu_info.vertical_alignment,
						rendering_mode = submenu_info.rendering_mode,
						font_hinting = submenu_info.font_hinting,
						--font_kerning = submenu_info.font_kerning,
						text = value,
					}
					new_entry.setting_text:set_xy(
						submenu_info.x + 32,
						submenu_info.y+(i-1)*submenu_info.y_offset
					)
					new_entry.text_surface:set_horizontal_alignment"right"
				end
				
				local icon_id = entry.icon
				if icon_id then
					is_lang,file_name = icon_id:match"(%&?)(.+)" --ustring.match(icon_id, "(%!?)(.+)")
					if is_lang then
						local text_width = new_entry.text_surface:get_size()
						is_lang = is_lang:len()>0
						local surface = sol.surface.create(file_name, is_lang)
						--local surface_width,surface_height = surface:get_size()
						surface:set_xy(
							submenu_info.x+text_width/2+4,
							submenu_info.y+(i-1)*submenu_info.y_offset-16 --TODO better fix for -16 offset
						)
						new_entry.surface = surface
						new_entry.icon = icon_id
					end
				end
				
				table.insert(entries, new_entry)
				entries[entry.id] = new_entry
			
				function new_entry:next_entry(delta)
					local old_index = sel_index
					
					sel_index = sel_index + delta
					if sel_index < 1 then
						sel_index = #entries
					elseif sel_index > #entries then
						sel_index = 1
					end
					
					if old_index ~= sel_index then
						local old_entry = entries[old_index]
						local old_color = submenu_info.font_color
						old_entry.text_surface:set_color(old_color)
						
						local new_entry = entries[sel_index]
						local new_color = submenu_info.selected_color
						new_entry.text_surface:set_color(new_color)
					end
				end
				
				function new_entry:adjust_entry(delta)
					if entry.adjust then
						local new_value = entry.adjust(entry.id, delta)
						
						if entry.setting_id then
							local setting_info = settings_data[entry.setting_id]
							if setting_info.type=="boolean" then
								local off_key = setting_info.off_key
								local on_key = setting_info.on_key
								new_value = sol.language.get_string(new_value and on_key or off_key)
							end
							
							self.setting_text:set_text(new_value)
						end
					end
				end
				
				function new_entry:activate()
					if self.action then
						local new_value = self.action(self.id, delta)
						
						if entry.setting_id then
							local setting_info = settings_data[entry.setting_id]
							if setting_info.type=="boolean" then
								local off_key = setting_info.off_key
								local on_key = setting_info.on_key
								new_value = sol.language.get_string(new_value and on_key or off_key)
							end
							
							self.setting_text:set_text(new_value)
						end
					end
				end
			end
		end
		
		--determine index of entry to be selected initially
		local starting_index = submenu_info.selected or 1
		if type(starting_index)=="function" then
			starting_index = tonumber(starting_index(submenu_info.id) or 1)
			assert(type(starting_index)=="number", "Bad return value from 'selected' callback function in 'submenu' (must return number or nil)")
			starting_index = math.floor(starting_index)
			assert(starting_index >= 1, "Bad return value from 'selected' callback function in 'submenu (number must be positive)")
			assert(starting_index <= #submenu_info.entries, "Bad return value from 'selected' callback function in 'submenu' (value of "..starting_index.." exceeds max index of "..#submenu_info.entries..")")
		end
		
		sel_index = starting_index
		entries[sel_index].text_surface:set_color(submenu_info.selected_color)
	end
	
	--// Event called each time the submenu is closed
	function submenu:on_finished()
		--all destroy controls
		entries = {}
	end
	
	--[[ unused
	function submenu:on_command_pressed(command)
		local action = COMMAND_ACTIONS[command]
		if action then action(self:get_selected_entry()) return true end
	end
	]]
	
	--// Event to process raw keyboard key presses
	function submenu:on_key_pressed(key)
		local action = KEY_ACTIONS[key]
		if action then action(self:get_selected_entry()) return true end
	end
	
	--// Event to process raw keyboard key releases
	function submenu:on_command_released(command)
		--TODO allow press and hold
	end
	
	--// Event to process raw joypad button presses
	function submenu:on_joypad_button_pressed(button)
		local command = gamepad_manager.get_button_action(button) --convert button to command
		local action = GAMEPAD_ACTIONS[command] --lookup action for command
		if action then action(self:get_selected_entry()); return true end
	end
	
	--// Event to process mouse button presses
	function submenu:on_mouse_pressed(button, x, y)
		--TODO fix horz alignment
		for _,entry in ipairs(entries) do
			local pos_x, pos_y = entry.text_surface:get_xy()
			local x_min = (entry.x or 0) + pos_x
			local y_min = (entry.y or 0) + pos_y
			local entry_width, entry_height = entry.text_surface:get_size()
			local x_max = x_min + entry_width
			local y_max = y_min + entry_height
			
			if entry.on_mouse_pressed then
				if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
					if entry:on_mouse_pressed(button, x-x_min, y-y_min) then
						return true
					end
				end
			end
		end
		
		return true
	end
	
	--// Event to process mouse button releases
	function submenu:on_mouse_released(button, x, y)
		for _,entry in ipairs(entries) do
			local pos_x, pos_y = entry.text_surface:get_xy()
			local entry_width, entry_height = entry.text_surface:get_size()
			
			--adjust for vertical and horizontal alignment
			local horz_offset = (ALIGNMENT_OFFSETS[entry.horizontal_alignment] or 0)*entry_width
			local vert_offset = (ALIGNMENT_OFFSETS[entry.vertical_alignment] or 0)*entry_height
			
			local x_min = (entry.x or 0) + pos_x - entry_width/2
			local y_min = (entry.y or 0) + pos_y - entry_height/2 + 10 --TODO better implementation for +8 to remove overlap
			local x_max = x_min + entry_width
			local y_max = y_min + entry_height - 17 --TODO better implementation for -8 to remove overlap
			
			if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
				if entry.on_mouse_released then
					control:on_mouse_released(mouse_button, x-x_min, y-y_min)
				end
				
				if entry.action then entry.action(entry.id) end --TODO better implementation (both click on and release within bounds)
			elseif entry.on_mouse_released then
				control:on_mouse_released(mouse_button, nil, nil) --released outside control bounds
			end
		end
		
		return true
	end
	
	--// Event called each draw frame to render submenu components
	function submenu:on_draw(dst_surface)
		--draw bg fill
		local fill = submenu_info.fill
		if fill then dst_surface:fill_color(fill) end
		
		--draw title text line(s)
		for _,title_surface in ipairs(title_surfaces) do
			title_surface:draw(dst_surface)
		end
		
		--draw selectable text entries
		for _,entry in ipairs(entries) do
			entry.text_surface:draw(dst_surface)
			if entry.setting_text then entry.setting_text:draw(dst_surface) end
			if entry.surface then entry.surface:draw(dst_surface) end
		end
	end
	
	return submenu
end

function menu:start_submenu(submenu_id, callback)
	assert(type(submenu_id)=="string", "Bad argument #2 to 'start_submenu' (string expected)")
	local data = submenu_data[submenu_id]
	assert(data, "Bad argument #2 to 'start_submenu', invalid submenu id: "..submenu_id)
	
	if not sol.menu.is_started(self) then sol.menu.start(sol.main, self) end
	
	local submenu = submenus[submenu_id]
	assert(not sol.menu.is_started(submenu), "Error in menus: submenu is already open: "..submenu_id)
	
	--stop top-most submenu
	local active_submenu = active_submenus[#active_submenus]
	if active_submenu then sol.menu.stop(active_submenu) end
	
	--start new submenu
	sol.menu.start(self, submenu)
	table.insert(active_submenus, submenu)
	table.insert(submenu_callbacks, callback or false)
end

--// Event called whenever the language is changed while the menu is open
	--updates window title text and calls on_language_changed() event for all submenus
function menu:on_language_changed()
	--TODO check if title menu is active first
	if self.title and self.title.on_language_changed then
		self.title.on_language_changed()
	end
	
	--TODO should this be only active submenus?
	for _,submenu in pairs(submenus) do
		submenu:on_language_changed()
	end
	
	local title_name = sol.language.get_string"title.name"
	sol.video.set_window_title(title_name.." - Solarus "..sol.main.get_solarus_version())
end

--// Closes the top-most active submenu then re-opens the one below it
--// Also closes the main menu when the last active submenu is closed
	--returns true if an active submenu was found to close, else false
function menu:prev_submenu(id, ...)
	local is_success
	
	local num_submenus = #active_submenus
	local active_submenu = active_submenus[num_submenus]
	local active_id = submenu_ids[active_submenu]
	if submenu_data[active_id].is_prevent_close then return false end
	
	if active_submenu and num_submenus >= 1 then
		--stop active submenu
		sol.menu.stop(active_submenu)
		table.remove(active_submenus)
		local callback = table.remove(submenu_callbacks)
		
		--re-activate previous submenu
		local prev_submenu = active_submenus[#active_submenus]
		if prev_submenu then
			sol.menu.start(self, prev_submenu)
			is_success = true
		else --all submenus now closed so exit master menu too
			sol.menu.stop(self)
			is_succes = false
		end
		
		if callback then callback(id, ...) end
	else
		sol.menu.stop(self)
		is_success = false
	end
	
	return is_success
end

--// Closes all active submenus and main menu
	--returns true if at least one submenu was closed, else returns false
function menu:close_all(id, ...)
	local is_success = false
	while self:prev_submenu(id, ...) do is_success = true end
	
	return is_success
end

--// Event that is called every time the main menu is started
function menu:on_started()
	active_submenus = {}
	submenu_callbacks = {}
end

--// Read menus.dat data file when script is loaded the first time
do
	local env = { --allow core functions
		_VERSION = _VERSION,
		string = string,
		table = table,
		math = math,
		bit = bit,
		type = type,
		tonumber = tonumber,
		tostring = tostring,
		ipairs = ipairs,
		pairs = pairs,
		next = next,
		select = select,
		unpack = unpack,
		rawget = rawget,
		rawset = rawset,
		rawequal = rawequal,
		pcall = pcall,
		xpcall = xpcall,
		print = print,
		error = error,
		assert = assert,
	}
	setmetatable(env, {__index = function() return function() end end})
	
	--// Calls the require() function for a list of menu scripts provided in menus.dat
	--// Then the menu can be started by using start_menu(id) from within menu.dat functions
		--properties (table, array) - list of scripts to be required, where each value is a a table (key/value) with the following keys:
			--id (string) - unique script_id to be passed to the start_menu() function
			--path (string) - the file path to the script relative to the data directory
	function env.menu_script(properties)
		if type(properties)=="string" then properties = {path=properties} end --convert string to table
		assert(type(properties)=="table", "Bad argument #1 to 'menu_script' (table or string expected)")
		
		local path = properties.path or properties[1]
		assert(type(path)=="string", "Bad value to property 'path' in 'menu_script' (string expected)")
		local new_menu = require(path)
		
		local id = properties.id
		if id then
			assert(type(id)=="string", "Bad value to property 'id' in 'menu_script' (string or nil expected)")
			assert(not menu_list[id], "Duplicate id detected in 'menu_script': "..id)
			menu_list[id] = new_menu
		end
	end
	
	function env.setting(properties)
		assert(type(properties)=="table", "Bad argument #1 to 'setting' (table expected)")
		
		local id = properties.id
		assert(type(id)=="string", "Bad value to property 'id' in 'setting' (string expected)")
		assert(not settings_data[id], "Duplicate id detected in 'setting': "..id)
		
		local setting_type = properties.type
		assert(type(setting_type)=="string", "Bad value to property 'type' in 'setting' (string expected)")
		
		--TODO more validation
		
		local data = {
			id = id,
			type = setting_type,
			default = properties.default,
			off_key = properties[1], --TODO change from index to key
			on_key = properties[2], --TODO change from index to key
			get_value = properties.get_value,
			set_value = properties.set_value,
			min = properties.min,
			max = properties.max,
			steps = properties.steps,
		}
		
		settings_data[id] = data
	end
	
	--// Creates a submenu
		--properties (table, combo)
		--TODO see menus.dat
	function env.submenu(properties)
		assert(type(properties)=="table", "Bad argument #1 to 'submenu' (table expected)")
		
		
		--## Overall submenu properties
		
		local id = properties.id
		assert(type(id)=="string", "Bad value to property 'id' in 'submenu' (string expected)")
		assert(not submenu_data[id], "Duplicate id detected in properties to 'submenu': "..id)
		
		local meta = properties.extend
		if meta then
			assert(type(meta)=="table", "Bad value to property 'extend' (table or nil expected)")
			setmetatable(properties, {__index = meta})
		end
		
		local title = properties.title~=false and (properties.title or "menu."..id)
		assert(not title or type(title)=="string", "Bad value to property 'title' in 'submenu' (string or nil expected)")
		
		assert(type(properties.font)=="string", "Bad value to property 'font' in 'submenu' (string expected)")
		
		local font_size = tonumber(properties.font_size)
		assert(font_size or not properties.font_size, "Bad value to property 'font_size' in 'submenu' (number or nil expected)")
		
		assert(type(properties.font_color)=="table" or not properties.font_color, "Bad value to property 'font_color' in 'submenu' (table or nil expected)")
		assert(type(properties.selected_color)=="table" or not properties.selected_color, "Bad value to property 'selected_color' in 'submenu' (table or nil expected)")
		
		local horz_alignment = properties.horizontal_alignment
		assert(type(horz_alignment)=="string" or not horz_alignment, "Bad value to property 'horizontal_alignment' in 'submenu' (string or nil expected)")
		
		local vert_alignment = properties.vertical_alignment
		assert(type(vert_alignment)=="string" or not vert_alignment, "Bad value to property 'vertical_alignment' in 'submenu' (string or nil expected)")
		
		assert(type(properties.rendering_mode)=="string" or not properties.rendering_mode, "Bad value to property 'rendering_mode' in 'submenu' (string or nil expected)")
		
		assert(type(properties.font_hinting)=="string" or not properties.font_hinting, "Bad value to property 'font_hinting' in 'submenu' (string or nil expected)")
		
		assert(properties.font_kerning==true or not properties.font_kerning, "Bad value to property 'font_kerning' in 'submenu' (boolean or nil expected)")
		
		local is_prevent_close = properties.is_prevent_close
		assert(not is_prevent_close or is_prevent_close==true, "Bad value to property 'is_prevent_close' in 'submenu' (boolean or nil expected)")
		
		local selected = properties.selected
		local selected_index = tonumber(selected)
		assert(not selected or selected_index or type(selected)=="function", "Bad value to property 'selected' in 'submenu' (number or function or nil expected)")
		if selected_index then
			selected = math.floor(selected_index)
			assert(selected >= 1, "Bad value to property 'selected' in 'submenu' (number must be positive)")
			assert(selected <= #properties, "Bad value to property 'selected' in 'submenu' (value of "..selected.." exceeds max index of "..#properties..")")
			selected_index = nil
		end
		
		local x = tonumber(properties.x or 0)
		assert(x, "Bad value to property 'x' in 'submenu' (number or nil expected)")
		local y = tonumber(properties.y or 0)
		assert(y, "Bad value to property 'y' in 'submenu' (number or nil expected)")
		
		local x_offset = tonumber(properties.x_offset or 0)
		assert(x_offset, "Bad value to property 'x_offset' in 'submenu' (number or nil expected)")
		local y_offset = tonumber(properties.y_offset or 0)
		assert(y_offset, "Bad value to property 'y_offset' in 'submenu' (number or nil expected)")
		
		assert(type(properties.fill)=="table" or not properties.fill, "Bad value to property 'fill' in 'submenu' (table or nil expected)")
		
		
		--## Properties for each entity
		
		local entries = {}
		local new_submenu_data = {
			id = id,
			title = title,
			entries = entries,
			font = properties.font,
			font_size = font_size,
			font_color = properties.font_color,
			selected_color = properties.selected_color,
			horizontal_alignment = horz_alignment,
			vertical_alignment = vert_alignment,
			rendering_mode = properties.rendering_mode,
			font_hinting = properties.font_hinting,
			font_kerning = properties.font_kerning,
			is_prevent_close = is_prevent_close or nil,
			selected = selected,
			x = x,
			y = y,
			x_offset = x_offset,
			y_offset = y_offset,
			fill = properties.fill,
		}
		submenu_data[id] = new_submenu_data
		
		for i,entry in ipairs(properties) do
			local entry_id = entry.id
			assert(type(entry_id)=="string", "Bad value to property entry #"..i.." 'id' in 'submenu' (string expected)")
			assert(not entries[entry_id], "Duplicate entry id detected in properties to 'submenu': "..entry_id)
			
			local entry_title = entry.title or (entry.title~=false and "menu."..entry_id)
			assert(type(entry_title)=="string" or not entry_title, "Bad value to property entry #"..i.." 'title' in 'submenu' (string or false or nil expected)")
			
			local visibility = entry.visibility
			assert(type(visibility)=="function" or not visibility, "Bad value to property entry #"..i.." 'visibility' in 'submenu' (function or nil expected)")
			
			local action = entry.action
			if action==nil then action = env.next_menu end
			assert(not action or type(action)=="function", "Bad value to property entry #"..i.." 'action' (function or nil expected)")
			
			local icon = entry.icon
			assert(type(icon)=="string" or not icon, "Bad value to property entry #"..i.." 'icon' in 'submenu' (string or nil expected)")
			
			local substitution = entry.substitution
			assert(not substitution or type(substitution)=="function", "Bad value to property entry #"..i.." 'substitution' in 'submenu' (function or nil expected)")
			
			local setting_id = entry.setting_id
			assert(not setting_id or type(setting_id)=="string", "Bad value to property entry #"..i.." 'setting_id' in 'submenu' (string or nil expected)")
			
			--local get_value = entry.get_value
			--assert(not get_value or type(get_value)=="function", "Bad value to property entry #"..i.." 'get_value' in 'submenu' (function or nil expected)")
			
			--local set_value = entry.set_value
			--assert(not set_value or type(set_value)=="function", "Bad value to property entry #"..i.." 'set_value' in 'submenu' (function or nil expected)")
			
			local adjust = entry.adjust
			assert(not adjust or type(adjust)=="function", "Bad value to property entry #"..i.." 'adjust' in 'submenu' (function or nil expected)")
			
			local new_entry = {
				id=entry_id,
				title = entry_title,
				visibility = visibility,
				action = action,
				icon = icon,
				substitution = substitution,
				
				setting_id = setting_id,
				--get_value = get_value,
				--set_value = set_value,
				adjust = adjust,
			}
			entries[entry_id] = new_entry
			entries[i] = new_entry
		end
		
		local new_submenu = create_submenu(id)
		submenus[id] = new_submenu
		submenu_ids[new_submenu] = id --reverse lookup
	end
	
	local SAVEGAME_NAMES = {
		"casual.dat",
		"normal.dat",
	}
	--// Returns number of savegame files that exist
	function env.num_savegames()
		local count = 0
		for _,file_name in ipairs(SAVEGAME_NAMES) do
			if sol.file.exists(file_name) then count = count + 1 end
		end
		
		return count
	end
	
	--// Returns true if the specified savegame file exists, else false
		--id (string) - name of the savegame file (excluding extension), e.g. "casual"
	function env.is_savegame(id)
		assert(type(id)=="string", "Bad argument #1 to 'is_savegame' (string expected)")
		return sol.file.exists(id..".dat")
	end
	
	--// Reads a savegame value from the specified savegame file
		--id (string) - name of the savegame file (excluding extension), e.g. "casual"
		--key (string) - key of the savegame variable to read the value of
		--default (any, optional) - if the value of the savegame variable is nil, then it will return the default value instead
		--returns (string or number or boolean or nil) - value of the savegame variable
			--no return if the savegame file does not exist
	function env.read_savegame_value(id, key, default)
		local file_name = id..".dat"
		local game
		
		if sol.game.exists(file_name) then game = sol.game.load(file_name) end
		if not game then return end
		
		local value = game:get_value(key)
		if value==nil then
			return default
		else return value end
	end
	
	--// Starts the specified submenu on top of the current submenu
		--id (string) - the id of the submenu to open
		--note: the current submenu is closed but will be re-opened automatically when the submenu above it is closed
	function env.next_menu(id)
		assert(type(id)=="string", "Bad argument #1 to 'next_menu' (string expected)")
		--TODO check valid id
		
		menu:start_submenu(id)
	end
	
	--// Closes the current submenu and opens the one below it (see menu:prev_submenu())
	function env.prev_menu()
		menu:prev_submenu()
	end
	
	--// Begins a new game with the specified difficulty
		--id (string) - difficulty of the new game to start, e.g. "casual"
	function env.new_game(id)
		sol.menu.stop_all(sol.main)
		
		local difficulty = id
		local game = sol.game_manager.new(difficulty)
		game:start()
		
		return game
	end
	
	--// Loads and starts an existing savegame for the specified difficulty
		--id (string) - difficulty of the new game to start, e.g. "casual"
	function env.load_game(id)
		sol.menu.stop_all(sol.main)
		
		local difficulty = id
		local game = sol.game_manager.start(difficulty)
		game:start()
		
		return game
	end
	
	--// Returns the value of a savegame variable for the currently running game
		--savegame_variable (string) - the key for the savegame variable to be read
		--returns (string or number or boolean or nil) - value of the savegame variable; nil if not defined
	function env.get_value(savegame_variable)
		assert(type(savegame_variable)=="string", "Bad argument #1 to 'get_value' (string expected)")
		
		local game = sol.main:get_game()
		if game then
			return game:get_value(savegame_variable)
		else return end
	end
	
	--// Sets the value of a savegame variable for the currently running game
		--savegame_variable (string) - the key for the savegame variable to be read
		--value (string or number or boolean or nil) - value of the savegame variable; nil to clear the variable
	function env.set_value(savegame_variable, value)
		assert(type(savegame_variable)=="string", "Bad argument #1 to 'set_value' (string expected)")
		
		local game = sol.main:get_game()
		if game then
			return game:set_value(savegame_variable, value)
		else return end
	end
	
	--// Returns the value of a settings variable
		--key (string) - the key for the settings variable to be read
		--returns (string or number or boolean or nil) - value of the settings variable; nil if not defined
	function env.get_setting(key)
		assert(type(key)=="string", "Bad argument #1 to 'get_setting' (string expected)")
		
		return settings.get_value(key)
	end
	
	--// Returns true if the specified menu script is running
		--script_id (string) - id of a menu script listed in the menus_scripts{} function
		--returns (boolean) - true if the menu is active, else false
	function env.is_menu_started(script_id)
		assert(type(script_id)=="string", "Bad argument #1 to 'is_menu_started' (string expected)")
		local new_menu = menu_list[script_id]
		assert(new_menu, "Bad argument #1 to 'is_menu_started', invalid script id: "..script_id)
		
		return sol.menu.is_started(new_menu)
	end
	
	--// Starts the specified menu script using the main menu as the context
		--script_id (string) - id of a menu script listed in the menus_scripts{} function
	function env.start_menu(script_id)
		assert(type(script_id)=="string", "Bad argument #1 to 'start_menu' (string expected)")
		local new_menu = menu_list[script_id]
		assert(new_menu, "Bad argument #1 to 'start_menu', invalid script id: "..script_id)
		
		sol.menu.start(menu, new_menu)
	end
	
	--// Begins the tutorial
	function env.start_tutorial()
		if sol.game.exists"tutorial" then sol.game.delete"tutorial" end
		local game = env.new_game"tutorial"
		
		sol.timer.start(sol.main, 50, function() --TODO use unregisterable event instead ON_GAME_STARTED
			game:start_dialog"tutorial.start" end
		)
	end
	
	--// Begins the credits
	function env.start_credits()
		credits:start_dialog"credits.text.original"
	end
	
	--// Returns number of languages available
	function env.num_languages()
		return #sol.language.get_languages()
	end
	
	function env.toggle_setting(id)
		local setting_info = settings_data[id]
		if not setting_info then return end
		
		local value
		if setting_info.get_value then
			value = setting_info.get_value()
		else value = settings.get_value(id) end
		value = not value --toggle
		
		if setting_info.set_value then
			setting_info.set_value(value)
		else settings.set_value(id, value) end
		
		return value
	end
	
	function env.adjust_setting(id, delta)
		local setting_info = settings_data[id]
		if not setting_info then return end
		
		--find old value
		local old_value
		if setting_info.get_value then
			old_value = setting_info.get_value()
		else old_value = settings.get_value(id) end
		
		--bounds/step data
		local min = setting_info.min or 0
		local max = setting_info.max or 100
		local step_count = setting_info.steps or 21
		local step = math.floor((max - min)/(step_count - 1))
		
		--calculate new value
		local new_value = math.floor(delta + (old_value - min)/step + 0.5) --add 0.5 to round nearest
		new_value = new_value*step + min
		
		if new_value > max then
			new_value = max
		elseif new_value < min then
			new_value = min
		end
		
		--set new value
		if setting_info.set_value then
			setting_info.set_value(new_value)
		else settings.set_value(id, new_value) end
		
		return new_value
	end
	
	--// Returns the name of the current language (see sol.language.get_language_name())
	function env.get_language_name(...) return sol.language.get_language_name(...) end
	
	--// Makes the next language the active one (in the order of sol.language.get_languages())
	function env.toggle_language()
		--read settings file to check if languages are enabled
		local is_language_enabled = settings.get_value"languages_enabled"
		if not is_language_enabled then return end
		
		local lang_id = sol.language.get_language()
		
		--find next language to change to
		local lang_list = sol.language.get_languages()
		for i,lang in ipairs(lang_list) do
			if lang_id==lang then
				local next_id = lang_list[i % #lang_list + 1]
				sol.language.set_language(next_id)
				break
			end
		end
		
		--update text_surface text for all submenus
		menu:on_language_changed()
	end
	
	function env.get_fullscreen() return sol.video.is_fullscreen() end
	function env.set_fullscreen(bool)
		local value = sol.video.set_fullscreen(bool)
		sol.video.set_window_size(sol.video.get_quest_size()) --TODO bug where window size not preserved, must set window size again
		return value
	end
	
	function env.get_sound_volume() return sol.audio.get_sound_volume() end
	function env.set_sound_volume(value) return sol.audio.set_sound_volume(value) end
	
	function env.get_music_volume() return sol.audio.get_music_volume() end
	function env.set_music_volume(value) return sol.audio.set_music_volume(value) end
	
	--// Closes all submenus
		--id (string) - the id of submenu entry invoking this call
	function env.close_menu(id)
		menu:close_all(id)
	end
	
	--// Performs a hard reset, returning to the title screen
	function env.exit_to_title()
		sol.main.reset()
	end
	
	--// Exits the program
	function env.exit()
		sol.main.exit()
	end
	
	local data_path = module_name..".dat"
	local chunk = sol.main.load_file(data_path)
	assert(chunk, "Error in menus.lua: unable to load data file: "..data_path)
	setfenv(chunk, env)
	chunk()
end

return menu

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
