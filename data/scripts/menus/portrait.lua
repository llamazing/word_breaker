--[[ portrait.lua
	version 0.1a1
	5 Jun 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This menu script displays a heart bar above the boss corresponding to its HP. The boss
	loses one heart each time the player guesses the codeword, and the maximum heart count
	depends on the current difficulty mode. The player wins once the boss HP reaches zero.
]]

local ui = require"scripts/menus/ui/ui"

local menu = {}

local controls = {}

local SPRITE_X = 678
local SPRITE_Y = 418

local bg_frame = ui.create_preset("frame.opponent", 172, 224) --200
bg_frame:set_xy(594, 202) --226
bg_frame:set_blend_mode"multiply"
function bg_frame:on_clicked()
	local game = sol.main.get_game()
	if not game then return end
	local map = game:get_map()
	if not map then return end
	
	map.opponent:play_taunt"taunt"
end
controls[#controls+1] = bg_frame

local boss_sprite = sol.sprite.create"enemies/robot"
boss_sprite:set_xy(SPRITE_X, SPRITE_Y)
controls[#controls+1] = boss_sprite

local hearts_image = sol.surface.create"menus/hearts.png"
local hearts_surface = sol.surface.create(6*28, 24) --up to 6 hearts max
hearts_surface:set_xy(596, 212)
controls[#controls+1] = hearts_surface

local boss_HP = 0
local max_HP = 0
local is_hearts_visible = false

function menu:refresh()
	local game = sol.main.get_game()
	
	is_hearts_visible = game:get_property"final" == (game:get_value"level" or 1)
	if is_hearts_visible then
		local max_HP = game:get_property"boss_hp"
		local boss_HP = game:get_value"boss_hp" or max_HP
		
		hearts_surface:clear()
		local x,y = 0,0
		for i = 1, max_HP do
			if i <= boss_HP then --draw full heart
				hearts_image:draw_region(56, 0, 28, 24, hearts_surface, x, y)
			else --draw empty heart
				hearts_image:draw_region(0, 0, 28, 24, hearts_surface, x, y)
			end
			
			x = x + 28
		end
	end
end

function menu:set_animation(...)
	boss_sprite:set_animation(...)
end

function menu:set_portrait(id)
	if id then
		boss_sprite = sol.sprite.create("enemies/"..id)
		boss_sprite:set_xy(SPRITE_X, SPRITE_Y)
	else boss_sprite = nil end
end

function menu:on_started()
	self:refresh()
end

function menu:on_finished()
	is_hearts_visible = false
end

function menu:on_mouse_pressed(mouse_button, x, y)
	for _,control in ipairs(controls) do
		local pos_x, pos_y = control:get_xy()
		local x_min = (control.x or 0) + pos_x
		local y_min = (control.y or 0) + pos_y
		local ctrl_width, ctrl_height = control:get_size()
		local x_max = x_min + ctrl_width
		local y_max = y_min + ctrl_height
		
		if control.on_mouse_pressed then
			if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
				if control:on_mouse_pressed(mouse_button, x-x_min, y-y_min) then
					return true
				end
			end
		end
	end
end

function menu:on_mouse_released(mouse_button, x, y)
	for _,control in ipairs(controls) do
		local pos_x, pos_y = control:get_xy()
		local x_min = (control.x or 0) + pos_x
		local y_min = (control.y or 0) + pos_y
		local ctrl_width, ctrl_height = control:get_size()
		local x_max = x_min + ctrl_width
		local y_max = y_min + ctrl_height
		
		if control.on_mouse_released then
			if x>=x_min and x<=x_max and y>=y_min and y<=y_max then
				control:on_mouse_released(mouse_button, x-x_min, y-y_min)
			else control:on_mouse_released(mouse_button, nil, nil) end --released outside control bounds
		end
	end
end

function menu:on_draw(dst_surface)
	bg_frame:draw(dst_surface)
	if boss_sprite then boss_sprite:draw(dst_surface) end
	if is_hearts_visible then
		hearts_surface:draw(dst_surface)
	end
end

return menu

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
