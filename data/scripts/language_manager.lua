--[[ langiage_manager.lua
	version 0.1a1
	18 May 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script updates text_surface text when changing the language is changed.
	
	This script is not currently used.
]]

local text_meta = sol.main.get_metatable"text_surface"

local text_surface_list = setmetatable({}, {__mode = 'k'})

local function update_text_lang()
	for text_surface in pairs(text_surface_list) do
		local key = text_surface._key
		if key then
			text_surface:set_text_key(key)
		end
	end
end

local create_raw = sol.text_surface.create
function sol.text_surface.create(properties, ...)
	local text_key = properties.text_key
	local text_surface = create_raw(properties, ...)
	text_surface._key = text_key
	text_surface_list[text_surface] = true
	
	local set_key_raw = text_surface.set_text_key
	function text_surface:set_text_key(key)
		set_key_raw(self, key)
		text_surface._key = key
	end
	
	local set_text_raw = text_surface.set_text
	function text_surface:set_text(text)
		set_text_raw(self, text)
		text_surface._key = nil
	end
	return text_surface
end

local set_lang_raw = sol.language.set_language
function sol.language.set_language(language_id)
	set_lang_raw(language_id)
	update_text_lang()
end

local get_string_raw = sol.language.get_string
function sol.language.get_string(key, language_id)
	if language_id then
		local old_lang_id = sol.language.get_language()
		set_lang_raw(language_id)
		local new_string = get_string_raw
		set_lang_raw(old_lang_id)
		return new_string
	else return get_string_raw(key) end
end

local get_dialog_raw = sol.language.get_dialog
function sol.language.get_dialog(dialog_id, language_id)
	if language_id then
		local old_lang_id = sol.language.get_language()
		set_lang_raw(language_id)
		local new_dialog = get_dialog_raw
		set_lang_raw(old_lang_id)
		return new_dialog
	else return get_dialog_raw(dialog_id) end
end

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
