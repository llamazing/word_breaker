--[[ map_manager.lua
	version 0.1a1
	23 Feb 2022
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script manages progression of the current level (i.e. map) by choosing the hidden
	codeword, by rating and keeping track of the player's guesses, by attempting to thwart
	the player's progress (i.e. monkey wrench), and by evaluating win/loss conditions.
]]

local ustring = require"scripts/lib/ustring/ustring"
local opponent = require"scripts/opponents/opponent"
local achievements = require"scripts/achievements"
local debug = require"scripts/menus/debug"
local credits = require"scripts/menus/credits"
local stats_menu = require"scripts/menus/final_stats"

local map_meta = sol.main.get_metatable"map"

local MUSIC_TRACKS = {
	"Arcade-Puzzler_v001.ogg",
	"Cryptic-Puzzler2.ogg",
	"Cyber-Puzzles.ogg",
	"Funky-Gameplay_Looping.ogg",
	"Mysterious-Puzzle(no_loop).ogg",
	"Pixelin-It-Around-Town.ogg",
	"Puzzle-Game_Looping.ogg",
	"Puzzling-Curiosities.ogg",
}


local function initialize(map)
	local game = map:get_game()
	local hero = map:get_hero()
	
	local TIMER_START --(number, non-negative integer) value in seconds that timer began counting down from
	
	local codeword --(string or nil) current codeword, lowercase
	local codeword_chars = {}
	local guess_list = {} --(table, combo) up to 30 entries with guess (string) as value
	local max_guesses = 30 --(number, positive integer) total guesses available to player, value gets reduced as stages progress
	local anagram_count = 0 --(number, non-negative integer) running tally of consecutive anagrams found, resets when streak ends
	local time_remaining --(number, integer) time remaining in seconds, negative values for elapsed time in seconds when no time limit
	local cumulative_time = game:get_value"cumulative_time" or 0 --(number, non-negative integer) cumulative active game time
	local is_first_guess --(boolean) true if this is the first guess since the codeword changed, else false
	local sequence = {}
	local timer
	
	--// Returns the current codeword (string)
	function map:get_codeword() return codeword end
	
	--// Randomly selects a new codeword from the active dictionary, will be different from current codeword
		--returns string - the newly selected codeword
	function map:new_codeword()
		local old_codeword = codeword
		
		is_first_guess = true
		anagram_count = 0
		
		local guess_pool = {} --list of words eligible for new codeword
		local pool_count = 0 --number of words in guess pool
		for _,word in game:iter_dictionary() do
			if word~=codeword and not guess_list[word] then --codeword must be different and not matching any previous guess
				pool_count = pool_count + 1
				guess_pool[pool_count] = word
			end
		end
		
		--randomly selected new codeword then check if is an anagram for any previous guesses
		local is_not_anagram
		repeat
			is_not_anagram = true --tentative
			
			assert(pool_count>0, "Error in 'new_codeword', unable to find new codeword")
			
			local index = math.random(pool_count) --randomly choose new word from guess pool
			codeword = guess_pool[index]
			
			--calculate codeword_chars for new codeword
			codeword_chars = {} --clear old
			for i = 1, ustring.len(codeword) do --UNICODE multi-codepoint
				local char = ustring.sub(codeword,i,i) --UNICODE multi-codepoint
				codeword_chars[char] = (codeword_chars[char] or 0) + 1
			end
			
			--check if new word is anagram of any previous guess
			for word,_ in pairs(guess_list) do
				if self:get_match_count(word)==5 then
					is_not_anagram = false
					table.remove(guess_pool, index) --remove word from guess pool so not chosen again
					pool_count = pool_count - 1
					break
				end
			end
		until is_not_anagram
		
		sol.audio.play_sound"door_closed"
		
		--print("codeword changed:", codeword, sol.main.get_elapsed_time(), game:get_map():get_guess_count())
		
		--call codeword changed event
		if game.on_codeword_changed then game:on_codeword_changed(codeword) end
		
		return codeword
	end
	
	--// Sets the codeword to the specified word or chooses random word if not specified
		--word (string, optional) - the new codeword, default: chooses word at random from dictionary
		--returns (string) - the newly chosen codeword
		--!! if the newly set codeword has already been guessed then the player will be stuck, use with caution !!
	function map:set_codeword(word)
		assert(not word or type(word)=="string", "Bad argument #2 to 'set_codeword' (string or nil expected)")
		
		word = word or game:random_word()
		assert(game:is_valid_word(word), "Bad argument #2 to 'set_codeword', invalid word: "..word)
		codeword = word
		
		is_first_guess = true
		anagram_count = 0
		
		--calculate codeword_chars
		codeword_chars = {} --clear old
		for i = 1, ustring.len(codeword) do --UNICODE multi-codepoint
			local char = ustring.sub(codeword,i,i) --UNICODE multi-codepoint
			codeword_chars[char] = (codeword_chars[char] or 0) + 1
		end
		
		if game.on_codeword_changed then game:on_codeword_changed(codeword) end
		
		return codeword
	end
	
	--// Returns number of guesses made this level
	function map:get_guess_count() return #guess_list end
	
	--// Returns most recent guess made this level (string); returns nil if none
	function map:get_last_guess() return guess_list[#guess_list] end
	
	--// Custom iterator to get word in guess list (does not expose internal table)
		--usage: for i,guess in game:iter_guess_list() do
	function map:iter_guess_list()
		local iter,_,start_val = ipairs(guess_list)
		return function (_,i) return iter(guess_list, i) end, {}, start_val
	end
	
	--// Returns maximum number of guesses available this level
	function map:get_max_guesses() return max_guesses end
	
	function map:add_guesses(amount)
		max_guesses = max_guesses + amount
		if max_guesses > 30 then max_guesses = 30 end
	end
	
	--// Tests whether the specified guess matches the codeword
		--returns boolean - true: the codeword and guess match, false: they do not match
	function map:is_match(guess)
		assert(type(guess)=="string", "Bad argument #2 to 'is_match' (string expected)")
		return codeword == guess --TODO case?
	end
	
	--// Counts how many letters the given guess has in common with the codeword
		--guess (string) - the string guessed by the player to be compared to the codeword
			--(number, non-negative integer) - the index of an existing guess to compare to the codeword
		--returns (number, non-negative integer) - the number of matching letters
			--returns false if invalid index specified for guess
	function map:get_match_count(guess)
		local num_matches = 0 --(number, non-negative integer) how many characters in the guess match the codeword
		local match_chars = {} --(table, key/value) chars as keys, number of instances of that char in the guess string
		
		--if guess is specified as a number then 
		local guess_index = tonumber(guess)
		if guess_index then
			guess = guess_list[guess_index]
			if not guess then return false end --invalid index specified
		end
		
		for i = 1, ustring.len(guess) do --UNICODE multi-codepoint
			local char = ustring.sub(guess,i,i) --UNICODE multi-codepoint
			local char_count = codeword_chars[char]
			
			if char_count then
				local guess_char_count = (match_chars[char] or 0) + 1
				match_chars[char] = guess_char_count
				if guess_char_count <= char_count then
					num_matches = num_matches + 1
				end
			end
		end
		
		return num_matches
	end
	
	--// Adds a guess to the guess list
		--guess (string) - the guess to be added, must be in dictionary and not a duplicate guess
		--returns true if codeword found
		--returns the number of matches (number, non-negative integer) if guess successfully added
		--returns false and the error message (string) if not successful
	function map:add_guess(guess)
		assert(type(guess)=="string", "Bad argument #2 to '_add_guess' (string expected)")
		
		local seq = debug:check_guess(guess)
		if not seq then return end
		
		if sequence and seq>0 then
			table.insert(sequence, seq)
			if seq==7 and table.concat(sequence)=="1234567" then
				game.guess_analyzer:sequence()
			end
		end
		
		guess = ustring.lower(guess) --UNICODE multi-codepoint
		local num_matches = self:get_match_count(guess)
		
		--count letters for duplicates
		local char_counts = {}
		local max_chars = 0
		for i = 1, ustring.len(guess) do --UNICODE multi-codepoint
			local char = ustring.sub(guess,i,i) --UNICODE multi-codepoint
			local char_count = (char_counts[char] or 0) + 1
			if char_count > max_chars then max_chars = char_count end
			char_counts[char] = char_count
		end
		
		if ustring.len(guess)==0 then
			return false --do nothing
		elseif ustring.len(guess)~=5 then --UNICODE multi-codepoint
			return false, string.format(sol.language.get_string"prompt.invalid_length", guess)
		elseif guess_list[guess] then --not a duplicate guess
			return false, string.format(sol.language.get_string"prompt.duplicate_word", guess)
		elseif max_chars>1 then
			return false, string.format(sol.language.get_string"prompt.duplicate_letters", guess)
		elseif not game:is_valid_word(guess) then
			return false, string.format(sol.language.get_string"prompt.invalid_word", guess)
		else --add guess to guess list
			--add guess to guess_list
			guess_list[#guess_list+1] = guess
			guess_list[guess] = true
			
			if not timer then self:start_timer(TIMER_START) print"start timer..." end
			
			if seq==0 then sequence = false end
			
			if self:is_match(guess) then --guess matches codeword
				return true, sol.language.get_string"prompt.advancing_level"
			else return num_matches end
		end
	end
	
	function map:remove_guess(guess)
		local guess_index = tonumber(guess)
		assert(not guess or guess_index or type(guess)=="string", "Bad argument #1 to 'remove_guess' (string or number or nil expected)")
		
		if not guess then
			guess_index = #guess_list
		elseif type(guess)=="string" then
			for i,word in ipairs(guess_list) do
				if word==guess then --UNICODE multi-codepoint
					guess_index = i
					break
				end
			end
		end
		
		if not guess_index then return end
		
		if guess_index >=1 and guess_index <= #guess_list then
			local guess = table.remove(guess_list, guess_index)
			guess_list[guess] = false
		end
	end
	
	function map:remove_all_guesses()
		for guess_index = #guess_list, 1, -1 do
			local guess = table.remove(guess_list, guess_index)
			guess_list[guess] = false
		end
	end
	
	function map:start_timer(time_limit)
		time_limit = time_limit or 0
		time_remaining = time_limit
		game.guess_analyzer:set_clock(math.abs(time_remaining))
		
		TIMER_START = time_limit
		
		--increments timer every second (continuously) until timer runs out
		timer = sol.timer.start(self, 1000, function()
			time_remaining = time_remaining - 1
			cumulative_time = cumulative_time + 1
			game.guess_analyzer:set_clock(math.abs(time_remaining))
			
			if time_remaining == 0 then
				self:game_over()
				
				if timer then
					timer:stop()
					timer = nil
				end
				
				return false --stop updating timer
			else return true end --repeat timer again
		end)
	end
	
	function map:set_timer_suspended(suspended)
		if timer then timer:set_suspended(suspended) end
	end
	
	function map:is_timer_suspended()
		if not timer then return end
		return timer:is_suspended()
	end
	
	function map:add_time(seconds)
		local new_time = time_remaining + seconds
		if time_remaining < 0 and new_time >= 0 then
			time_remaining = -1
		else time_remaining = time_remaining + seconds end
		
		game.guess_analyzer:set_clock(math.abs(time_remaining))
	end
	
	function map:toggle_timer()
		--TODO
	end
	
	--// Increments a counter whenever an anagram is found
		--is_anangram (boolean or string) if not false/nil then increment anagram streak; possible values:
			--false - the player guessed a non-anagram word, reset anagram streak
			--true - the player guessed an anagram (not the codeword), increment anagram streak
			--"codeword" - the player found the codeword, increment anagram streak
	function map:found_anagram(is_anagram)
		if is_anagram=="codeword" then
			if is_first_guess then achievements.give"achievement_lucky_ducky" end
		end
		
		if is_anagram then
			anagram_count = anagram_count + 1
			if anagram_count >= 5 then
				achievements.give("achievement_anagrampage", 3)
			elseif anagram_count >= 4 then
				achievements.give("achievement_anagrampage", 2)
			elseif anagram_count >= 3 then
				achievements.give("achievement_anagrampage", 1)
			end
			
			--increment cumulative stats
			if is_anagram==true then --don't count codeword as anagram
				local cumulative_count = game:get_value"total_anagrams" or 0
				cumulative_count = cumulative_count + 1
				game:set_value("total_anagrams", cumulative_count)
			end
		else anagram_count = 0 end
		is_first_guess = false
	end
	
	function map:next_level()
		local stage_time = TIMER_START - time_remaining
		
		if stage_time <= 30 then
			achievements.give("achievement_quick_study", 3)
		elseif stage_time <= 45 then
			achievements.give("achievement_quick_study", 2)
		elseif stage_time <= 60 then
			achievements.give("achievement_quick_study", 1)
		end
		
		if self.is_done then
			if self:is_done()==false then return false end --TODO better implemenation
		end
		
		--update current level
		local level = (game:get_value"level" or 1) + 1
		game:set_value("level", level)
		
		--update cumulative time stats
		game:set_value("cumulative_time", cumulative_time)
		
		--update total guesses stats
		local stage_guesses = #guess_list
		local total_guesses = (game:get_value"total_guesses" or 0) + stage_guesses
		game:set_value("total_guesses", total_guesses)
		
		--update least guesses stats
		local least_guesses = game:get_value"least_guesses"
		if not least_guesses or stage_guesses<least_guesses then least_guesses = stage_guesses end
		game:set_value("least_guesses", least_guesses)
		
		--update fastest time stats
		local fastest_time = game:get_value"fastest_time"
		if not fastest_time or stage_time<fastest_time then fastest_time = stage_time end
		game:set_value("fastest_time", fastest_time)
		--print("stage time:", stage_time, "total time:", cumulative_time)
		
		--update hints count
		local max_hint_points = game:get_property"hints" or 0
		if max_hint_points > 0 then
			local hints = game:get_value"hints"
			local next_hint_points = game:get_value"next_hint_points"
			
			local unused_guesses = max_guesses - stage_guesses
			if unused_guesses > 0 then
				next_hint_points = next_hint_points + unused_guesses
				if next_hint_points >= max_hint_points then
					local new_count = math.floor(next_hint_points/max_hint_points)
					next_hint_points = next_hint_points % max_hint_points
					game:set_value("hints", hints + new_count)
				end
				game:set_value("next_hint_points", next_hint_points)
			end
		end
		
		--print("stage completed:", level-1, "time:", string.format("%0.2f", math.abs(time_remaining/60)), "unused guesses:", max_guesses - #guess_list, "hints:", game:get_value"hints")
		
		local map_id = game:get_map_id(level)
		if map_id then --go to next map
			game.guess_analyzer:reset()
			hero:teleport(map_id, "start", "fade") --start next level
		else --no next level; do end sequence and roll credits
			--display total game time
			local total_hours = math.floor(cumulative_time/60/60)
			local total_minutes = math.floor((cumulative_time - total_hours*60*60)/60)
			local total_seconds = cumulative_time - total_hours*60*60 - total_minutes*60
			print("Total playtime:", string.format("%02d:%02d:%02d",
				total_hours, total_minutes, total_seconds
			))
			
			--achievements
			local difficulty = game:get_difficulty()
			if difficulty == "casual" then
				achievements.give"achievement_casual"
				if cumulative_time <= 2700 then --under 45 minutes
					achievements.give("achievement_speedrunner", 3)
				elseif cumulative_time <= 3600 then --under 60 minutes
					achievements.give("achievement_speedrunner", 2)
				elseif cumulative_time <= 5400 then --under 90 minutes
					achievements.give("achievement_speedrunner", 1)
				end
			elseif difficulty == "normal" then
				achievements.give"achievement_normal"
				--[[ --TODO score achievement
				if score >= TBD then
					achievements.give"achievement_high_score"
				end
				]]
			elseif difficulty == "hardcore" then
				achievements.give"achievement_hardcore"
				local initial_continues = tonumber(game:get_property"continues")
				local continues_remaining = game:get_value"continues"
				if continues_remaining==initial_continues then
					achievements.give"achievement_brain_transplant"
				end
			end
			
			--start credits
			sol.timer.start(self, 2000, function()
				credits:start_dialog("credits.text.original", function()
					--calculate final stats
					local stats = {
						difficulty_mode = sol.language.get_string(
							"menu."..game:get_difficulty()
						),
						total_playtime = string.format("%02d:%02d:%02d",
							total_hours, total_minutes, total_seconds
						),
						--total_score --TODO
						total_guesses = total_guesses,
						total_hints = game:get_value"total_hints" or 0,
						total_anagrams = game:get_value"total_anagrams" or 0,
						--monkey_wrenches --TODO
						total_retires = game:get_value"total_retries" or 0,
						fastest_time = fastest_time,
						least_guesses = least_guesses,
					}
					
					stats_menu:start(game, stats, function()
						sol.game_manager.delete(difficulty) --game complete, delete save
						sol.main.reset()
					end)
				end)
			end)
		end
		
		return true
	end
	
	function map:level_skip(level)
		level = tonumber(level)
		assert(level, "Bad argument #1 to 'level_skip' (number expected)")
		
		local map_id = game:get_map_id(level)
		assert(map_id, "Bad arguent #1 to 'level_skip', invalid level: "..level)
		
		game:set_value("level", level)
		game:set_value("cumulative_time", cumulative_time)
		
		game.guess_analyzer:reset()
		hero:teleport(map_id, "start", "fade")
	end
	
	function map:game_over()
		game:set_value("cumulative_time", cumulative_time)
		
		if timer then
			timer:stop()
			timer = nil
		end
		
		game.guess_analyzer:do_lose() --lockout inputs and reaveal codeowrd
		self.opponent:on_lose() --do lose taunt and animation
		
		--update starting map in save game file
		local is_continue = game:regress_stage()
		if is_continue then game:save_game() end
					
		sol.timer.start(self, 5000, function() --wait 5 sec then start game over with continue option
			sol.menu.stop(game.guess_analyzer)
			local hero = game:get_hero()
			hero:teleport("game_over", "start", "fade") --start game over sequence
		end)
	end
	
	function map:taunt()
		self.opponent:play_taunt"taunt"
	end
	
	function map:joke()
		--TODO
	end
	
	--## begin the new map
	
	map:set_codeword()
	
	--determine how many guesses available based on level and difficulty mode
	max_guesses = game:get_max_guesses()
	
	map.opponent = opponent.create(map.ai)
	
	game.guess_analyzer:update_level()
	game.guess_analyzer:update_panel_count()
	
	--determine timer start, wait until first guess to activate
	TIMER_START = 60*(game:get_property"time_limit" or 0)
	game.guess_analyzer:set_clock(math.abs(TIMER_START))
	--map:start_timer(time_limit)
	
	game:save_game()
end

function map_meta:on_started() initialize(self) end

--[[ Copyright 2019-2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
