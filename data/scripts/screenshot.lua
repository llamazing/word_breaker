--[[screenshot.lua
	version 0.1a1
	22 May 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script captures a screenshot of the game, saving it in the quest write directory.
	The saved image file will have a .ppm extension.
	
	Usage:
	local screenshot = require"scripts/screenshot"
	screenshot:save"my_file"
]]

local screenshot = {}

local SAVE_DIR = "screenshots/"
local EXTENSION = ".ppm"

--// Creates a .ppm image file from a sol.surface at the specified file path
	--surface (sol.surface) - exports an image from the content of this surface
	--file_name (string) - name of the file to export in the quest write directory under SAVE_DIR
	--is_flipped (boolean, optional) - true to flip the image vertically, default: false
	--NOTE: The exported image will not have any transparency
local function write_ppm(surface, file_name, is_flipped)
	local width, height = surface:get_size()
	local MAX_COLOR = 255 --8 bits, don't change
	
	--## determine unique file name for image
	
	local file_path = string.format("%s%s_%s", SAVE_DIR, file_name, os.date"%Y%m%d_%H%M%S")
	
	local full_path = file_path..EXTENSION --tentative
	local file_index = 0
	if sol.file.exists(full_path) then
		file_path = file_path.."_%d"..EXTENSION --append an index number to duplicate file names
		repeat
			file_index = file_index + 1
			full_path = string.format(file_path, file_index)
		until not sol.file.exists(full_path)
	end
	--full_path is now a file that does not exist
	
	--## read pixel data
	
	local header = string.format("P6\n%d %d\n%d\n", width, height, MAX_COLOR)
	
	local file = sol.file.open(full_path, "wb")
	file:write(header)
	
	local pixels = surface:get_pixels()
	
	local data = {}
	do
		local index = 1
		for i=1,pixels:len(),4 do --pixes stores each pixel as R,G,B,A bytes, only need RGB
			data[index] = pixels:sub(i,i+2) --first 3 bytes of each set of 4 bytes
			index = index + 1
		end
	end
	
	--## flip image vertically
	
	if is_flipped then
		local data_flipped = {}
		--local row_width = 3 * width
		
		local index = 1
		for row=height-1,0,-1 do
			local i_start = row*width + 1
			for i=i_start,i_start+width-1 do
				data_flipped[index] = data[i]
				index = index + 1
			end
		end
		data = data_flipped
	end
	
	--## write image file
	
	file:write(table.concat(data))
	
	file:flush()
    file:close()
    
    print(string.format("screenshot saved: %s", full_path))
end

--TODO better multi-events implementation with unregistering events
function screenshot:save(file_name)
	function sol.video:on_draw(screen)
		sol.video.on_draw = nil
		write_ppm(screen, file_name, true)
	end
end

--// make directory for screenshot if it doesn't already exist
if SAVE_DIR:len()>0 then sol.file.mkdir(SAVE_DIR) end

return screenshot


--[[ Copyright 2018-2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
