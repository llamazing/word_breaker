--[[
This script initializes game values for a new savegame file.
You should modify the initialize_new_savegame() function below
to set values like the initial life and equipment
as well as the starting location.

Usage:
local initial_game = require("scripts/initial_game")
initial_game:initialize_new_savegame(game)
]]

local initial_game = {}

--Sets initial values to a new savegame file.
function initial_game:initialize_new_savegame(game, num_continues)
	game:set_starting_location("map1", "start")  -- Starting location.
	
	--game:set_max_life(12)
	--game:set_life(game:get_max_life())
	--game:set_ability("lift", 1)
	--game:set_ability("tunic", 1)
	game:set_value("level", 1)
	game:set_value("hints", 0)
	game:set_value("next_hint_points", 0)
	if num_continues then game:set_value("continues", num_continues) end
end

return initial_game
